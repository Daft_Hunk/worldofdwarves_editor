﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BuildingInfo : MonoBehaviour
{
    public Building building;
    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public CostsInfo costsInfo;
    public BuildingFurnituresInfo buildingFurnituresInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != building.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != building.Description)
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != building.Name)
            nameInput.text = building.Name;
        if (descriptionInput != null && descriptionInput.text != building.Description)
            descriptionInput.text = building.Description;
        if (buildingFurnituresInfo != null)
            buildingFurnituresInfo.RevertChanges();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != building.Name)
            building.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != building.Description)
            building.Description = descriptionInput.text;

        if (buildingFurnituresInfo != null)
        {
            List<BuildingFurnitureInfo> furnituresToDelete = new List<BuildingFurnitureInfo>();

            foreach (BuildingFurnitureInfo furniture in buildingFurnituresInfo.furnituresInfo)
            {
                if (furniture.transform.parent == UIManager.Instance.toDeleteParent)
                    furnituresToDelete.Add(furniture);
                else
                {
                    if (furniture.isNew)
                    {
                        switch (furniture.furniture.Type)
                        {
                            case 0:
                                building.furnituresIn.Add(furniture.furniture);
                                break;
                            case 1:
                                building.furnituresOut.Add(furniture.furniture);
                                break;
                            case 2:
                                building.furnituresProcess.Add(furniture.furniture);
                                break;
                            case 3:
                                building.furnituresOpti.Add(furniture.furniture);
                                break;
                            case 4:
                                building.furnituresStorage.Add(furniture.furniture);
                                break;
                        }
                    }                        
                    furniture.SaveChanges();
                }
            }
            foreach (BuildingFurnitureInfo furniture in furnituresToDelete)
            {
                switch (furniture.furniture.Type)
                {
                    case 0:
                        building.furnituresIn.Remove(furniture.furniture);
                        break;
                    case 1:
                        building.furnituresOut.Remove(furniture.furniture);
                        break;
                    case 2:
                        building.furnituresProcess.Remove(furniture.furniture);
                        break;
                    case 3:
                        building.furnituresOpti.Remove(furniture.furniture);
                        break;
                    case 4:
                        building.furnituresStorage.Remove(furniture.furniture);
                        break;
                }
                buildingFurnituresInfo.furnituresInfo.Remove(furniture);
            }
        }

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if (cost.isNew)
                        building.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach (CostInfo cost in costsToDelete)
            {
                building.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }
}
