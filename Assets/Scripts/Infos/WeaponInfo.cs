﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WeaponInfo : MonoBehaviour
{
    public Weapon weapon;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField basePriceInput;
    public TMP_InputField damageInput;
    public TMP_InputField speedInput;
    public Toggle isRangedToggle;
    public Toggle isOneHandedToggle;
    public CostsInfo costsInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != weapon.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != weapon.Description)
            isPendingChanges = true;
        if (basePriceInput != null && basePriceInput.text != weapon.BasePrice.ToString())
            isPendingChanges = true;
        if (damageInput != null && damageInput.text != weapon.Damage.ToString())
            isPendingChanges = true;
        if (speedInput != null && speedInput.text != weapon.Speed.ToString())
            isPendingChanges = true;
        if (isRangedToggle != null && isRangedToggle.isOn != weapon.IsRanged)
            isPendingChanges = true;
        if (isOneHandedToggle != null && isOneHandedToggle.isOn != weapon.IsOneHanded)
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != weapon.Name)
            nameInput.text = weapon.Name;
        if (descriptionInput != null && descriptionInput.text != weapon.Description)
            descriptionInput.text = weapon.Description;
        if (basePriceInput != null && basePriceInput.text != weapon.BasePrice.ToString())
            basePriceInput.text = weapon.BasePrice.ToString();
        if (damageInput != null && damageInput.text != weapon.Damage.ToString())
            damageInput.text = weapon.Damage.ToString();
        if (speedInput != null && speedInput.text != weapon.Speed.ToString())
            speedInput.text = weapon.Speed.ToString();
        if (isRangedToggle != null && isRangedToggle.isOn != weapon.IsRanged)
            isRangedToggle.isOn = weapon.IsRanged;
        if (isOneHandedToggle != null && isOneHandedToggle.isOn != weapon.IsOneHanded)
            isOneHandedToggle.isOn = weapon.IsOneHanded;
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != weapon.Name)
            weapon.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != weapon.Description)
            weapon.Description = descriptionInput.text;
        if (basePriceInput != null && basePriceInput.text != weapon.BasePrice.ToString())
            weapon.BasePrice = int.Parse(basePriceInput.text);
        if (damageInput != null && damageInput.text != weapon.Damage.ToString())
            weapon.Damage = int.Parse(damageInput.text);
        if (speedInput != null && speedInput.text != weapon.Speed.ToString())
            weapon.Speed = int.Parse(speedInput.text);
        if (isRangedToggle != null && isRangedToggle.isOn != weapon.IsRanged)
            weapon.IsRanged = isRangedToggle.isOn;
        if (isOneHandedToggle != null && isOneHandedToggle.isOn != weapon.IsOneHanded)
            weapon.IsOneHanded = isOneHandedToggle.isOn;

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if (cost.isNew)
                        weapon.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach (CostInfo cost in costsToDelete)
            {
                weapon.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }
}
