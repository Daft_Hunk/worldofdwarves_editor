using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MeshInfo : MonoBehaviour
{
    public int id;
    public MeshRenderer mesh;
    public MaterialData currentMaterialData;

    public MeshInfo(int _id, MeshRenderer _mesh, MaterialData _materialData)
    {
        this.id = _id;
        this.mesh = _mesh;
        this.currentMaterialData = _materialData;
    }

    public void ToggleMesh(bool isOn)
    {
        mesh.enabled = isOn;
    }

    public void ChangeMeshMaterial(int materialIndex)
    {
        currentMaterialData = GlobalValues.materials[materialIndex];
        mesh.material = currentMaterialData.material;
        ModelsManager.Instance.CheckForChanges("");
    }
}
