using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using uCPf;

public class ModelInfo : MonoBehaviour
{
    public int id;
    public TMP_Text indexLabel;
    public TMP_Text nameInput;
    public float scale;

    public ModelData modelData;
    public GameObject currentModel;

    public List<MeshInfo> meshInfos = new List<MeshInfo>();

    public bool isNew;

    public void ModelConstructor(ModelData _modelData)
    {
        currentModel = _modelData.model;
        modelData = _modelData;
        id = _modelData.id;
        indexLabel.text = id.ToString();
        nameInput.text = _modelData.name;
        scale = _modelData.scale;
        foreach(MeshData mesh in _modelData.meshesData)
        {
            meshInfos.Add(new MeshInfo(mesh.id, mesh.mesh, mesh.materialData));
        }
    }

    public void Edit()
    {
        ModelsManager.Instance.ChangeModel(this);
    }

    public void Delete()
    {
        foreach (ModelInfo modelInfo in transform.parent.GetComponentsInChildren<ModelInfo>())
        {
            if (modelInfo.id > id)
            {
                modelInfo.id--;
                modelInfo.indexLabel.text = modelInfo.id.ToString();
            }
        }
        this.transform.SetParent(UIManager.Instance.toDeleteParent);
        currentModel.transform.SetParent(UIManager.Instance.toDeleteParent);
        GlobalValues.isPendingChanges = true;
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);
        
        if (id != modelData.id)
            id = modelData.id;
        if (modelData.name != nameInput.text)
            nameInput.text = modelData.name;
        if (scale != modelData.scale)
            scale = modelData.scale;

        if (modelData.model != currentModel)
        {
            Destroy(currentModel);
            currentModel = modelData.model;
            meshInfos.Clear();
            foreach (MeshData mesh in modelData.meshesData)
            {
                meshInfos.Add(new MeshInfo(mesh.id, mesh.mesh, mesh.materialData));
            }
            ModelsManager.Instance.ChangeModel(this);
        }

        foreach (MeshInfo meshInfo in meshInfos)
        {
            meshInfo.ChangeMeshMaterial(meshInfo.currentMaterialData.id);
            //Debug.Log(modelData.meshesData[meshInfo.id].mesh.name + " : " + modelData.meshesData[meshInfo.id].materialData.id);
            //Debug.Log(meshInfo.mesh.name + " : " + meshInfo.currentMaterialData.id);

            /*if (modelData.meshesData[meshInfo.id].materialData.material != meshInfo.currentMaterialData.material)
                Debug.Log("test");

            /*
            MeshData meshData = modelData.meshesData.Find(x => x.id == meshInfo.currentMaterialData.id);
            if (meshInfo.currentMaterialData != meshData.material)
            {
                Debug.Log(meshInfo.name);
                meshInfo.currentMaterialData = meshData.material;
            }*/
        }

        indexLabel.text = id.ToString();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            transform.SetParent(AssetsManager.Instance.textureEditorContent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void SaveChanges()
    {
        if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
        {
            GlobalValues.models.Remove(modelData);
            GlobalValues.FileDelete(Lex.ASS_MODELS + modelData.name + Lex.EXT_OBJ);
        }
        else
        {
            if (modelData.id != id)
                modelData.id = id;
            if (modelData.name != nameInput.text)
            {
                GlobalValues.FileRename(Lex.ASS_MODELS, modelData.name + Lex.EXT_OBJ, nameInput.text + Lex.EXT_OBJ);
                modelData.name = nameInput.text;
            }
            if (modelData.scale != scale)
                modelData.scale = scale;
            foreach (MeshInfo meshInfo in meshInfos)
            {
                MeshData meshData = modelData.meshesData.Find(x => x.id == meshInfo.currentMaterialData.id);
                if (meshInfo.currentMaterialData != meshData.materialData)
                    meshData.materialData = meshInfo.currentMaterialData;
            }
            if (isNew)
            {
                GlobalValues.models.Add(modelData);
                isNew = false;
            }
        }
    }

    public void CheckForChanges(string text)
    {
        if (modelData.id != id)
            GlobalValues.isPendingChanges = true;
        if (modelData.name != nameInput.text)
            GlobalValues.isPendingChanges = true;
        if (modelData.scale != scale)
            GlobalValues.isPendingChanges = true;
        foreach (MeshInfo meshInfo in meshInfos)
        {
            MeshData meshData = modelData.meshesData.Find(x => x.id == meshInfo.currentMaterialData.id);
            if (meshData == null || meshInfo.currentMaterialData != meshData.materialData)
                GlobalValues.isPendingChanges = true;
        }
    }
}
