﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FiltersInfo : MonoBehaviour
{
    public ItemInfo itemInfo;
    public List<FilterInfo> filters = new List<FilterInfo>();

    public void RevertChanges()
    {
        List<FilterInfo> itemsToDestroy = filters.FindAll(x => x.isNew == true);

        foreach (FilterInfo filter in filters)
        {
            filter.RevertChanges();
        }

        foreach (FilterInfo itemToDestroy in itemsToDestroy)
        {
            filters.Remove(itemToDestroy);
        }
    }

    public void AddRecipe()
    {
        GlobalValues.currentParent = this.transform;
        int id = filters.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceFilter rF = new ResourceFilter(0, 0, 0);

        FilterInfo filter = UIManager.Instance.FilterConstructor(id, rF);
        filter.isNew = true;
        filter.parent = this.transform;
        filters.Add(filter);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }
}
