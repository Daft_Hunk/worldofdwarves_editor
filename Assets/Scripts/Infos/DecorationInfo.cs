﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DecorationInfo : MonoBehaviour
{
    public Decoration decoration;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public Toggle isExteriorToggle;
    public Toggle isInteriorToggle;
    public Toggle isWallToggle;
    public TMP_InputField staminaRateInput;
    public TMP_InputField healthRateInput;
    public TMP_InputField hungerRateInput;
    public CostsInfo costsInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != decoration.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != decoration.Description)
            isPendingChanges = true;
        if (isExteriorToggle != null && isExteriorToggle.isOn != decoration.IsExterior)
            isPendingChanges = true;
        if (isInteriorToggle != null && isInteriorToggle.isOn != decoration.IsInterior)
            isPendingChanges = true;
        if (isWallToggle != null && isWallToggle.isOn != decoration.IsWall)
            isPendingChanges = true;
        if (staminaRateInput != null && staminaRateInput.text != decoration.StaminaRate.ToString())
            isPendingChanges = true;
        if (healthRateInput != null && healthRateInput.text != decoration.HealthRate.ToString())
            isPendingChanges = true;
        if (hungerRateInput != null && hungerRateInput.text != decoration.HungerRate.ToString())
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != decoration.Name)
            nameInput.text = decoration.Name;
        if (descriptionInput != null && descriptionInput.text != decoration.Description)
            descriptionInput.text = decoration.Description;
        if (isExteriorToggle != null && isExteriorToggle.isOn != decoration.IsExterior)
            isExteriorToggle.isOn = decoration.IsExterior;
        if (isInteriorToggle != null && isInteriorToggle.isOn != decoration.IsInterior)
            isInteriorToggle.isOn = decoration.IsInterior;
        if (isWallToggle != null && isWallToggle.isOn != decoration.IsWall)
            isWallToggle.isOn = decoration.IsWall;
        if (staminaRateInput != null && staminaRateInput.text != decoration.StaminaRate.ToString())
            staminaRateInput.text = decoration.StaminaRate.ToString();
        if (healthRateInput != null && healthRateInput.text != decoration.HealthRate.ToString())
            healthRateInput.text = decoration.HealthRate.ToString();
        if (hungerRateInput != null && hungerRateInput.text != decoration.HungerRate.ToString())
            hungerRateInput.text = decoration.HungerRate.ToString();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != decoration.Name)
            decoration.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != decoration.Description)
            decoration.Description = descriptionInput.text;
        if (isExteriorToggle != null && isExteriorToggle.isOn != decoration.IsExterior)
            decoration.IsExterior = isExteriorToggle.isOn;
        if (isInteriorToggle != null && isInteriorToggle.isOn != decoration.IsInterior)
            decoration.IsInterior = isInteriorToggle.isOn;
        if (isWallToggle != null && isWallToggle.isOn != decoration.IsWall)
            decoration.IsWall = isWallToggle.isOn;
        if (staminaRateInput != null && staminaRateInput.text != decoration.StaminaRate.ToString())
            decoration.StaminaRate = int.Parse(staminaRateInput.text);
        if (healthRateInput != null && healthRateInput.text != decoration.HealthRate.ToString())
            decoration.HealthRate = int.Parse(healthRateInput.text);
        if (hungerRateInput != null && hungerRateInput.text != decoration.HungerRate.ToString())
            decoration.HungerRate = int.Parse(hungerRateInput.text);

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if (cost.isNew)
                        decoration.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach (CostInfo cost in costsToDelete)
            {
                decoration.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }
}
