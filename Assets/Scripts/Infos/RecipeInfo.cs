using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.UI;

public class RecipeInfo : MonoBehaviour
{
    public Transform infosParent;
    public ResourceRecipe resourceRecipe;
    public int id;
    public int oldId;
    public bool isNew;

    public TMP_Text subtitle;
    public TMP_Dropdown idInput;
    public TMP_InputField speedInput;
    public Toggle isAutocraftedInput;

    public void ChangeId(int _id)
    {
        if (_id != resourceRecipe.Id)
        {
            infosParent.GetComponent<RecipesInfo>().buildingFurnitureInfo.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void ChangeSpeed(string _speed)
    {
        if (_speed != resourceRecipe.Speed.ToString())
        {
            infosParent.GetComponent<RecipesInfo>().buildingFurnitureInfo.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void ChangeIsAutoCrafted(bool _isAutoCrafted)
    {
        if (_isAutoCrafted != resourceRecipe.IsAutoCrafted)
        {
            infosParent.GetComponent<RecipesInfo>().buildingFurnitureInfo.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (idInput.value != resourceRecipe.Id)
            idInput.value = resourceRecipe.Id;
        speedInput.text = resourceRecipe.Speed.ToString();
        isAutocraftedInput.isOn = resourceRecipe.IsAutoCrafted;

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            id = oldId;
            transform.SetParent(infosParent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void Duplicate()
    {
        GlobalValues.currentParent = infosParent;
        RecipesInfo recipesParent = infosParent.GetComponent<RecipesInfo>();
        int id = recipesParent.recipes.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceRecipe rR = new ResourceRecipe(resourceRecipe.Id, resourceRecipe.IsAutoCrafted, resourceRecipe.Speed);

        RecipeInfo recipe = UIManager.Instance.RecipeConstructor(id, rR);
        recipe.isNew = true;
        recipe.infosParent = infosParent;
        recipesParent.recipes.Add(recipe);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void Delete()
    {
        this.transform.SetParent(UIManager.Instance.toDeleteParent);

        foreach (RecipeInfo recipe in infosParent.GetComponent<RecipesInfo>().recipes)
        {
            if (recipe.id > id)
            {
                recipe.id--;
                recipe.subtitle.text = recipe.id + "] Recipe :";
            }
        }
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void SaveChanges()
    {
        if (idInput != null && idInput.value != resourceRecipe.Id)
            resourceRecipe.Id = idInput.value;
        if (speedInput != null && speedInput.text != resourceRecipe.Speed.ToString())
            resourceRecipe.Speed = int.Parse(speedInput.text);
        if (isAutocraftedInput != null && isAutocraftedInput.isOn != resourceRecipe.IsAutoCrafted)
            resourceRecipe.IsAutoCrafted = isAutocraftedInput.isOn;
        if (isNew)
            isNew = false;
    }
}
