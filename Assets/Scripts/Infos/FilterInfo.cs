﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class FilterInfo : MonoBehaviour
{
    public Transform parent;
    public ResourceFilter resourceFilter;
    public int id;
    public int oldId;
    public bool isNew;

    public TMP_Text subtitle;
    public TMP_Dropdown typeInput;
    public TMP_Dropdown idInput;
    public TMP_InputField percentInput;

    public void ChangeType(int _type)
    {
        idInput.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedResources = new List<TMP_Dropdown.OptionData>();

        foreach (Resource resource in GlobalValues.resources.OrderBy(x => x.Id))
        {
            if (resource.type == _type)
            {
                orderedResources.Add(new TMP_Dropdown.OptionData(resource.Name, resource.Sprite));
            }
        }
        idInput.AddOptions(orderedResources);
        idInput.value = 0;
        parent.GetComponent<FiltersInfo>().itemInfo.isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
    }

    public void ChangeId(int _id)
    {
        if (_id != resourceFilter.Id)
        {
            parent.GetComponent<FiltersInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void ChangePercent(string _percent)
    {
        if (_percent != resourceFilter.Percent.ToString())
        {
            parent.GetComponent<FiltersInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (typeInput.value != resourceFilter.Type)
            typeInput.value = resourceFilter.Type;
        if (idInput.value != resourceFilter.Id)
            idInput.value = resourceFilter.Id;
        percentInput.text = resourceFilter.Percent.ToString();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            id = oldId;
            transform.SetParent(parent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void Duplicate()
    {
        GlobalValues.currentParent = parent;
        FiltersInfo filtersParent = parent.GetComponent<FiltersInfo>();
        int id = filtersParent.filters.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceFilter rF = new ResourceFilter(resourceFilter.Type, resourceFilter.Id, resourceFilter.Percent);

        FilterInfo filter = UIManager.Instance.FilterConstructor(id, rF);
        filter.isNew = true;
        filter.parent = parent;
        filtersParent.filters.Add(filter);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void Delete()
    {
        this.transform.SetParent(UIManager.Instance.toDeleteParent);

        foreach (FilterInfo filter in parent.GetComponent<FiltersInfo>().filters)
        {
            if (filter.id > id)
            {
                filter.id--;
                filter.subtitle.text = filter.id + "] Filter :";
            }
        }
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void SaveChanges()
    {
        if (typeInput != null && typeInput.value != resourceFilter.Type)
            resourceFilter.Type = typeInput.value;
        if (idInput != null && idInput.value != resourceFilter.Id)
            resourceFilter.Id = idInput.value;
        if (percentInput != null && percentInput.text != resourceFilter.Percent.ToString())
            resourceFilter.Percent = int.Parse(percentInput.text);
        if (isNew)
            isNew = false;
    }
}
