﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExteriorJobInfo : MonoBehaviour
{
    public ExteriorJob exteriorJob;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField extractionTimeInput;
    public FiltersInfo filtersInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != exteriorJob.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != exteriorJob.Description)
            isPendingChanges = true;
        if (extractionTimeInput != null && extractionTimeInput.text != exteriorJob.ExtractionTime.ToString())
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != exteriorJob.Name)
            nameInput.text = exteriorJob.Name;
        if (descriptionInput != null && descriptionInput.text != exteriorJob.Description)
            descriptionInput.text = exteriorJob.Description;
        if (extractionTimeInput != null && extractionTimeInput.text != exteriorJob.ExtractionTime.ToString())
            extractionTimeInput.text = exteriorJob.ExtractionTime.ToString();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != exteriorJob.Name)
            exteriorJob.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != exteriorJob.Description)
            exteriorJob.Description = descriptionInput.text;
        if (extractionTimeInput != null && extractionTimeInput.text != exteriorJob.ExtractionTime.ToString())
            exteriorJob.ExtractionTime = int.Parse(extractionTimeInput.text);

        if (filtersInfo != null)
        {
            List<FilterInfo> filtersToDelete = new List<FilterInfo>();

            foreach (FilterInfo filter in filtersInfo.filters)
            {
                if (filter.transform.parent == UIManager.Instance.toDeleteParent)
                    filtersToDelete.Add(filter);
                else
                {
                    if (filter.isNew)
                        exteriorJob.resources.Add(filter.resourceFilter);
                    filter.SaveChanges();
                }
            }
            foreach (FilterInfo filter in filtersToDelete)
            {
                exteriorJob.resources.Remove(filter.resourceFilter);
                filtersInfo.filters.Remove(filter);
            }
        }
    }
}
