using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using SFB;

public class TextureInfo : MonoBehaviour
{
    public int id;

    public TextureData currentTexture;
    public TMP_Text indexLabel;
    public TMP_InputField nameInput;

    public RawImage albedoInput;
    public RawImage metallicInput;
    public RawImage normalInput;

    public bool isNew;

    public void TextureConstructor(TextureData _texture)
    {
        currentTexture = _texture;
        id = currentTexture.id;
        indexLabel.text = id.ToString();
        nameInput.text = currentTexture.name;

        albedoInput.texture = currentTexture.albedo;
        metallicInput.texture = currentTexture.metallic;
        normalInput.texture = currentTexture.normal;
    }

    public void Delete()
    {
        foreach (TextureInfo textureInfo in transform.parent.GetComponentsInChildren<TextureInfo>())
        {
            if (textureInfo.id > id)
            {
                textureInfo.id--;
                textureInfo.indexLabel.text = textureInfo.id.ToString();
            }
        }
        this.transform.SetParent(UIManager.Instance.toDeleteParent);
        GlobalValues.isPendingChanges = true;
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (id != currentTexture.id)
            id = currentTexture.id;
        if (currentTexture.name != nameInput.text)
            nameInput.text = currentTexture.name;
        if (currentTexture.albedo != albedoInput.texture)
            albedoInput.texture = currentTexture.albedo;
        if (currentTexture.metallic != metallicInput.texture)
            metallicInput.texture = currentTexture.metallic;
        if (currentTexture.normal != normalInput.texture)
            normalInput.texture = currentTexture.normal;

        indexLabel.text = id.ToString();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            transform.SetParent(AssetsManager.Instance.textureEditorContent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void SaveChanges()
    {
        if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
        {
            GlobalValues.textures.Remove(currentTexture);
            GlobalValues.FolderDelete(Lex.ASS_TEXTURES + currentTexture.name + "/");
        }
        else
        {
            if (currentTexture.id != id)
                currentTexture.id = id;
            if (currentTexture.name != nameInput.text)
            {
                GlobalValues.TextureFolderRename(Lex.ASS_TEXTURES, currentTexture.name, nameInput.text);
                currentTexture.name = nameInput.text;
            }
            GlobalValues.FolderCheck(Lex.ASS_TEXTURES + currentTexture.name + "/");
            if (currentTexture.albedo != albedoInput.texture)
            {
                currentTexture.albedo = albedoInput.texture as Texture2D;
                GameManager.SaveJPGTexture(Lex.ASS_TEXTURES + currentTexture.name + "/" + currentTexture.name + Lex.TEX_EXT[0], currentTexture.albedo);
            }
            if (currentTexture.metallic != metallicInput.texture)
            {
                currentTexture.metallic = metallicInput.texture as Texture2D;
                GameManager.SaveJPGTexture(Lex.ASS_TEXTURES + currentTexture.name + "/" + currentTexture.name + Lex.TEX_EXT[1], currentTexture.metallic);
            }
            if (currentTexture.normal != normalInput.texture)
            {
                currentTexture.normal = normalInput.texture as Texture2D;
                GameManager.SaveJPGTexture(Lex.ASS_TEXTURES + currentTexture.name + "/" + currentTexture.name + Lex.TEX_EXT[2], currentTexture.normal);
            }
            if (isNew)
            {
                GlobalValues.textures.Add(currentTexture);
                isNew = false;
            }
        }
    }

    public void CheckForChanges(string text)
    {
        if (currentTexture.name != nameInput.text)
            GlobalValues.isPendingChanges = true;
        if (currentTexture.albedo != albedoInput.texture)
            GlobalValues.isPendingChanges = true;
        if (currentTexture.metallic != metallicInput.texture)
            GlobalValues.isPendingChanges = true;
        if (currentTexture.normal != normalInput.texture)
            GlobalValues.isPendingChanges = true;
    }

    public void OpenTexturePicker(int type)
    {
        var extensions = new[] { new ExtensionFilter("Texture File", "png", "jpg", "jpeg") };

        var path = StandaloneFileBrowser.OpenFilePanel(Lex.MESS_SELECT_TEXTURE, GlobalValues.modPath + Lex.ASS_TEXTURES + currentTexture.name, extensions, false);
        if (path.Length > 0)
        {
            switch (type)
            {
                case 0:
                    albedoInput.texture = TextureLoader.LoadTexture(path[0]);
                    break;
                case 1:
                    metallicInput.texture = TextureLoader.LoadTexture(path[0]);
                    break;
                case 2:
                    normalInput.texture = TextureLoader.LoadTexture(path[0], true);
                    break;
            }
            CheckForChanges("");
        }
    }
}
