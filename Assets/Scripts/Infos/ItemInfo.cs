using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public class ItemInfo : MonoBehaviour
{
    public int type;
    public int id;
    public Sprite sprite;
    public Transform parent;
    public Button spriteButton;
    public Button upButton;
    public Button downButton;
    public Button duplicateButton;
    public Button deleteButton;
    public Button revertButton;
    public Button saveButton;

    public bool isNew;
    public bool isPendingChanges;

    public void Update()
    {
        if (revertButton != null && isPendingChanges)
            revertButton.interactable = true;
        else
            revertButton.interactable = false;

        if (saveButton != null && isPendingChanges)
            saveButton.interactable = true;
        else
            saveButton.interactable = false;
    }

    public void CheckForInteractableMove()
    {
        if (id == 0)
            upButton.interactable = false;
        else
            upButton.interactable = true;

        if (id >= parent.gameObject.GetComponentsInChildren<ItemInfo>().Count() - 1)
            downButton.interactable = false;
        else
            downButton.interactable = true;
    }

    public void SaveChanges()
    {
        switch (type)
        {
            case 0:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.resources.Remove(GetComponent<ResourceInfo>().resource);
                else
                {
                    GetComponent<ResourceInfo>().SaveChanges();
                    if (GetComponent<ResourceInfo>().resource.Id != id)
                        GetComponent<ResourceInfo>().resource.Id = id;                    
                    if (GetComponent<ResourceInfo>().resource.Sprite != sprite)
                    {
                        GetComponent<ResourceInfo>().resource.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[GetComponent<ResourceInfo>().resource.Type] + id, sprite);                    
                    }
                    if (isNew)
                    {
                        GlobalValues.resources.Add(GetComponent<ResourceInfo>().resource);
                        isNew = false;
                    }
                }
                break;
            case 1:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.furnitures.Remove(GetComponent<FurnitureInfo>().furniture);
                else
                {
                    GetComponent<FurnitureInfo>().SaveChanges();
                    if (GetComponent<FurnitureInfo>().furniture.Id != id)
                        GetComponent<FurnitureInfo>().furniture.Id = id;
                    if (GetComponent<FurnitureInfo>().furniture.Sprite != sprite)
                    {
                        GetComponent<FurnitureInfo>().furniture.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.furnitures.Add(GetComponent<FurnitureInfo>().furniture);
                        isNew = false;
                    }
                }             
                break;
            case 2:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.facilities.Remove(GetComponent<FacilityInfo>().facility);
                else
                {
                    GetComponent<FacilityInfo>().SaveChanges();
                    if (GetComponent<FacilityInfo>().facility.Id != id)
                        GetComponent<FacilityInfo>().facility.Id = id;
                    if (GetComponent<FacilityInfo>().facility.Sprite != sprite)
                    {
                        GetComponent<FacilityInfo>().facility.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.facilities.Add(GetComponent<FacilityInfo>().facility);
                        isNew = false;
                    }
                }
                break;
            case 3:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.decorations.Remove(GetComponent<DecorationInfo>().decoration);
                else
                {
                    GetComponent<DecorationInfo>().SaveChanges();
                    if (GetComponent<DecorationInfo>().decoration.Id != id)
                        GetComponent<DecorationInfo>().decoration.Id = id;
                    if (GetComponent<DecorationInfo>().decoration.Sprite != sprite)
                    {
                        GetComponent<DecorationInfo>().decoration.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.decorations.Add(GetComponent<DecorationInfo>().decoration);
                        isNew = false;
                    }
                }
                break;
            case 4:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.buildings.Remove(GetComponent<BuildingInfo>().building);
                else
                {
                    GetComponent<BuildingInfo>().SaveChanges();
                    if (GetComponent<BuildingInfo>().building.Id != id)
                        GetComponent<BuildingInfo>().building.Id = id;
                    if (GetComponent<BuildingInfo>().building.Sprite != sprite)
                    {
                        GetComponent<BuildingInfo>().building.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.buildings.Add(GetComponent<BuildingInfo>().building);
                        isNew = false;
                    }
                }
                break;
            case 5:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.exteriorJobs.Remove(GetComponent<ExteriorJobInfo>().exteriorJob);
                else
                {
                    GetComponent<ExteriorJobInfo>().SaveChanges();
                    if (GetComponent<ExteriorJobInfo>().exteriorJob.Id != id)
                        GetComponent<ExteriorJobInfo>().exteriorJob.Id = id;
                    if (GetComponent<ExteriorJobInfo>().exteriorJob.Sprite != sprite)
                    {
                        GetComponent<ExteriorJobInfo>().exteriorJob.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.exteriorJobs.Add(GetComponent<ExteriorJobInfo>().exteriorJob);
                        isNew = false;
                    }
                }
                break;
            case 6:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.weapons.Remove(GetComponent<WeaponInfo>().weapon);
                else
                {
                    GetComponent<WeaponInfo>().SaveChanges();
                    if (GetComponent<WeaponInfo>().weapon.Id != id)
                        GetComponent<WeaponInfo>().weapon.Id = id;
                    if (GetComponent<WeaponInfo>().weapon.Sprite != sprite)
                    {
                        GetComponent<WeaponInfo>().weapon.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.weapons.Add(GetComponent<WeaponInfo>().weapon);
                        isNew = false;
                    }
                }
                break;
            case 7:
                if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
                    GlobalValues.units.Remove(GetComponent<UnitInfo>().unit);
                else
                {
                    GetComponent<UnitInfo>().SaveChanges();
                    if (GetComponent<UnitInfo>().unit.Id != id)
                        GetComponent<UnitInfo>().unit.Id = id;
                    if (GetComponent<UnitInfo>().unit.Sprite != sprite)
                    {
                        GetComponent<UnitInfo>().unit.Sprite = sprite;
                        GameManager.SaveSprite(Lex.SP_PATHS[type + 3] + id, sprite);
                    }
                    if (isNew)
                    {
                        GlobalValues.units.Add(GetComponent<UnitInfo>().unit);
                        isNew = false;
                    }
                }
                break;
        }
        isPendingChanges = false;        
    }

    public void CheckForChange(string _text)
    {
        switch (type)
        {
            case 0:
                if (GetComponent<ResourceInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<ResourceInfo>().resource.Id)
                    isPendingChanges = true;
                break;
            case 1:
                if (GetComponent<FurnitureInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<FurnitureInfo>().furniture.Id)
                    isPendingChanges = true;
                break;
            case 2:
                if (GetComponent<FacilityInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<FacilityInfo>().facility.Id)
                    isPendingChanges = true;
                break;
            case 3:
                if (GetComponent<DecorationInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<DecorationInfo>().decoration.Id)
                    isPendingChanges = true;
                break;
            case 4:
                if (GetComponent<BuildingInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<BuildingInfo>().building.Id)
                    isPendingChanges = true;
                break;
            case 5:
                if (GetComponent<ExteriorJobInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<ExteriorJobInfo>().exteriorJob.Id)
                    isPendingChanges = true;
                break;
            case 6:
                if (GetComponent<WeaponInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<WeaponInfo>().weapon.Id)
                    isPendingChanges = true;
                break;
            case 7:
                if (GetComponent<UnitInfo>().CheckForChange())
                    isPendingChanges = true;
                if (id != GetComponent<UnitInfo>().unit.Id)
                    isPendingChanges = true;
                break;
        }
        if (isPendingChanges)
            GlobalValues.isPendingChanges = true;
    }

    public void CheckForChange(bool _value)
    {
        switch (type)
        {
            case 0:
                if (GetComponent<ResourceInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 1:
                if (GetComponent<FurnitureInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 2:
                if (GetComponent<FacilityInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 3:
                if (GetComponent<DecorationInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 4:
                if (GetComponent<BuildingInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 5:
                if (GetComponent<ExteriorJobInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 6:
                if (GetComponent<WeaponInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
            case 7:
                if (GetComponent<UnitInfo>().CheckForChange())
                    isPendingChanges = true;
                break;
        }
        if (isPendingChanges)
            GlobalValues.isPendingChanges = true;
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        switch (type)
        {
            case 0:
                GetComponent<ResourceInfo>().RevertChanges();

                if (id != GetComponent<ResourceInfo>().resource.Id)
                {
                    id = GetComponent<ResourceInfo>().resource.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 1:
                GetComponent<FurnitureInfo>().RevertChanges();

                if (id != GetComponent<FurnitureInfo>().furniture.Id)
                {
                    id = GetComponent<FurnitureInfo>().furniture.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 2:
                GetComponent<FacilityInfo>().RevertChanges();

                if (id != GetComponent<FacilityInfo>().facility.Id)
                {
                    id = GetComponent<FacilityInfo>().facility.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 3:
                GetComponent<DecorationInfo>().RevertChanges();

                if (id != GetComponent<DecorationInfo>().decoration.Id)
                {
                    id = GetComponent<DecorationInfo>().decoration.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 4:
                GetComponent<BuildingInfo>().RevertChanges();

                if (id != GetComponent<BuildingInfo>().building.Id)
                {
                    id = GetComponent<BuildingInfo>().building.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 5:
                GetComponent<ExteriorJobInfo>().RevertChanges();

                if (id != GetComponent<ExteriorJobInfo>().exteriorJob.Id)
                {
                    id = GetComponent<ExteriorJobInfo>().exteriorJob.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 6:
                GetComponent<WeaponInfo>().RevertChanges();

                if (id != GetComponent<WeaponInfo>().weapon.Id)
                {
                    id = GetComponent<WeaponInfo>().weapon.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
            case 7:
                GetComponent<UnitInfo>().RevertChanges();

                if (id != GetComponent<UnitInfo>().unit.Id)
                {
                    id = GetComponent<UnitInfo>().unit.Id;
                    this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
                    transform.SetSiblingIndex(id);
                    CheckForInteractableMove();
                }
                isPendingChanges = false;
                break;
        }
        if (GetComponentInChildren<CostsInfo>())
            GetComponentInChildren<CostsInfo>().RevertChanges();
        if (GetComponentInChildren<FiltersInfo>())
            GetComponentInChildren<FiltersInfo>().RevertChanges();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            transform.SetParent(parent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void Delete()
    {
        this.transform.SetParent(UIManager.Instance.toDeleteParent);

        foreach (ItemInfo item in parent.GetComponent<ContentInfo>().items)
        {
            if (item.id > id)
            {
                item.id--;
                item.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = item.id.ToString();
            }
            item.CheckForInteractableMove();
        }
        GlobalValues.isPendingChanges = true;
    }

    public void Duplicate()
    {
        foreach (ItemInfo item in parent.GetComponent<ContentInfo>().items)
        {
            if (item.id > id)
            {
                item.id++;
                item.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = item.id.ToString();
            }
            item.CheckForInteractableMove();
        }
        switch (type)
        {
            case 0:
                {
                    Resource ori = GetComponent<ResourceInfo>().resource;
                    Resource _resource = new Resource(ori.Type, id + 1, ori.Name, ori.Rarity, ori.BasePrice, ori.Description, ori.Sprite, ori.Decal);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _resource.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[_resource.Type];

                    ItemInfo item = UIManager.Instance.ResourceUIConstructor(_resource);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 1:
                {
                    Furniture ori = GetComponent<FurnitureInfo>().furniture;
                    Furniture _furniture = new Furniture(ori.UId, id + 1, ori.Name, ori.Description, ori.StaminaRate, ori.HealthRate, ori.HungerRate, ori.Sprite, ori.Prefab, ori.MinimapIcon);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _furniture.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.FurnitureUIConstructor(_furniture);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 2:
                {
                    Facility ori = GetComponent<FacilityInfo>().facility;
                    Facility _facility = new Facility(id + 1, ori.Name, ori.Description, ori.Sprite, ori.Prefab, ori.DwarfCapacity, ori.ExtractionTime, ori.ExtractionAmount);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _facility.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.FacilityUIConstructor(_facility);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 3:
                {
                    Decoration ori = GetComponent<DecorationInfo>().decoration;
                    Decoration _decoration = new Decoration(id + 1, ori.Name, ori.Description, ori.IsExterior, ori.IsInterior, ori.IsWall, ori.StaminaRate, ori.HealthRate, ori.HungerRate, ori.Sprite, ori.Prefab);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _decoration.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.DecorationUIConstructor(_decoration);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 4:
                {
                    Building ori = GetComponent<BuildingInfo>().building;
                    Building _building = new Building(id + 1, ori.Name, ori.Description, ori.Sprite, ori.FloorMaterial);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _building.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    if (ori.furnituresIn != null)
                    {
                        foreach (Furniture furniture in ori.furnituresIn)
                        {
                            _building.furnituresIn.Add(furniture);
                        }
                    }
                    if (ori.furnituresOut != null)
                    {
                        foreach (Furniture furniture in ori.furnituresOut)
                        {
                            _building.furnituresOut.Add(furniture);
                        }
                    }
                    if (ori.furnituresProcess != null)
                    {
                        foreach (Furniture furniture in ori.furnituresProcess)
                        {
                            _building.furnituresProcess.Add(furniture);
                        }
                    }
                    if (ori.furnituresOpti != null)
                    {
                        foreach (Furniture furniture in ori.furnituresOpti)
                        {
                            _building.furnituresOpti.Add(furniture);
                        }
                    }
                    if (ori.furnituresStorage != null)
                    {
                        foreach (Furniture furniture in ori.furnituresStorage)
                        {
                            _building.furnituresStorage.Add(furniture);
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.BuildingUIConstructor(_building);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 5:
                {
                    ExteriorJob ori = GetComponent<ExteriorJobInfo>().exteriorJob;
                    ExteriorJob _exteriorJob = new ExteriorJob(id + 1, ori.Name, ori.Description, ori.ExtractionTime, ori.Sprite);
                    if (ori.resources != null)
                    {
                        foreach (ResourceFilter rF in ori.resources)
                        {
                            _exteriorJob.resources.Add(new ResourceFilter(rF.Type, rF.Id, rF.Percent));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.ExteriorJobUIConstructor(_exteriorJob);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 6:
                {
                    Weapon ori = GetComponent<WeaponInfo>().weapon;
                    Weapon _weapon = new Weapon(id + 1, ori.Name, ori.Description, ori.BasePrice, ori.Damage, ori.Speed, ori.IsRanged, ori.IsOneHanded, ori.Sprite);
                    if (ori.resourcesNeeded != null)
                    {
                        foreach (ResourceAmount rA in ori.resourcesNeeded)
                        {
                            _weapon.resourcesNeeded.Add(new ResourceAmount(rA.Type, rA.Id, rA.Amount));
                        }
                    }
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.WeaponUIConstructor(_weapon);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
            case 7:
                {
                    Unit ori = GetComponent<UnitInfo>().unit;
                    Unit _unit = new Unit(id + 1, ori.Name, ori.Description, ori.Health, ori.Stamina, ori.Hunger, ori.Speed, ori.Sprite);
                    GlobalValues.currentParent = UIManager.Instance.contentParents[type + 3];

                    ItemInfo item = UIManager.Instance.UnitUIConstructor(_unit);
                    item.transform.SetSiblingIndex(item.id);
                    item.isNew = true;
                    break;
                }
        }
        StartCoroutine(GameManager.Instance.DelayedJumpToItem(GetComponent<RectTransform>()));
        GlobalValues.isPendingChanges = true;
    }

    public void MoveUp()
    {
        ItemInfo siblingItem = parent.GetComponent<ContentInfo>().items.Find(x => x.id == id - 1);

        id--;
        siblingItem.id++;

        siblingItem.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = siblingItem.id.ToString();
        this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
        transform.SetSiblingIndex(id);

        siblingItem.CheckForInteractableMove();
        CheckForInteractableMove();
        siblingItem.CheckForChange("");
        CheckForChange("");
    }

    public void MoveDown()
    {
        ItemInfo siblingItem = parent.GetComponent<ContentInfo>().items.Find(x => x.id == id + 1);

        id++;
        siblingItem.id--;

        siblingItem.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = siblingItem.id.ToString();
        this.transform.Find(Lex.LBL_ID).GetComponentInChildren<TMP_InputField>().text = id.ToString();
        transform.SetSiblingIndex(id);

        siblingItem.CheckForInteractableMove();
        CheckForInteractableMove();
        siblingItem.CheckForChange("");
        CheckForChange("");
    }

    public void SelectSprite()
    {
        GameManager.Instance.OpenSpriteEditor(this);
    }

    public void ChangeSprite(Sprite _sprite)
    {
        spriteButton.transform.Find("Image").GetComponent<Image>().sprite = _sprite;
        sprite = _sprite;
        isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
    }
}
