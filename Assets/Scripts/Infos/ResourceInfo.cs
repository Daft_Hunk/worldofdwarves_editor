using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceInfo : MonoBehaviour {

    public Resource resource;
    public Texture2D decal;

    public Button decalButton;
    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField basePriceInput;
    public TMP_InputField rarityInput;
    public CostsInfo costsInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != resource.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != resource.Description)
            isPendingChanges = true;        
        if (basePriceInput != null && basePriceInput.text != resource.BasePrice.ToString())
            isPendingChanges = true;
        if (rarityInput != null && rarityInput.text != resource.Rarity.ToString())
            isPendingChanges = true;
        if(resource.Decal != null && decal != null)
        {
            if (resource.Decal != decal)
                isPendingChanges = true;
        }

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != resource.Name)
            nameInput.text = resource.Name;
        if (descriptionInput != null && descriptionInput.text != resource.Description)
            descriptionInput.text = resource.Description;
        if (basePriceInput != null && basePriceInput.text != resource.BasePrice.ToString())
            basePriceInput.text = resource.BasePrice.ToString();
        if (rarityInput != null && rarityInput.text != resource.Rarity.ToString())
            rarityInput.text = resource.Rarity.ToString();
        if (resource.Decal != null && decal != null)
        {
            if (resource.Decal != decal)
            {
                decal = resource.Decal;
                decalButton.GetComponentInChildren<RawImage>().texture = decal;
            }
        }
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != resource.Name)
            resource.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != resource.Description)
            resource.Description = descriptionInput.text;
        if (basePriceInput != null && basePriceInput.text != resource.BasePrice.ToString())
            resource.BasePrice = int.Parse(basePriceInput.text);
        if (rarityInput != null && rarityInput.text != resource.Rarity.ToString())
            resource.rarity = int.Parse(rarityInput.text);
        if (resource.Decal != null && decal != null)
        {
            if (resource.Decal != decal)
            {
                resource.Decal = decal;
                switch (resource.Type)
                {
                    case 0:
                        GameManager.SavePNGTexture(Lex.DEC_MINERALS + GetComponent<ItemInfo>().id, decal);
                        break;
                    case 1:
                        GameManager.SavePNGTexture(Lex.DEC_GEMS + GetComponent<ItemInfo>().id, decal);
                        break;
                }
            }
        }

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if(cost.isNew)
                        resource.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach(CostInfo cost in costsToDelete)
            {
                resource.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }

    public void SelectTexture()
    {
        GameManager.Instance.OpenTextureEditor(this);
    }

    public void ChangeTexture(Texture2D _texture)
    {
        decalButton.transform.GetComponentInChildren<RawImage>().texture = _texture;
        decal = _texture;
        GetComponent<ItemInfo>().isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
    }
}
