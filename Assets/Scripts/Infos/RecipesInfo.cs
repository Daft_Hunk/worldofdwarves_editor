﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RecipesInfo : MonoBehaviour
{
    public BuildingFurnitureInfo buildingFurnitureInfo;
    public List<RecipeInfo> recipes = new List<RecipeInfo>();

    public void RevertChanges()
    {
        List<RecipeInfo> itemsToDestroy = recipes.FindAll(x => x.isNew == true);

        foreach (RecipeInfo recipe in recipes)
        {
            recipe.RevertChanges();
        }

        foreach (RecipeInfo itemToDestroy in itemsToDestroy)
        {
            recipes.Remove(itemToDestroy);
        }
    }

    public void AddRecipe()
    {
        GlobalValues.currentParent = this.transform;
        int id = recipes.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceRecipe rR = new ResourceRecipe(0, false, 0);

        RecipeInfo recipe = UIManager.Instance.RecipeConstructor(id, rR);
        recipe.isNew = true;
        recipe.infosParent = this.transform;
        recipes.Add(recipe);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }
}
