using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class BuildingFurnitureInfo : MonoBehaviour
{
    public Transform infosParent;
    public Furniture furniture;
    public int id;
    public int oldId;
    public bool isNew;

    public TMP_Text subtitle;
    public TMP_Dropdown furnitureInput;
    public TMP_InputField capacityInput;

    public FiltersInfo filters;
    public RecipesInfo recipes;

    public void ChangeFurniture(int _id)
    {
        if (_id != furniture.Id)
        {
            infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void ChangeCapacity(string _capacity)
    {
        if (_capacity != furniture.Capacity.ToString())
        {
            infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (furnitureInput.value != furniture.Id)
            furnitureInput.value = furniture.Id;
        if (capacityInput != null)
            capacityInput.text = furniture.Capacity.ToString();
        if (filters != null)
            filters.RevertChanges();
        if (recipes != null)
            recipes.RevertChanges();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            id = oldId;
            transform.SetParent(infosParent);
            this.transform.SetSiblingIndex(id);
        }
    }

    public void Duplicate()
    {
        GlobalValues.currentParent = transform.parent;
        BuildingFurnituresInfo buildingFurnituresParent = infosParent.GetComponent<BuildingFurnituresInfo>();

        string label = "";
        int UId = GlobalValues.furnitureUID++;
        int id = 0;
        Furniture _furniture = new Furniture(UId, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.HungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
        _furniture.InitiateType(furniture.Type, furniture.Capacity);

        switch (_furniture.Type)
        {
            case 0:
                id = buildingFurnituresParent.insParent.childCount;
                label = "In";
                foreach(ResourceFilter rF in furniture.resourcesAllowed)
                {
                    _furniture.resourcesAllowed.Add(new ResourceFilter(rF.Type, rF.Id, rF.Percent));
                }
                break;
            case 1:
                id = buildingFurnituresParent.outsParent.childCount;
                label = "Out";
                foreach (ResourceFilter rF in furniture.resourcesAllowed)
                {
                    _furniture.resourcesAllowed.Add(new ResourceFilter(rF.Type, rF.Id, rF.Percent));
                }
                break;
            case 2:
                id = buildingFurnituresParent.processesParent.childCount;
                label = "Process";
                foreach (ResourceRecipe rR in furniture.resourcesRecipes)
                {
                    _furniture.resourcesRecipes.Add(new ResourceRecipe(rR.Id, rR.IsAutoCrafted, rR.Speed));
                }
                break;
            case 3:
                id = buildingFurnituresParent.optisParent.childCount;
                label = "Opti";
                foreach (ResourceFilter rF in furniture.resourcesAllowed)
                {
                    _furniture.resourcesAllowed.Add(new ResourceFilter(rF.Type, rF.Id, rF.Percent));
                }
                break;
            case 4:
                id = buildingFurnituresParent.storagesParent.childCount;
                label = "Storage";
                foreach (ResourceFilter rF in furniture.resourcesAllowed)
                {
                    _furniture.resourcesAllowed.Add(new ResourceFilter(rF.Type, rF.Id, rF.Percent));
                }
                break;
        }

        BuildingFurnitureInfo buildingFurniture = UIManager.Instance.BuildingFurnitureConstructor(id, _furniture, label);
        buildingFurniture.isNew = true;
        buildingFurniture.infosParent = this.transform;
        buildingFurnituresParent.furnituresInfo.Add(buildingFurniture);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void Delete()
    {
        this.transform.SetParent(UIManager.Instance.toDeleteParent);

        foreach (BuildingFurnitureInfo furnitureInfo in infosParent.GetComponent<BuildingFurnituresInfo>().furnituresInfo)
        {
            if (furnitureInfo.id > id)
            {
                furnitureInfo.id--;
                //furnitureInfo.subtitle.text = cost.id + "] Cost :";
            }
        }
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void SaveChanges()
    {
        if (furnitureInput.value != furniture.Id)
            furniture.Id = furnitureInput.value;
        if (capacityInput != null)
            furniture.Capacity = int.Parse(capacityInput.text);

        if (filters != null)
        {
            List<FilterInfo> filtersToDelete = new List<FilterInfo>();

            foreach (FilterInfo filter in filters.filters)
            {
                if (filter.transform.parent == UIManager.Instance.toDeleteParent)
                    filtersToDelete.Add(filter);
                else
                {
                    if (filter.isNew)
                        furniture.resourcesAllowed.Add(filter.resourceFilter);
                    filter.SaveChanges();
                }
            }
            foreach (FilterInfo filter in filtersToDelete)
            {
                furniture.resourcesAllowed.Remove(filter.resourceFilter);
                filters.filters.Remove(filter);
            }
        }

        if (recipes != null)
        {
            List<RecipeInfo> recipesToDelete = new List<RecipeInfo>();

            foreach (RecipeInfo recipe in recipes.recipes)
            {
                if (recipe.transform.parent == UIManager.Instance.toDeleteParent)
                    recipesToDelete.Add(recipe);
                else
                {
                    if (recipe.isNew)
                        furniture.resourcesRecipes.Add(recipe.resourceRecipe);
                    recipe.SaveChanges();
                }
            }
            foreach (RecipeInfo recipe in recipesToDelete)
            {
                furniture.resourcesRecipes.Remove(recipe.resourceRecipe);
                recipes.recipes.Remove(recipe);
            }
        }
    }
}