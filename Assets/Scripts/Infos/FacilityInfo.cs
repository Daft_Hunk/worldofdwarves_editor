﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FacilityInfo : MonoBehaviour
{
    public Facility facility;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField dwarfCapacityInput;
    public TMP_InputField extractionTimeInput;
    public TMP_InputField extractionAmountInput;
    public CostsInfo costsInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != facility.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != facility.Description)
            isPendingChanges = true;
        if (dwarfCapacityInput != null && dwarfCapacityInput.text != facility.DwarfCapacity.ToString())
            isPendingChanges = true;
        if (extractionTimeInput != null && extractionTimeInput.text != facility.ExtractionTime.ToString())
            isPendingChanges = true;
        if (extractionAmountInput != null && extractionAmountInput.text != facility.ExtractionAmount.ToString())
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != facility.Name)
            nameInput.text = facility.Name;
        if (descriptionInput != null && descriptionInput.text != facility.Description)
            descriptionInput.text = facility.Description;
        if (dwarfCapacityInput != null && dwarfCapacityInput.text != facility.DwarfCapacity.ToString())
            dwarfCapacityInput.text = facility.DwarfCapacity.ToString();
        if (extractionTimeInput != null && extractionTimeInput.text != facility.ExtractionTime.ToString())
            extractionTimeInput.text = facility.ExtractionTime.ToString();
        if (extractionAmountInput != null && extractionAmountInput.text != facility.ExtractionAmount.ToString())
            extractionAmountInput.text = facility.ExtractionAmount.ToString();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != facility.Name)
            facility.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != facility.Description)
            facility.Description = descriptionInput.text;
        if (dwarfCapacityInput != null && dwarfCapacityInput.text != facility.DwarfCapacity.ToString())
            facility.DwarfCapacity = int.Parse(dwarfCapacityInput.text);
        if (extractionTimeInput != null && extractionTimeInput.text != facility.ExtractionTime.ToString())
            facility.ExtractionTime = int.Parse(extractionTimeInput.text);
        if (extractionAmountInput != null && extractionAmountInput.text != facility.ExtractionAmount.ToString())
            facility.ExtractionAmount = int.Parse(extractionAmountInput.text);

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if (cost.isNew)
                        facility.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach (CostInfo cost in costsToDelete)
            {
                facility.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }
}
