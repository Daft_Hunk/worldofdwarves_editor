using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using uCPf;

public class MaterialInfo : MonoBehaviour
{
    public int id;
    public TMP_Text indexLabel;
    public TMP_Text nameLabel;

    public string albedoColor;
    public float occlusion;

    public float metallicStrengh;
    public float smoothness;
    public float smoothMin;
    public float smoothMax;

    public float normalStrengh;

    public TextureData textureData;
    public MaterialData materialData;

    public bool isNew;

    public void MaterialConstructor(MaterialData _material)
    {
        textureData = _material.textureData;
        materialData = _material;
        id = materialData.id;
        indexLabel.text = id.ToString();
        nameLabel.text = materialData.name;

        albedoColor = GlobalValues.ColorToHex(materialData.material.color);
        occlusion = materialData.material.GetFloat("_OcclusionStrength");

        metallicStrengh = materialData.material.GetFloat("_Metallic");
        smoothness = materialData.material.GetFloat("_Glossiness");
        smoothMin = materialData.material.GetFloat("_GlossMin");
        smoothMax = materialData.material.GetFloat("_GlossMax");

        normalStrengh = materialData.material.GetFloat("_BumpScale");
    }

    public void ChangeValue()
    {
        if (albedoColor != MaterialsManager.Instance.hexColor.text)
            albedoColor = MaterialsManager.Instance.hexColor.text;
        if (occlusion != MaterialsManager.Instance.occlusionSlider.value)
            occlusion = MaterialsManager.Instance.occlusionSlider.value;

        if (metallicStrengh != MaterialsManager.Instance.metallicSlider.value)
            metallicStrengh = MaterialsManager.Instance.metallicSlider.value;
        if (smoothness != MaterialsManager.Instance.smoothnessSlider.value)
            smoothness = MaterialsManager.Instance.smoothnessSlider.value;
        if (smoothMin != MaterialsManager.Instance.smoothMinSlider.value)
            smoothMin = MaterialsManager.Instance.smoothMinSlider.value;
        if (smoothMax != MaterialsManager.Instance.smoothMaxSlider.value)
            smoothMax = MaterialsManager.Instance.smoothMaxSlider.value;

        if (normalStrengh != MaterialsManager.Instance.normalSlider.value)
            normalStrengh = MaterialsManager.Instance.normalSlider.value;
        CheckForChanges("");
    }

    public void Edit()
    {
        MaterialsManager.Instance.ChangeMaterial(this);
    }

    public void Delete()
    {
        foreach (MaterialInfo materialInfo in transform.parent.GetComponentsInChildren<MaterialInfo>())
        {
            if (materialInfo.id > id)
            {
                materialInfo.id--;
                materialInfo.indexLabel.text = materialInfo.id.ToString();
            }
        }
        this.transform.SetParent(UIManager.Instance.toDeleteParent);
        GlobalValues.isPendingChanges = true;
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (materialData.textureData != textureData)
            textureData = materialData.textureData;
        if (id != materialData.id)
            id = materialData.id;
        if (indexLabel.text != id.ToString())
            indexLabel.text = id.ToString();
        if (nameLabel.text != materialData.name)
            nameLabel.text = materialData.name;

        if (albedoColor != GlobalValues.ColorToHex(materialData.material.color))
            albedoColor = GlobalValues.ColorToHex(materialData.material.color);
        if (occlusion != materialData.material.GetFloat("_OcclusionStrength"))
            occlusion = materialData.material.GetFloat("_OcclusionStrength");

        if (metallicStrengh != materialData.material.GetFloat("_Metallic"))
            metallicStrengh = materialData.material.GetFloat("_Metallic");
        if (smoothness != materialData.material.GetFloat("_Glossiness"))
            smoothness = materialData.material.GetFloat("_Glossiness");
        if (smoothMin != materialData.material.GetFloat("_GlossMin"))
            smoothMin = materialData.material.GetFloat("_GlossMin");
        if (smoothMax != materialData.material.GetFloat("_GlossMax"))
            smoothMax = materialData.material.GetFloat("_GlossMax");

        if (normalStrengh != materialData.material.GetFloat("_BumpScale"))
            normalStrengh = materialData.material.GetFloat("_BumpScale");

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            transform.SetParent(AssetsManager.Instance.textureEditorContent);
            this.transform.SetSiblingIndex(id);
        }
        if (MaterialsManager.Instance.currentMaterialInfo == this && !isNew)
            MaterialsManager.Instance.ChangeMaterial(this);
    }

    public void SaveChanges()
    {
        if (transform.parent == UIManager.Instance.toDeleteParent && !isNew)
            GlobalValues.materials.Remove(materialData);
        else
        {
            if (materialData.textureData != textureData)
                materialData.textureData = textureData;
            if (id != materialData.id)
                materialData.id = id;
            if (indexLabel.text != id.ToString())
                id = int.Parse(indexLabel.text);
            if (nameLabel.text != materialData.name)
                materialData.name = nameLabel.text;

            if (albedoColor != GlobalValues.ColorToHex(materialData.material.color))
                materialData.material.color = GlobalValues.HexToColor(albedoColor);
            if (occlusion != materialData.material.GetFloat("_OcclusionStrength"))
                materialData.material.SetFloat("_OcclusionStrength", occlusion);

            if (metallicStrengh != materialData.material.GetFloat("_Metallic"))
                materialData.material.SetFloat("_Metallic", metallicStrengh);
            if (smoothness != materialData.material.GetFloat("_Glossiness"))
                materialData.material.SetFloat("_Glossiness", smoothness);
            if (smoothMin != materialData.material.GetFloat("_GlossMin"))
                materialData.material.SetFloat("_GlossMin", smoothMin);
            if (smoothMax != materialData.material.GetFloat("_GlossMax"))
                materialData.material.SetFloat("_GlossMax", smoothMax);

            if (normalStrengh != materialData.material.GetFloat("_BumpScale"))
                materialData.material.SetFloat("_BumpScale", normalStrengh);

            if (isNew)
            {
                GlobalValues.materials.Add(materialData);
                isNew = false;
            }
        }
    }

    public void CheckForChanges(string text)
    {
        if (materialData.textureData != textureData)
            GlobalValues.isPendingChanges = true;
        if (id != materialData.id)
            GlobalValues.isPendingChanges = true;
        if (indexLabel.text != id.ToString())
            GlobalValues.isPendingChanges = true;
        if (nameLabel.text != materialData.name)
            GlobalValues.isPendingChanges = true;

        if (albedoColor != GlobalValues.ColorToHex(materialData.material.color))
            GlobalValues.isPendingChanges = true;
        if (occlusion != materialData.material.GetFloat("_OcclusionStrength"))
            GlobalValues.isPendingChanges = true;

        if (metallicStrengh != materialData.material.GetFloat("_Metallic"))
            GlobalValues.isPendingChanges = true;
        if (smoothness != materialData.material.GetFloat("_Glossiness"))
            GlobalValues.isPendingChanges = true;
        if (smoothMin != materialData.material.GetFloat("_GlossMin"))
            GlobalValues.isPendingChanges = true;
        if (smoothMax != materialData.material.GetFloat("_GlossMax"))
            GlobalValues.isPendingChanges = true;

        if (normalStrengh != materialData.material.GetFloat("_BumpScale"))
            GlobalValues.isPendingChanges = true;
    }
}
