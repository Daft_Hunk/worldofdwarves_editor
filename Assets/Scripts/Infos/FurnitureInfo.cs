﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FurnitureInfo : MonoBehaviour
{
    public Furniture furniture;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField staminaRateInput;
    public TMP_InputField healthRateInput;
    public TMP_InputField hungerRateInput;
    public CostsInfo costsInfo;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != furniture.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != furniture.Description)
            isPendingChanges = true;
        if (staminaRateInput != null && staminaRateInput.text != furniture.StaminaRate.ToString())
            isPendingChanges = true;
        if (healthRateInput != null && healthRateInput.text != furniture.HealthRate.ToString())
            isPendingChanges = true;
        if (hungerRateInput != null && hungerRateInput.text != furniture.HungerRate.ToString())
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != furniture.Name)
            nameInput.text = furniture.Name;
        if (descriptionInput != null && descriptionInput.text != furniture.Description)
            descriptionInput.text = furniture.Description;
        if (staminaRateInput != null && staminaRateInput.text != furniture.StaminaRate.ToString())
            staminaRateInput.text = furniture.StaminaRate.ToString();
        if (healthRateInput != null && healthRateInput.text != furniture.HealthRate.ToString())
            healthRateInput.text = furniture.HealthRate.ToString();
        if (hungerRateInput != null && hungerRateInput.text != furniture.HungerRate.ToString())
            hungerRateInput.text = furniture.HungerRate.ToString();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != furniture.Name)
            furniture.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != furniture.Description)
            furniture.Description = descriptionInput.text;
        if (staminaRateInput != null && staminaRateInput.text != furniture.StaminaRate.ToString())
            furniture.StaminaRate = float.Parse(staminaRateInput.text);
        if (healthRateInput != null && healthRateInput.text != furniture.HealthRate.ToString())
            furniture.HealthRate = float.Parse(healthRateInput.text);
        if (hungerRateInput != null && hungerRateInput.text != furniture.HungerRate.ToString())
            furniture.HungerRate = float.Parse(hungerRateInput.text);

        if (costsInfo != null)
        {
            List<CostInfo> costsToDelete = new List<CostInfo>();

            foreach (CostInfo cost in costsInfo.costs)
            {
                if (cost.transform.parent == UIManager.Instance.toDeleteParent)
                    costsToDelete.Add(cost);
                else
                {
                    if (cost.isNew)
                        furniture.resourcesNeeded.Add(cost.resourceAmount);
                    cost.SaveChanges();
                }
            }
            foreach (CostInfo cost in costsToDelete)
            {
                furniture.resourcesNeeded.Remove(cost.resourceAmount);
                costsInfo.costs.Remove(cost);
            }
        }
    }
}
