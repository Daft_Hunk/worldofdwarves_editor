﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CostsInfo : MonoBehaviour {

    public ItemInfo itemInfo;
    public List<CostInfo> costs = new List<CostInfo>();
    public Button addButton;

    public void RevertChanges()
    {
        List<CostInfo> itemsToDestroy = costs.FindAll(x => x.isNew == true);

        foreach (CostInfo cost in costs)
        {
            cost.RevertChanges();
        }

        foreach (CostInfo itemToDestroy in itemsToDestroy)
        {
            costs.Remove(itemToDestroy);
        }
        itemInfo.isPendingChanges = true;
    }

    public void AddCost()
    {
        GlobalValues.currentParent = this.transform;
        int id = costs.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceAmount rA = new ResourceAmount(0, 0, 0);

        CostInfo cost = UIManager.Instance.CostConstructor(id, rA);
        cost.isNew = true;
        cost.parent = this.transform;
        costs.Add(cost);

        itemInfo.isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }
}
