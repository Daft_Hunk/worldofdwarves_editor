using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UnitInfo : MonoBehaviour
{
    public Unit unit;

    public TMP_InputField nameInput;
    public TMP_InputField descriptionInput;
    public TMP_InputField staminaInput;
    public TMP_InputField healthInput;
    public TMP_InputField hungerInput;
    public TMP_InputField speedInput;

    public bool CheckForChange()
    {
        bool isPendingChanges = false;

        if (nameInput != null && nameInput.text != unit.Name)
            isPendingChanges = true;
        if (descriptionInput != null && descriptionInput.text != unit.Description)
            isPendingChanges = true;
        if (staminaInput != null && staminaInput.text != unit.Stamina.ToString())
            isPendingChanges = true;
        if (healthInput != null && healthInput.text != unit.Health.ToString())
            isPendingChanges = true;
        if (hungerInput != null && hungerInput.text != unit.Hunger.ToString())
            isPendingChanges = true;
        if (speedInput != null && speedInput.text != unit.Speed.ToString())
            isPendingChanges = true;

        return isPendingChanges;
    }

    public void RevertChanges()
    {
        if (nameInput != null && nameInput.text != unit.Name)
            nameInput.text = unit.Name;
        if (descriptionInput != null && descriptionInput.text != unit.Description)
            descriptionInput.text = unit.Description;
        if (staminaInput != null && staminaInput.text != unit.Stamina.ToString())
            staminaInput.text = unit.Stamina.ToString();
        if (healthInput != null && healthInput.text != unit.Health.ToString())
            healthInput.text = unit.Health.ToString();
        if (hungerInput != null && hungerInput.text != unit.Hunger.ToString())
            hungerInput.text = unit.Hunger.ToString();
        if (speedInput != null && speedInput.text != unit.Speed.ToString())
            speedInput.text = unit.Speed.ToString();
    }

    public void SaveChanges()
    {
        if (nameInput != null && nameInput.text != unit.Name)
            unit.Name = nameInput.text;
        if (descriptionInput != null && descriptionInput.text != unit.Description)
            unit.Description = descriptionInput.text;
        if (staminaInput != null && staminaInput.text != unit.Stamina.ToString())
            unit.Stamina = float.Parse(staminaInput.text);
        if (healthInput != null && healthInput.text != unit.Health.ToString())
            unit.Health = float.Parse(healthInput.text);
        if (hungerInput != null && hungerInput.text != unit.Hunger.ToString())
            unit.Hunger = float.Parse(hungerInput.text);
        if (speedInput != null && speedInput.text != unit.Speed.ToString())
            unit.Speed = float.Parse(speedInput.text);
    }
}
