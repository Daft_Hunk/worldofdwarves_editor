using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CostInfo : MonoBehaviour
{
    public Transform parent;
    public ResourceAmount resourceAmount;
    public int id;
    public int oldId;
    public bool isNew;

    public TMP_Text subtitle;
    public Button duplicateButton;
    public Button deleteButton;
    public TMP_Dropdown typeInput;
    public TMP_Dropdown idInput;
    public TMP_InputField amountInput;

    public void ChangeType(int _type)
    {
        idInput.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedResources = new List<TMP_Dropdown.OptionData>();

        foreach (Resource resource in GlobalValues.resources.OrderBy(x => x.Id))
        {
            if (resource.type == _type)
                orderedResources.Add(new TMP_Dropdown.OptionData(resource.Name, resource.Sprite));
        }
        idInput.AddOptions(orderedResources);
        idInput.value = 0;
        parent.GetComponent<CostsInfo>().itemInfo.isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
    }

    public void ChangeId(int _id)
    {
        if (_id != resourceAmount.Id)
        {
            parent.GetComponent<CostsInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void ChangeAmount(string _amount)
    {
        if (_amount != resourceAmount.Amount.ToString())
        {
            parent.GetComponent<CostsInfo>().itemInfo.isPendingChanges = true;
            GlobalValues.isPendingChanges = true;
        }
    }

    public void RevertChanges()
    {
        if (isNew)
            Destroy(this.gameObject);

        if (typeInput.value != resourceAmount.Type)
            typeInput.value = resourceAmount.Type;
        if (idInput.value != resourceAmount.Id)
            idInput.value = resourceAmount.Id;
        amountInput.text = resourceAmount.Amount.ToString();

        if (transform.parent == UIManager.Instance.toDeleteParent)
        {
            id = oldId;
            transform.SetParent(parent);
            this.transform.SetSiblingIndex(id);
        }
        parent.GetComponent<CostsInfo>().itemInfo.isPendingChanges = false;
    }

    public void Duplicate()
    {
        GlobalValues.currentParent = parent;
        CostsInfo costsParent = parent.GetComponent<CostsInfo>();
        int id = costsParent.costs.Where(x => x.transform.parent != UIManager.Instance.toDeleteParent).Count();
        ResourceAmount rA = new ResourceAmount(resourceAmount.Type, resourceAmount.Id, resourceAmount.Amount);

        CostInfo cost = UIManager.Instance.CostConstructor(id, rA);
        cost.isNew = true;
        cost.parent = parent;
        costsParent.costs.Add(cost);

        costsParent.itemInfo.isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void Delete()
    {
        this.transform.SetParent(UIManager.Instance.toDeleteParent);        

        foreach (CostInfo cost in parent.GetComponent<CostsInfo>().costs)
        {
            if (cost.id > id)
            {
                cost.id--;
                cost.subtitle.text = cost.id + "] Cost :";
            }
        }
        parent.GetComponent<CostsInfo>().itemInfo.isPendingChanges = true;
        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }

    public void SaveChanges()
    {
        if (typeInput != null && typeInput.value != resourceAmount.Type)
            resourceAmount.Type = typeInput.value;
        if (idInput != null && idInput.value != resourceAmount.Id)
            resourceAmount.Id = idInput.value;
        if (amountInput != null && amountInput.text != resourceAmount.Amount.ToString())
            resourceAmount.Amount = int.Parse(amountInput.text);
        if (isNew)
            isNew = false;
    }
}
