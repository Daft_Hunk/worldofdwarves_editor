﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingFurnituresInfo : MonoBehaviour
{
    public ItemInfo itemInfo;
    public List<BuildingFurnitureInfo> furnituresInfo = new List<BuildingFurnitureInfo>();

    public Transform insParent;
    public Transform outsParent;
    public Transform processesParent;
    public Transform optisParent;
    public Transform storagesParent;

    public void RevertChanges()
    {
        List<BuildingFurnitureInfo> itemsToDestroy = furnituresInfo.FindAll(x => x.isNew == true);

        foreach (BuildingFurnitureInfo furniture in furnituresInfo)
        {
            furniture.RevertChanges();
        }

        foreach (BuildingFurnitureInfo itemToDestroy in itemsToDestroy)
        {
            furnituresInfo.Remove(itemToDestroy);
        }
    }

    public void AddBuildingFurniture(int type)
    {
        string label = "";
        int id = 0;
        int uId = GlobalValues.furnitureUID++;
        Furniture furniture = new Furniture(uId, 0, "", "", 0, 0, 0, null, null, null);
        switch (type)
        {
            case 0:
                GlobalValues.currentParent = insParent;
                id = insParent.childCount;
                label = "In";
                furniture.InitiateType(0, 0);
                break;
            case 1:
                GlobalValues.currentParent = outsParent;
                id = outsParent.childCount;
                label = "Out";
                furniture.InitiateType(1, 0);
                break;
            case 2:
                GlobalValues.currentParent = processesParent;
                id = processesParent.childCount;
                label = "Process";
                furniture.InitiateType(2, 0);
                break;
            case 3:
                GlobalValues.currentParent = optisParent;
                id = optisParent.childCount;
                label = "Opti";
                furniture.InitiateType(3, 0);
                break;
            case 4:
                GlobalValues.currentParent = storagesParent;
                id = storagesParent.childCount;
                label = "Storage";
                furniture.InitiateType(4, 0);
                break;
        }
        BuildingFurnitureInfo buildingFurniture = UIManager.Instance.BuildingFurnitureConstructor(id, furniture, label);
        buildingFurniture.isNew = true;
        buildingFurniture.infosParent = this.transform;
        furnituresInfo.Add(buildingFurniture);

        GlobalValues.isPendingChanges = true;
        GameManager.Instance.MarkForRebuild();
    }
}
