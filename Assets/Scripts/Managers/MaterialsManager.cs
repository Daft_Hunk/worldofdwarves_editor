using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using uCPf;
using System.Linq;

public class MaterialsManager : MonoBehaviour
{
    public Transform materialViewer;
    public Renderer materialExemple;
    public TMP_Dropdown texturesDropdown;

    public MaterialInfo currentMaterialInfo;
    public TMP_Text titleInput;
    public TMP_InputField nameInput;

    public ColorPicker colorPicker;
    public Image albedoColor;
    public TMP_InputField hexColor;
    public Slider occlusionSlider;

    public Slider metallicSlider;
    public Slider smoothnessSlider;
    public Slider smoothMinSlider;
    public Slider smoothMaxSlider;

    public Slider normalSlider;

    public static MaterialsManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void ChangeMaterial(MaterialInfo material)
    {
        currentMaterialInfo = material;

        titleInput.text = currentMaterialInfo.nameLabel.text;
        nameInput.text = currentMaterialInfo.nameLabel.text;

        albedoColor.color = GlobalValues.HexToColor(currentMaterialInfo.albedoColor);
        hexColor.text = currentMaterialInfo.albedoColor;
        colorPicker.hsv = albedoColor.color;
        colorPicker.UpdateUI();
        occlusionSlider.value = currentMaterialInfo.occlusion;

        metallicSlider.value = currentMaterialInfo.metallicStrengh;
        smoothnessSlider.value = currentMaterialInfo.smoothness;
        smoothMinSlider.value = currentMaterialInfo.smoothMin;
        smoothMaxSlider.value = currentMaterialInfo.smoothMax;

        normalSlider.value = currentMaterialInfo.normalStrengh;

        materialExemple.material = currentMaterialInfo.materialData.material;

        texturesDropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedTextures = new List<TMP_Dropdown.OptionData>();
        int value = 0;

        foreach (TextureData texture in GlobalValues.textures.OrderBy(x => x.id))
        {
            Rect rect = new Rect(0, 0, texture.albedo.width, texture.albedo.height);
            orderedTextures.Add(new TMP_Dropdown.OptionData(texture.name, Sprite.Create(texture.albedo, rect, new Vector2(0,0))));

            if (currentMaterialInfo.textureData == texture)
            {
                value = texture.id;
            }
        }
        texturesDropdown.AddOptions(orderedTextures);
        texturesDropdown.value = value;

        GameManager.Instance.ChangeOrbitFocus(materialViewer.gameObject, materialViewer.Find("Focus"), AssetsManager.Instance.CalculateLocalBounds(materialExemple.gameObject));
        if (!materialViewer.gameObject.activeInHierarchy && GameManager.Instance.currentEditor == 2)
            materialViewer.gameObject.SetActive(true);
    }

    public void Update()
    {
        if (currentMaterialInfo != null)
        {
            if (occlusionSlider.value != materialExemple.material.GetFloat("_OcclusionStrength"))
                materialExemple.material.SetFloat("_OcclusionStrength", occlusionSlider.value);
            if (metallicSlider.value != materialExemple.material.GetFloat("_Metallic"))
                materialExemple.material.SetFloat("_Metallic", metallicSlider.value);
            if (smoothnessSlider.value != materialExemple.material.GetFloat("_Glossiness"))
                materialExemple.material.SetFloat("_Glossiness", smoothnessSlider.value);
            if (smoothMinSlider.value != materialExemple.material.GetFloat("_GlossMin"))
                materialExemple.material.SetFloat("_GlossMin", smoothMinSlider.value);
            if (smoothMaxSlider.value != materialExemple.material.GetFloat("_GlossMax"))
                materialExemple.material.SetFloat("_GlossMax", smoothMaxSlider.value);
            if (normalSlider.value != materialExemple.material.GetFloat("_BumpScale"))
                materialExemple.material.SetFloat("_BumpScale", normalSlider.value);
            currentMaterialInfo.ChangeValue();
        }
    }

    public void ChangeAlbedoColor(Color _color)
    {
        if (currentMaterialInfo != null)
        {
            materialExemple.material.SetColor("_Color", _color);
            hexColor.text = GlobalValues.ColorToHex(_color);
            currentMaterialInfo.CheckForChanges("");
        }
    }

    public void ChangeHexColor(string _hexValue)
    {
        if (currentMaterialInfo != null)
        {
            Color color = GlobalValues.HexToColor(_hexValue);

            materialExemple.material.SetColor("_Color", color);
            colorPicker.hsv = color;
            colorPicker.UpdateUI();
            currentMaterialInfo.CheckForChanges("");
        }
    }

    public void OnNameChanged(string _name)
    {
        titleInput.text = _name;
        currentMaterialInfo.nameLabel.text = _name;
        currentMaterialInfo.CheckForChanges("");
    }

    public void OnTextureDataChanged(int _index)
    {
        TextureData textureData = GlobalValues.textures.Find(x => x.id == _index);
        materialExemple.material.SetTexture("_MainTex", textureData.albedo);
        materialExemple.material.SetTexture("_MetallicGlossMap", textureData.metallic);
        materialExemple.material.SetTexture("_BumpMap", textureData.normal);
        currentMaterialInfo.textureData = textureData;
        currentMaterialInfo.CheckForChanges("");
    }

    private void OnDisable()
    {
        if(materialViewer != null)
            materialViewer.gameObject.SetActive(false);
    }
}
