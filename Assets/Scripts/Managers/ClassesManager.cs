using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ClassesManager : MonoBehaviour
{
    public static ClassesManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void ResourcesConstructor()
    {
        //Minerals Construction
        GlobalValues.currentParent = UIManager.Instance.contentParents[0];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Resource resource in GlobalValues.resources.Where(x => x.Type == 0).OrderBy(x => x.Id))
        {
            UIManager.Instance.ResourceUIConstructor(resource);
        }
        UIManager.Instance.CheckForInteractableMoves();

        //Gems Construction
        GlobalValues.currentParent = UIManager.Instance.contentParents[1];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Resource resource in GlobalValues.resources.Where(x => x.Type == 1).OrderBy(x => x.Id))
        {
            UIManager.Instance.ResourceUIConstructor(resource);
        }
        UIManager.Instance.CheckForInteractableMoves();

        //Resources Construction
        GlobalValues.currentParent = UIManager.Instance.contentParents[2];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Resource resource in GlobalValues.resources.Where(x => x.Type == 2).OrderBy(x => x.Id))
        {
            UIManager.Instance.ResourceUIConstructor(resource);
        }
        UIManager.Instance.CheckForInteractableMoves();

        //Crafted Construction
        GlobalValues.currentParent = UIManager.Instance.contentParents[3];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Resource resource in GlobalValues.resources.Where(x => x.Type == 3).OrderBy(x => x.Id))
        {
            UIManager.Instance.ResourceUIConstructor(resource);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void FurnituresConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[4];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Furniture furniture in GlobalValues.furnitures.OrderBy(x => x.Id))
        {
            UIManager.Instance.FurnitureUIConstructor(furniture);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void FacilitiesConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[5];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Facility facility in GlobalValues.facilities.OrderBy(x => x.Id))
        {
            UIManager.Instance.FacilityUIConstructor(facility);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void DecorationsConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[6];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Decoration decoration in GlobalValues.decorations.OrderBy(x => x.Id))
        {
            UIManager.Instance.DecorationUIConstructor(decoration);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void BuildingsConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[7];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Building building in GlobalValues.buildings.OrderBy(x => x.Id))
        {
            UIManager.Instance.BuildingUIConstructor(building);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void ExteriorJobsConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[8];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (ExteriorJob exteriorJob in GlobalValues.exteriorJobs.OrderBy(x => x.Id))
        {
            UIManager.Instance.ExteriorJobUIConstructor(exteriorJob);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void WeaponsConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[9];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Weapon weapon in GlobalValues.weapons.OrderBy(x => x.Id))
        {
            UIManager.Instance.WeaponUIConstructor(weapon);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }

    public void UnitsConstructor()
    {
        GlobalValues.currentParent = UIManager.Instance.contentParents[10];
        GlobalValues.contents.Add(GlobalValues.currentParent.gameObject.AddComponent<ContentInfo>());

        foreach (Unit unit in GlobalValues.units.OrderBy(x => x.Id))
        {
            UIManager.Instance.UnitUIConstructor(unit);
        }
        UIManager.Instance.CheckForInteractableMoves();
    }
}
