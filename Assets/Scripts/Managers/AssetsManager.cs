using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using SimpleJSON;

public class AssetsManager : MonoBehaviour
{
    public Material defaultMaterial;
    public Texture2D defaultNormal;
    public GameObject defaultModel;
    public GameObject defaultPrebab;

    public Transform textureEditorContent;
    public GameObject textureElement;

    public Transform materialEditorContent;
    public GameObject materialElement;

    public Transform modelEditorContent;
    public GameObject modelElement;
    public Transform modelsPool;

    public List<TextureInfo> textureInfos = new List<TextureInfo>();
    public List<MaterialInfo> materialInfos = new List<MaterialInfo>();
    public List<ModelInfo> modelInfos = new List<ModelInfo>();

    private DirectoryInfo subDirectory;

    public static AssetsManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void StartAssetsLoading()
    {
        subDirectory = new DirectoryInfo(GlobalValues.modPath + Lex.JSON_ASSETS_FOLDER);
        StartCoroutine(LoadTextures());
        //ObjReader.use.ConvertFile("file://D:/Projects/WODMods/CoreMod/Assets/Models/Bench.obj", false);
    }

    public IEnumerator LoadTextures()
    {
        string texturesJSON = GlobalValues.FileRead(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[0] + Lex.EXT_JSON);

        if (texturesJSON != null && texturesJSON != "")
        {
            var N = JSON.Parse(texturesJSON);
            string root = Lex.ASS_CATS[0];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;

                string texFolder = GlobalValues.modPath + Lex.ASS_TEXTURES + _name + "/";

                Texture2D albedo = AssetsTextureLoader(texFolder + _name + Lex.TEX_EXT[0], false);
                yield return new WaitForEndOfFrame();
                Texture2D metallic = AssetsTextureLoader(texFolder + _name + Lex.TEX_EXT[1], false);
                yield return new WaitForEndOfFrame();
                Texture2D normal = AssetsTextureLoader(texFolder + _name + Lex.TEX_EXT[2], false);
                yield return new WaitForEndOfFrame();

                TextureData textureData = new TextureData(_id, _name, albedo, metallic, normal);
                GlobalValues.textures.Add(textureData);
                textureInfos.Add(CreateTextureElement(textureData));
            }
        }
        StartCoroutine(LoadMaterials());
    }

    public IEnumerator LoadMaterials()
    {
        string materialsJSON = GlobalValues.FileRead(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[1] + Lex.EXT_JSON);

        if (materialsJSON != null && materialsJSON != "")
        {
            var N = JSON.Parse(materialsJSON);
            string root = Lex.ASS_CATS[1];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                TextureData texture = GlobalValues.textures.Find(x => x.id == N[root][i][Lex.MAT_PARAMS[0]].AsInt);

                Material material = new Material(defaultMaterial);
                material.SetTexture("_MainTex", texture.albedo);
                material.SetColor("_Color", GlobalValues.HexToColor(N[root][i][Lex.MAT_PARAMS[1]].Value));
                material.SetFloat("_OcclusionStrength", N[root][i][Lex.MAT_PARAMS[2]].AsFloat);
                material.SetTexture("_MetallicGlossMap", texture.metallic);
                material.SetFloat("_Metallic", N[root][i][Lex.MAT_PARAMS[3]].AsFloat);
                material.SetFloat("_Glossiness", N[root][i][Lex.MAT_PARAMS[4]].AsFloat);
                material.SetFloat("_GlossMin", N[root][i][Lex.MAT_PARAMS[5]].AsFloat);
                material.SetFloat("_GlossMax", N[root][i][Lex.MAT_PARAMS[6]].AsFloat);
                material.SetTexture("_BumpMap", texture.normal);
                material.SetFloat("_BumpScale", N[root][i][Lex.MAT_PARAMS[7]].AsFloat);

                MaterialData materialData = new MaterialData(_id, _name, material, texture);
                GlobalValues.materials.Add(materialData);
                materialInfos.Add(CreateMaterialElement(materialData));
                yield return new WaitForEndOfFrame();
            }
        }
        StartCoroutine(LoadModels());
    }

    public IEnumerator LoadModels()
    {
        string modelsJSON = GlobalValues.FileRead(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[2] + Lex.EXT_JSON);

        if (modelsJSON != null && modelsJSON != "")
        {
            var N = JSON.Parse(modelsJSON);
            string root = Lex.ASS_CATS[2];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                float _scale = N[root][i][Lex.LBL_SCALE].AsFloat;

                ObjReader.ObjData objData;
                objData = ObjReader.use.ConvertFileAsync("file:///" + GlobalValues.modPath + Lex.ASS_MODELS + _name + Lex.EXT_OBJ, false, defaultMaterial);

                while (!objData.isDone)
                {
                    yield return null;
                }

                GameObject model = new GameObject();
                for (int j = 0; j < objData.gameObjects.Length; j++)
                {
                    objData.gameObjects[j].transform.SetParent(model.transform);
                    objData.gameObjects[j].name = "Mesh" + j;
                }

                List<MeshData> _meshesData = new List<MeshData>();
                MeshRenderer[] modelMeshes = model.GetComponentsInChildren<MeshRenderer>();

                for(int j = 0; j < modelMeshes.Length; j++)
                {
                    int id = N[root][i][Lex.MODEL_PARAMS[0]][j][Lex.MODEL_PARAMS[1]].AsInt;
                    int matId = N[root][i][Lex.MODEL_PARAMS[0]][j][Lex.MODEL_PARAMS[2]].AsInt;

                    MaterialData materialData = GlobalValues.materials.Find(x => x.id == matId);
                    modelMeshes[j].material = materialData.material;
                    modelMeshes[j].transform.localScale = new Vector3(1f, 1f, 1f);

                    _meshesData.Add(new MeshData(id, modelMeshes[j], materialData));
                }
                ModelData modelData = new ModelData(_id, _name, _scale, model, _meshesData);

                model.name = modelData.name;
                GlobalValues.models.Add(modelData);
                model.transform.SetParent(modelsPool);
                model.SetActive(false);
                modelInfos.Add(CreateModelElement(modelData));
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public IEnumerator LoadPrefabs()
    {
        string prefabsJSON = GlobalValues.FileRead(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[3] + Lex.EXT_JSON);

        /*foreach (MeshRenderer mesh in GlobalValues.prefabs[0].model.meshes)
        {
            mesh.material = GlobalValues.materials[0].material;
        }*/
        yield return new WaitForEndOfFrame();
    }

    public TextureInfo CreateTextureElement(TextureData texture)
    {
        GameObject element = Instantiate(textureElement) as GameObject;
        element.transform.SetParent(textureEditorContent, false);
        element.name = texture.name.ToString();

        element.GetComponent<TextureInfo>().TextureConstructor(texture);
        return element.GetComponent<TextureInfo>();
    }

    public MaterialInfo CreateMaterialElement(MaterialData material)
    {
        GameObject element = Instantiate(materialElement) as GameObject;
        element.transform.SetParent(materialEditorContent, false);
        element.name = material.name.ToString();

        element.GetComponent<MaterialInfo>().MaterialConstructor(material);
        return element.GetComponent<MaterialInfo>();
    }

    public ModelInfo CreateModelElement(ModelData _model)
    {
        GameObject element = Instantiate(modelElement) as GameObject;
        element.transform.SetParent(modelEditorContent, false);
        element.name = _model.name.ToString();

        ModelInfo modelInfo = element.GetComponent<ModelInfo>();
        modelInfo.ModelConstructor(_model);

        return modelInfo;
    }

    public Vector3 CalculateLocalBounds(GameObject _model)
    {
        Quaternion currentRotation = _model.transform.rotation;
        _model.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        Bounds bounds = new Bounds(_model.transform.position, Vector3.zero);

        foreach (Renderer renderer in _model.GetComponentsInChildren<Renderer>())
        {
            bounds.Encapsulate(renderer.bounds);
        }

        Vector3 localCenter = bounds.center - _model.transform.position;
        bounds.center = localCenter;

        _model.transform.rotation = currentRotation;
        return bounds.center;
    }

    public Texture2D AssetsTextureLoader(string path, bool isNormal)
    {
        Texture2D texture = null;
        texture = TextureLoader.LoadTexture(path + Lex.EXT_PNG, isNormal);

        if (texture == null)
        {
            texture = TextureLoader.LoadTexture(path + Lex.EXT_JPG, isNormal);
            if (texture == null)
            {
                texture = TextureLoader.LoadTexture(path + Lex.EXT_JPEG, isNormal);
                if (texture == null)
                    Debug.Log(path + "\n" + Lex.ER_MISSING);
            }
        }
        return texture;
    }
}
