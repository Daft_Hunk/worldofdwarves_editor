using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Linq;
using System.Text;

public class JSONManager : MonoBehaviour
{
    public static JSONManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public IEnumerator StartJSONExtraction(string path)
    {
        GlobalValues.furnitureUID = 0;
        GlobalValues.modPath = path;

        ResourcesConstructor(GlobalValues.FileRead(Lex.JSON_PATHS[0] + Lex.ITEMS_CATS[0] + Lex.LG_ENG + Lex.EXT_JSON), Lex.ITEMS_CATS[0], 0);
        yield return new WaitForEndOfFrame();
        ResourcesConstructor(GlobalValues.FileRead(Lex.JSON_PATHS[1] + Lex.ITEMS_CATS[1] + Lex.LG_ENG + Lex.EXT_JSON), Lex.ITEMS_CATS[1], 1);
        yield return new WaitForEndOfFrame();
        ResourcesConstructor(GlobalValues.FileRead(Lex.JSON_PATHS[2] + Lex.ITEMS_CATS[2] + Lex.LG_ENG + Lex.EXT_JSON), Lex.ITEMS_CATS[2], 2);
        yield return new WaitForEndOfFrame();
        ResourcesConstructor(GlobalValues.FileRead(Lex.JSON_PATHS[3] + Lex.ITEMS_CATS[3] + Lex.LG_ENG + Lex.EXT_JSON), Lex.ITEMS_CATS[3], 3);
        yield return new WaitForEndOfFrame();
        ClassesManager.Instance.ResourcesConstructor();
        yield return new WaitForEndOfFrame();

        FurnituresConstructor();
        yield return new WaitForEndOfFrame();
        FacilitiesConstructor();
        yield return new WaitForEndOfFrame();
        DecorationsConstructor();
        yield return new WaitForEndOfFrame();
        BuildingsConstructor();
        yield return new WaitForEndOfFrame();
        ExteriorJobsConstructor();
        yield return new WaitForEndOfFrame();
        WeaponsConstructor();
        yield return new WaitForEndOfFrame();
        UnitsConstructor();
        yield return new WaitForEndOfFrame();
        StartCoroutine(GameManager.Instance.StopWaitingPanel());
    }

    private void ResourcesConstructor(string json, string root, int type)
    {
        if (json != null && json != "")
        {
            var N = JSON.Parse(json);
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                int _basePrice = N[root][i][Lex.LBL_BASE_PRICE].AsInt;
                int _rarity = 0;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[type] + _id);
                Texture2D _decal = null;
                switch (type)
                {
                    case 0:
                        _decal = GameManager.TextureLoader(GlobalValues.modPath + Lex.DEC_MINERALS + _id);
                        _rarity = N[root][i][Lex.LBL_RARITY].AsInt;
                        break;
                    case 1:
                        _decal = GameManager.TextureLoader(GlobalValues.modPath + Lex.DEC_GEMS + _id);
                        _rarity = N[root][i][Lex.LBL_RARITY].AsInt;
                        break;
                }
                Resource _resource = new Resource(type, _id, _name, _rarity, _basePrice, _description, _sprite, _decal);

                if (type == 3)
                {
                    for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                    {
                        _resource.resourcesNeeded.Add(JSONCost(N, i, root, j));
                    }
                }
                GlobalValues.resources.Add(_resource);
            }
        }
        else
            Debug.LogWarning(root + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void FurnituresConstructor()
    {
        string furnituresJSON = GlobalValues.FileRead(Lex.JSON_PATHS[4] + Lex.ITEMS_CATS[4] + Lex.LG_ENG + Lex.EXT_JSON);


        if (furnituresJSON != null && furnituresJSON != "")
        {
            var N = JSON.Parse(furnituresJSON);
            string root = Lex.ITEMS_CATS[4];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                float _staminaRate = N[root][i][Lex.LBL_STAMINA_RATE].AsFloat;
                float _healthRate = N[root][i][Lex.LBL_HEALTH_RATE].AsFloat;
                float _hungerRate = N[root][i][Lex.LBL_HUNGER_RATE].AsFloat;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[4] + _id);
                GameObject _prefab = Resources.Load<GameObject>("Furnitures/Prefabs/" + _id);
                Sprite _minimapIcon = Resources.Load<Sprite>("Furnitures/Icon/" + _id);

                Furniture _furniture = new Furniture(GlobalValues.furnitureUID, _id, _name, _description, _staminaRate, _healthRate, _hungerRate, _sprite, _prefab, _minimapIcon);
                GlobalValues.furnitureUID++;

                for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                {
                    _furniture.resourcesNeeded.Add(JSONCost(N, i, root, j));
                }
                GlobalValues.furnitures.Add(_furniture);
            }
            ClassesManager.Instance.FurnituresConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[4] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void FacilitiesConstructor()
    {
        string facilitiesJSON = GlobalValues.FileRead(Lex.JSON_PATHS[5] + Lex.ITEMS_CATS[5] + Lex.LG_ENG + Lex.EXT_JSON);

        if (facilitiesJSON != null && facilitiesJSON != "")
        {
            var N = JSON.Parse(facilitiesJSON);
            string root = Lex.ITEMS_CATS[5];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[5] + _id);
                GameObject _prefab = Resources.Load<GameObject>("Facilities/Prefabs/" + _id);
                int _dwarfCapacity = N[root][i][Lex.LBL_DWARF_CAP].AsInt;
                float _extractionTime = N[root][i][Lex.LBL_EXTRACT_TIME].AsFloat;
                int _extractionAmount = N[root][i][Lex.LBL_EXTRACT_AMOUNT].AsInt;

                Facility _facility = new Facility(_id, _name, _description, _sprite, _prefab, _dwarfCapacity, _extractionTime, _extractionAmount);

                for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                {
                    _facility.resourcesNeeded.Add(JSONCost(N, i, root, j));
                }
                GlobalValues.facilities.Add(_facility);
            }
            ClassesManager.Instance.FacilitiesConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[5] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void DecorationsConstructor()
    {
        string decorationsJSON = GlobalValues.FileRead(Lex.JSON_PATHS[6] + Lex.ITEMS_CATS[6] + Lex.LG_ENG + Lex.EXT_JSON);

        if (decorationsJSON != null && decorationsJSON != "")
        {
            var N = JSON.Parse(decorationsJSON);
            string root = Lex.ITEMS_CATS[6];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                bool _isExterior = N[root][i][Lex.LBL_IS_EXT].AsBool;
                bool _isInterior = N[root][i][Lex.LBL_IS_INT].AsBool;
                bool _isWall = N[root][i][Lex.LBL_IS_WALL].AsBool;
                float _staminaRate = N[root][i][Lex.LBL_STAMINA_RATE].AsFloat;
                float _healthRate = N[root][i][Lex.LBL_HEALTH_RATE].AsFloat;
                float _hungerRate = N[root][i][Lex.LBL_HUNGER_RATE].AsFloat;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[6] + _id);
                GameObject _prefab = Resources.Load<GameObject>("Decorations/Prefabs/" + _id);

                Decoration _decoration = new Decoration(_id, _name, _description, _isExterior, _isInterior, _isWall, _staminaRate, _healthRate, _hungerRate, _sprite, _prefab);

                for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                {
                    _decoration.resourcesNeeded.Add(JSONCost(N, i, root, j));
                }
                GlobalValues.decorations.Add(_decoration);
            }
            ClassesManager.Instance.DecorationsConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[6] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void BuildingsConstructor()
    {
        string buildingsJSON = GlobalValues.FileRead(Lex.JSON_PATHS[7] + Lex.ITEMS_CATS[7] + Lex.LG_ENG + Lex.EXT_JSON);

        if (buildingsJSON != null && buildingsJSON != "")
        {
            var N = JSON.Parse(buildingsJSON);
            string root = Lex.ITEMS_CATS[7];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[7] + _id);
                Material _floorMaterial = Resources.Load("Buildings/Floors/" + _id, typeof(Material)) as Material;

                Building _building = new Building(_id, _name, _description, _sprite, _floorMaterial);

                for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                {
                    _building.resourcesNeeded.Add(JSONCost(N, i, root, j));
                }

                string branch0 = Lex.LBL_INS;
                for (int j = 0; j < N[root][i][branch0].Count; j++)
                {
                    Furniture furniture = GlobalValues.furnitures.Find(x => x.Id == N[root][i][branch0][j][Lex.LBL_ID].AsInt);
                    Furniture furnitureIn = new Furniture(GlobalValues.furnitureUID, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.hungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
                    furnitureIn.InitiateType(0, N[root][i][branch0][j][Lex.LBL_CAP].AsInt);

                    for (int k = 0; k < N[root][i][branch0][j][Lex.LBL_RESOURCES_ALLOWED].Count; k++)
                    {
                        furnitureIn.resourcesAllowed.Add(JSONFilter(N, root, i, branch0, j, k));
                    }
                    _building.furnituresIn.Add(furnitureIn);
                    GlobalValues.furnitureUID++;
                }

                string branch1 = Lex.LBL_OUTS;
                for (int j = 0; j < N[root][i][branch1].Count; j++)
                {
                    Furniture furniture = GlobalValues.furnitures.Find(x => x.Id == N[root][i][branch1][j][Lex.LBL_ID].AsInt);
                    Furniture furnitureOut = new Furniture(GlobalValues.furnitureUID, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.hungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
                    furnitureOut.InitiateType(1, N[root][i][branch1][j][Lex.LBL_CAP].AsInt);

                    for (int k = 0; k < N[root][i][branch1][j][Lex.LBL_RESOURCES_ALLOWED].Count; k++)
                    {
                        furnitureOut.resourcesAllowed.Add(JSONFilter(N, root, i, branch1, j, k));
                    }
                    _building.furnituresOut.Add(furnitureOut);
                    GlobalValues.furnitureUID++;
                }

                string branch2 = Lex.LBL_PROCESSES;
                for (int j = 0; j < N[root][i][branch2].Count; j++)
                {
                    Furniture furniture = GlobalValues.furnitures.Find(x => x.Id == N[root][i][branch2][j][Lex.LBL_ID].AsInt);
                    Furniture furnitureProcess = new Furniture(GlobalValues.furnitureUID, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.hungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
                    furnitureProcess.InitiateType(2, 0);

                    for (int k = 0; k < N[root][i][branch2][j][Lex.LBL_RESOURCES_RECIPES].Count; k++)
                    {
                        furnitureProcess.resourcesRecipes.Add(JSONRecipe(N, root, i, branch2, j, k));
                    }
                    _building.furnituresProcess.Add(furnitureProcess);
                    GlobalValues.furnitureUID++;
                }

                string branch3 = Lex.LBL_OPTIS;
                for (int j = 0; j < N[root][i][branch3].Count; j++)
                {
                    Furniture furniture = GlobalValues.furnitures.Find(x => x.Id == N[root][i][branch3][j][Lex.LBL_ID].AsInt);
                    Furniture furnitureOpti = new Furniture(GlobalValues.furnitureUID, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.hungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
                    furnitureOpti.InitiateType(3, 0);

                    for (int k = 0; k < N[root][i][branch3][j][Lex.LBL_RESOURCES_ALLOWED].Count; k++)
                    {
                        furnitureOpti.resourcesAllowed.Add(JSONFilter(N, root, i, branch3, j, k));
                    }
                    _building.furnituresOpti.Add(furnitureOpti);
                    GlobalValues.furnitureUID++;
                }

                string branch4 = Lex.LBL_STORAGES;
                for (int j = 0; j < N[root][i][branch4].Count; j++)
                {
                    Furniture furniture = GlobalValues.furnitures.Find(x => x.Id == N[root][i][branch4][j][Lex.LBL_ID].AsInt);
                    Furniture furnitureStorage = new Furniture(GlobalValues.furnitureUID, furniture.Id, furniture.Name, furniture.Description, furniture.StaminaRate, furniture.HealthRate, furniture.hungerRate, furniture.Sprite, furniture.Prefab, furniture.MinimapIcon);
                    furnitureStorage.InitiateType(4, N[root][i][branch4][j][Lex.LBL_CAP].AsInt);

                    if (N[root][i][branch4][j][Lex.LBL_RESOURCES_ALLOWED].Count > 0)
                    {
                        for (int k = 0; k < N[root][i][branch4][j][Lex.LBL_RESOURCES_ALLOWED].Count; k++)
                        {
                            furnitureStorage.resourcesAllowed.Add(JSONFilter(N, root, i, branch4, j, k));
                        }
                    }
                    else
                    {
                        foreach (Resource resource in GlobalValues.resources)
                        {
                            furnitureStorage.resourcesAllowed.Add(new ResourceFilter(resource.Type, resource.Id, 100));
                        }
                    }
                    _building.furnituresStorage.Add(furnitureStorage);
                    GlobalValues.furnitureUID++;
                }
                GlobalValues.buildings.Add(_building);
            }
            ClassesManager.Instance.BuildingsConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[7] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void ExteriorJobsConstructor()
    {
        string exteriorJobsJSON = GlobalValues.FileRead(Lex.JSON_PATHS[8] + Lex.ITEMS_CATS[8] + Lex.LG_ENG + Lex.EXT_JSON);

        if (exteriorJobsJSON != null && exteriorJobsJSON != "")
        {
            var N = JSON.Parse(exteriorJobsJSON);
            string root = Lex.ITEMS_CATS[8];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                int _extractionTime = N[root][i][Lex.LBL_EXTRACT_TIME].AsInt;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[8] + _id);

                ExteriorJob exteriorJob = new ExteriorJob(_id, _name, _description, _extractionTime, _sprite);

                for (int j = 0; j < N[root][i][Lex.LBL_RESOURCES_ALLOWED].Count; j++)
                {
                    int _resourceId = N[root][i][Lex.LBL_RESOURCES_ALLOWED][j][Lex.LBL_ID].AsInt;
                    int _resourceRarity = N[root][i][Lex.LBL_RESOURCES_ALLOWED][j][Lex.LBL_PERCENT].AsInt;

                    exteriorJob.resources.Add(new ResourceFilter(2, _resourceId, _resourceRarity));
                }
                GlobalValues.exteriorJobs.Add(exteriorJob);
            }
            ClassesManager.Instance.ExteriorJobsConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[8] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void WeaponsConstructor()
    {
        string weaponsJSON = GlobalValues.FileRead(Lex.JSON_PATHS[9] + Lex.ITEMS_CATS[9] + Lex.LG_ENG + Lex.EXT_JSON);

        if (weaponsJSON != null && weaponsJSON != "")
        {
            var N = JSON.Parse(weaponsJSON);
            string root = Lex.ITEMS_CATS[9];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                int _basePrice = N[root][i][Lex.LBL_BASE_PRICE].AsInt;
                float _damage = N[root][i][Lex.LBL_DAMAGE].AsFloat;
                float _speed = N[root][i][Lex.LBL_SPEED].AsFloat;
                bool _isRanged = N[root][i][Lex.LBL_IS_RANGED].AsBool;
                bool _isOneHanded = N[root][i][Lex.LBL_IS_ONE_HANDED].AsBool;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[9] + _id);

                Weapon _weapon = new Weapon(_id, _name, _description, _basePrice, _damage, _speed, _isRanged, _isOneHanded, _sprite);

                for (int j = 0; j < N[root][i][Lex.LBL_COSTS].Count; j++)
                {
                    _weapon.resourcesNeeded.Add(JSONCost(N, i, root, j));
                }
                GlobalValues.weapons.Add(_weapon);
            }
            ClassesManager.Instance.WeaponsConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[9] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private void UnitsConstructor()
    {
        string unitsJSON = GlobalValues.FileRead(Lex.JSON_PATHS[10] + Lex.ITEMS_CATS[10] + Lex.LG_ENG + Lex.EXT_JSON);

        if (unitsJSON != null && unitsJSON != "")
        {
            var N = JSON.Parse(unitsJSON);
            string root = Lex.ITEMS_CATS[10];
            for (int i = 0; i < N[root].Count; i++)
            {
                int _id = N[root][i][Lex.LBL_ID].AsInt;
                string _name = N[root][i][Lex.LBL_NAME].Value;
                string _description = N[root][i][Lex.LBL_DESC].Value;
                float _health = N[root][i][Lex.LBL_HEALTH].AsFloat;
                float _stamina = N[root][i][Lex.LBL_STAMINA].AsFloat;
                float _hunger = N[root][i][Lex.LBL_HUNGER].AsFloat;
                float _speed = N[root][i][Lex.LBL_SPEED].AsFloat;
                Sprite _sprite = GameManager.SpriteLoader(GlobalValues.modPath + Lex.SP_PATHS[10] + _id);
                //GameObject _prefab = Resources.Load<GameObject>("Decorations/Prefabs/" + _id);

                Unit _unit = new Unit(_id, _name, _description, _health, _stamina, _hunger, _speed, _sprite);
                GlobalValues.units.Add(_unit);
            }
            ClassesManager.Instance.UnitsConstructor();
        }
        else
            Debug.LogWarning(Lex.ITEMS_CATS[10] + Lex.LG_ENG + Lex.EXT_JSON + Lex.ER_MISSING);
    }

    private ResourceAmount JSONCost(JSONNode N, int i, string root, int j)
    {
        int _typeCost = N[root][i][Lex.LBL_COSTS][j][Lex.LBL_TYPE].AsInt;
        int _resourceIDCost = N[root][i][Lex.LBL_COSTS][j][Lex.LBL_ID].AsInt;
        int _amountCost = N[root][i][Lex.LBL_COSTS][j][Lex.LBL_AMOUNT].AsInt;

        ResourceAmount resourceAmount = new ResourceAmount(_typeCost, _resourceIDCost, _amountCost);
        return resourceAmount;
    }

    private ResourceFilter JSONFilter(JSONNode N, string root, int i, string branch, int j, int k)
    {
        int _resourceType = N[root][i][branch][j][Lex.LBL_RESOURCES_ALLOWED][k][Lex.LBL_TYPE].AsInt;
        int _resourceId = N[root][i][branch][j][Lex.LBL_RESOURCES_ALLOWED][k][Lex.LBL_ID].AsInt;
        int _resourcePercent = N[root][i][branch][j][Lex.LBL_RESOURCES_ALLOWED][k][Lex.LBL_PERCENT].AsInt;

        ResourceFilter resourceFilter = new ResourceFilter(_resourceType, _resourceId, _resourcePercent);
        return resourceFilter;
    }

    private ResourceRecipe JSONRecipe(JSONNode N, string root, int i, string branch, int j, int k)
    {
        int _resourceIDRecipe = N[root][i][branch][j][Lex.LBL_RESOURCES_RECIPES][k][Lex.LBL_ID].AsInt;
        float _resourceSpeedRecipe = N[root][i][branch][j][Lex.LBL_RESOURCES_RECIPES][k][Lex.LBL_SPEED].AsFloat;
        bool _resourceIsAutoCrafted = N[root][i][branch][j][Lex.LBL_RESOURCES_RECIPES][k][Lex.LBL_IS_AUTO_CRAFTED].AsBool;

        ResourceRecipe resourceRecipe = new ResourceRecipe(_resourceIDRecipe, _resourceIsAutoCrafted, _resourceSpeedRecipe);
        return resourceRecipe;
    }

    public void CreateJSON()
    {
        MineralsCreation();
        GemsCreation();
        ResourcesCreation();
        CraftedCreation();
        FurnituresCreation();
        FacilitiesCreation();
        DecorationCreation();
        BuildingsCreation();
        ExteriorJobsCreation();
        WeaponsCreation();
        UnitsCreation();

        TexturesCreation();
        MaterialsCreation();
        ModelsCreation();
    }

    private void MineralsCreation()
    {
        StringBuilder mineralsJSON = new StringBuilder();
        StartJSON(mineralsJSON, Lex.ITEMS_CATS[0]);

        int i = GlobalValues.resources.Where(x => x.Type == 0).Count();
        foreach (Resource mineral in GlobalValues.resources.Where(x => x.Type == 0).OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(mineralsJSON);

            mineralsJSON.AppendLine(JSONFormator(Lex.LBL_ID, mineral.Id) + ",");
            mineralsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, mineral.Name) + ",");
            mineralsJSON.AppendLine(JSONFormator(Lex.LBL_RARITY, mineral.Rarity) + ",");
            mineralsJSON.AppendLine(JSONFormator(Lex.LBL_BASE_PRICE, mineral.BasePrice) + ",");
            mineralsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, mineral.Description));

            JSONEndSubCategory(mineralsJSON, i);
        }
        EndJSON(mineralsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[0] + Lex.ITEMS_CATS[0] + Lex.LG_ENG + Lex.EXT_JSON, mineralsJSON.ToString());
    }

    private void GemsCreation()
    {
        StringBuilder gemsJSON = new StringBuilder();
        StartJSON(gemsJSON, Lex.ITEMS_CATS[1]);

        int i = GlobalValues.resources.Where(x => x.Type == 1).Count();
        foreach (Resource gem in GlobalValues.resources.Where(x => x.Type == 1).OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(gemsJSON);

            gemsJSON.AppendLine(JSONFormator(Lex.LBL_ID, gem.Id) + ",");
            gemsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, gem.Name) + ",");
            gemsJSON.AppendLine(JSONFormator(Lex.LBL_RARITY, gem.Rarity) + ",");
            gemsJSON.AppendLine(JSONFormator(Lex.LBL_BASE_PRICE, gem.BasePrice) + ",");
            gemsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, gem.Description));

            JSONEndSubCategory(gemsJSON, i);
        }
        EndJSON(gemsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[1] + Lex.ITEMS_CATS[1] + Lex.LG_ENG + Lex.EXT_JSON, gemsJSON.ToString());
    }

    private void ResourcesCreation()
    {
        StringBuilder resourcesJSON = new StringBuilder();
        StartJSON(resourcesJSON, Lex.ITEMS_CATS[2]);

        int i = GlobalValues.resources.Where(x => x.Type == 2).Count();
        foreach (Resource resource in GlobalValues.resources.Where(x => x.Type == 2).OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(resourcesJSON);

            resourcesJSON.AppendLine(JSONFormator(Lex.LBL_ID, resource.Id) + ",");
            resourcesJSON.AppendLine(JSONFormator(Lex.LBL_NAME, resource.Name) + ",");
            resourcesJSON.AppendLine(JSONFormator(Lex.LBL_BASE_PRICE, resource.BasePrice) + ",");
            resourcesJSON.AppendLine(JSONFormator(Lex.LBL_DESC, resource.Description));

            JSONEndSubCategory(resourcesJSON, i);
        }
        EndJSON(resourcesJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[2] + Lex.ITEMS_CATS[2] + Lex.LG_ENG + Lex.EXT_JSON, resourcesJSON.ToString());
    }

    private void CraftedCreation()
    {
        StringBuilder craftedJSON = new StringBuilder();
        StartJSON(craftedJSON, Lex.ITEMS_CATS[3]);

        int i = GlobalValues.resources.Where(x => x.Type == 3).Count();
        foreach (Resource crafted in GlobalValues.resources.Where(x => x.Type == 3).OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(craftedJSON);

            craftedJSON.AppendLine(JSONFormator(Lex.LBL_ID, crafted.Id) + ",");
            craftedJSON.AppendLine(JSONFormator(Lex.LBL_NAME, crafted.Name) + ",");
            craftedJSON.AppendLine(JSONFormator(Lex.LBL_DESC, crafted.Description) + ",");
            craftedJSON.AppendLine(JSONFormator(Lex.LBL_BASE_PRICE, crafted.BasePrice) + ",");
            JSONCosts(craftedJSON, crafted.resourcesNeeded);

            JSONEndSubCategory(craftedJSON, i);
        }
        EndJSON(craftedJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[3] + Lex.ITEMS_CATS[3] + Lex.LG_ENG + Lex.EXT_JSON, craftedJSON.ToString());
    }

    private void FurnituresCreation()
    {
        StringBuilder furnituresJSON = new StringBuilder();
        StartJSON(furnituresJSON, Lex.ITEMS_CATS[4]);

        int i = GlobalValues.furnitures.Count();
        foreach (Furniture furniture in GlobalValues.furnitures.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(furnituresJSON);

            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_NAME, furniture.Name) + ",");
            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_DESC, furniture.Description) + ",");
            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_STAMINA_RATE, furniture.StaminaRate) + ",");
            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_HEALTH_RATE, furniture.HealthRate) + ",");
            furnituresJSON.AppendLine(JSONFormator(Lex.LBL_HUNGER_RATE, furniture.HungerRate) + ",");
            JSONCosts(furnituresJSON, furniture.resourcesNeeded);

            JSONEndSubCategory(furnituresJSON, i);
        }
        EndJSON(furnituresJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[4] + Lex.ITEMS_CATS[4] + Lex.LG_ENG + Lex.EXT_JSON, furnituresJSON.ToString());
    }    

    private void FacilitiesCreation()
    {
        StringBuilder facilitiesJSON = new StringBuilder();
        StartJSON(facilitiesJSON, Lex.ITEMS_CATS[5]);

        int i = GlobalValues.facilities.Count();
        foreach (Facility facility in GlobalValues.facilities.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(facilitiesJSON);

            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_ID, facility.Id) + ",");
            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_NAME, facility.Name) + ",");
            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_DESC, facility.Description) + ",");
            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_DWARF_CAP, facility.DwarfCapacity) + ",");
            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_EXTRACT_TIME, facility.ExtractionTime) + ",");
            facilitiesJSON.AppendLine(JSONFormator(Lex.LBL_EXTRACT_AMOUNT, facility.ExtractionAmount) + ",");
            JSONCosts(facilitiesJSON, facility.resourcesNeeded);

            JSONEndSubCategory(facilitiesJSON, i);
        }
        EndJSON(facilitiesJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[5] + Lex.ITEMS_CATS[5] + Lex.LG_ENG + Lex.EXT_JSON, facilitiesJSON.ToString());
    }

    private void DecorationCreation()
    {
        StringBuilder decorationsJSON = new StringBuilder();
        StartJSON(decorationsJSON, Lex.ITEMS_CATS[6]);

        int i = GlobalValues.decorations.Count();
        foreach (Decoration decoration in GlobalValues.decorations.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(decorationsJSON);

            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_ID, decoration.Id) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, decoration.Name) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, decoration.Description) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_IS_EXT, decoration.IsExterior) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_IS_INT, decoration.IsInterior) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_IS_WALL, decoration.IsWall) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_STAMINA_RATE, decoration.StaminaRate) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_HEALTH_RATE, decoration.HealthRate) + ",");
            decorationsJSON.AppendLine(JSONFormator(Lex.LBL_HUNGER_RATE, decoration.HungerRate) + ",");
            JSONCosts(decorationsJSON, decoration.resourcesNeeded);

            JSONEndSubCategory(decorationsJSON, i);
        }
        EndJSON(decorationsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[6] + Lex.ITEMS_CATS[6] + Lex.LG_ENG + Lex.EXT_JSON, decorationsJSON.ToString());
    }

    private void BuildingsCreation()
    {
        StringBuilder buildingsJSON = new StringBuilder();
        StartJSON(buildingsJSON, Lex.ITEMS_CATS[7]);

        int i = GlobalValues.buildings.Count();
        foreach (Building building in GlobalValues.buildings.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(buildingsJSON);

            buildingsJSON.AppendLine(JSONFormator(Lex.LBL_ID, building.Id) + ",");
            buildingsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, building.Name) + ",");
            buildingsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, building.Description) + ",");
            JSONFurnitures(buildingsJSON, building);
            JSONCosts(buildingsJSON, building.resourcesNeeded);

            JSONEndSubCategory(buildingsJSON, i);
        }
        EndJSON(buildingsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[7] + Lex.ITEMS_CATS[7] + Lex.LG_ENG + Lex.EXT_JSON, buildingsJSON.ToString());
    }

    private void ExteriorJobsCreation()
    {
        StringBuilder exteriorJobsJSON = new StringBuilder();
        StartJSON(exteriorJobsJSON, Lex.ITEMS_CATS[8]);

        int i = GlobalValues.exteriorJobs.Count();
        foreach (ExteriorJob exteriorJob in GlobalValues.exteriorJobs.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(exteriorJobsJSON);

            exteriorJobsJSON.AppendLine(JSONFormator(Lex.LBL_ID, exteriorJob.Id) + ",");
            exteriorJobsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, exteriorJob.Name) + ",");
            exteriorJobsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, exteriorJob.Description) + ",");
            exteriorJobsJSON.AppendLine(JSONFormator(Lex.LBL_EXTRACT_TIME, exteriorJob.ExtractionTime) + ",");
            JSONFilters(exteriorJobsJSON, exteriorJob.resources);

            JSONEndSubCategory(exteriorJobsJSON, i);
        }
        EndJSON(exteriorJobsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[8] + Lex.ITEMS_CATS[8] + Lex.LG_ENG + Lex.EXT_JSON, exteriorJobsJSON.ToString());
    }

    private void WeaponsCreation()
    {
        StringBuilder weaponsJSON = new StringBuilder();
        StartJSON(weaponsJSON, Lex.ITEMS_CATS[9]);

        int i = GlobalValues.weapons.Count();
        foreach (Weapon weapon in GlobalValues.weapons.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(weaponsJSON);

            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_ID, weapon.Id) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, weapon.Name) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, weapon.Description) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_BASE_PRICE, weapon.BasePrice) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_DAMAGE, weapon.Damage) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_SPEED, weapon.Speed) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_IS_RANGED, weapon.IsRanged) + ",");
            weaponsJSON.AppendLine(JSONFormator(Lex.LBL_IS_ONE_HANDED, weapon.IsOneHanded) + ",");
            JSONCosts(weaponsJSON, weapon.resourcesNeeded);

            JSONEndSubCategory(weaponsJSON, i);
        }
        EndJSON(weaponsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[9] + Lex.ITEMS_CATS[9] + Lex.LG_ENG + Lex.EXT_JSON, weaponsJSON.ToString());
    }

    private void UnitsCreation()
    {
        StringBuilder unitsJSON = new StringBuilder();
        StartJSON(unitsJSON, Lex.ITEMS_CATS[10]);

        int i = GlobalValues.units.Count();
        foreach (Unit unit in GlobalValues.units.OrderBy(x => x.Id))
        {
            i--;
            JSONStartSubCategory(unitsJSON);

            unitsJSON.AppendLine(JSONFormator(Lex.LBL_ID, unit.Id) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, unit.Name) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_DESC, unit.Description) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_STAMINA, unit.Stamina) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_HEALTH, unit.Health) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_HUNGER, unit.Hunger) + ",");
            unitsJSON.AppendLine(JSONFormator(Lex.LBL_SPEED, unit.Speed));

            JSONEndSubCategory(unitsJSON, i);
        }
        EndJSON(unitsJSON);
        GlobalValues.FileWrite(Lex.JSON_PATHS[10] + Lex.ITEMS_CATS[10] + Lex.LG_ENG + Lex.EXT_JSON, unitsJSON.ToString());
    }

    private void TexturesCreation()
    {
        StringBuilder texturesJSON = new StringBuilder();
        StartJSON(texturesJSON, Lex.ASS_CATS[0]);

        int i = GlobalValues.textures.Count();
        foreach (TextureData texture in GlobalValues.textures.OrderBy(x => x.id))
        {
            i--;
            JSONStartSubCategory(texturesJSON);

            texturesJSON.AppendLine(JSONFormator(Lex.LBL_ID, texture.id) + ",");
            texturesJSON.AppendLine(JSONFormator(Lex.LBL_NAME, texture.name));

            JSONEndSubCategory(texturesJSON, i);
        }
        EndJSON(texturesJSON);
        GlobalValues.FileWrite(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[0] + Lex.EXT_JSON, texturesJSON.ToString());
    }

    private void MaterialsCreation()
    {
        StringBuilder materialsJSON = new StringBuilder();
        StartJSON(materialsJSON, Lex.ASS_CATS[1]);

        int i = GlobalValues.materials.Count();
        foreach (MaterialData material in GlobalValues.materials.OrderBy(x => x.id))
        {
            i--;
            JSONStartSubCategory(materialsJSON);

            materialsJSON.AppendLine(JSONFormator(Lex.LBL_ID, material.id) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, material.name) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[0], material.textureData.id) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[1], GlobalValues.ColorToHex(material.material.color)) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[2], material.material.GetFloat("_OcclusionStrength")) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[3], material.material.GetFloat("_Metallic")) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[4], material.material.GetFloat("_Glossiness")) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[5], material.material.GetFloat("_GlossMin")) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[6], material.material.GetFloat("_GlossMax")) + ",");
            materialsJSON.AppendLine(JSONFormator(Lex.MAT_PARAMS[7], material.material.GetFloat("_BumpScale")));

            JSONEndSubCategory(materialsJSON, i);
        }
        EndJSON(materialsJSON);
        GlobalValues.FileWrite(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[1] + Lex.EXT_JSON, materialsJSON.ToString());
    }

    private void ModelsCreation()
    {
        StringBuilder modelsJSON = new StringBuilder();
        StartJSON(modelsJSON, Lex.ASS_CATS[2]);

        int i = GlobalValues.models.Count();
        foreach(ModelData model in GlobalValues.models.OrderBy(x => x.id))
        {
            i--;
            JSONStartSubCategory(modelsJSON);

            modelsJSON.AppendLine(JSONFormator(Lex.MODEL_PARAMS[1], model.id) + ",");
            modelsJSON.AppendLine(JSONFormator(Lex.LBL_NAME, model.name) + ",");
            modelsJSON.AppendLine(JSONFormator(Lex.MODEL_PARAMS[3], model.scale) + ",");
            JSONMeshes(modelsJSON, model.meshesData);

            JSONEndSubCategory(modelsJSON, i);
        }
        EndJSON(modelsJSON);
        GlobalValues.FileWrite(Lex.JSON_ASSETS_FOLDER + Lex.ASS_CATS[2] + Lex.EXT_JSON, modelsJSON.ToString());
    }

    private void StartJSON(StringBuilder jsonString, string label)
    {
        jsonString.AppendLine(Tab() + "{");
        GlobalValues.currentTab++;
        JSONStartList(jsonString, label);
    }

    private void EndJSON(StringBuilder jsonString)
    {
        JSONEndList(jsonString);
        GlobalValues.currentTab--;
        jsonString.AppendLine("");
        jsonString.AppendLine(Tab() + "}");
    }

    private void JSONStartList(StringBuilder jsonString, string label)
    {
        jsonString.Append(Tab() + "\"" + label + "\": [");
    }

    private void JSONEndList(StringBuilder jsonString)
    {
        jsonString.Append("]");
    }

    private void JSONStartSubCategory(StringBuilder jsonString)
    {
        jsonString.AppendLine("{");
        GlobalValues.currentTab++;
    }

    private void JSONEndSubCategory(StringBuilder jsonString, int index)
    {
        GlobalValues.currentTab--;
        jsonString.Append(Tab() + "}");
        if (index > 0)
            jsonString.Append(",");
    }

    private void JSONCosts(StringBuilder jsonString, List<ResourceAmount> costs)
    {
        JSONStartList(jsonString, Lex.LBL_COSTS);
        int i = costs.Count;

        foreach (ResourceAmount cost in costs)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_TYPE, cost.Type) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, cost.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_AMOUNT, cost.Amount));

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine("");
    }

    private void JSONFilters(StringBuilder jsonString, List<ResourceFilter> filters)
    {
        JSONStartList(jsonString, Lex.LBL_RESOURCES_ALLOWED);
        int i = filters.Count;

        foreach (ResourceFilter filter in filters)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_TYPE, filter.Type) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, filter.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_PERCENT, filter.Percent));

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine("");
    }

    private void JSONRecipes(StringBuilder jsonString, List<ResourceRecipe> recipes)
    {
        JSONStartList(jsonString, Lex.LBL_RESOURCES_RECIPES);
        int i = recipes.Count;

        foreach (ResourceRecipe recipe in recipes)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, recipe.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_IS_AUTO_CRAFTED, recipe.IsAutoCrafted) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_SPEED, recipe.Speed));

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine("");
    }

    private void JSONMeshes(StringBuilder jsonString, List<MeshData> meshes)
    {
        JSONStartList(jsonString, Lex.MODEL_PARAMS[0]);
        int i = meshes.Count;

        foreach (MeshData mesh in meshes)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, mesh.id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_NAME, mesh.mesh.name) + ",");
            jsonString.AppendLine(JSONFormator(Lex.MODEL_PARAMS[2], mesh.materialData.id));

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine("");
    }

    private void JSONFurnitures(StringBuilder jsonString, Building building)
    {
        //In Construction
        JSONStartList(jsonString, Lex.LBL_INS);

        int i = building.furnituresIn.Count;
        foreach (Furniture furniture in building.furnituresIn)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_CAP, furniture.Capacity) + ",");
            JSONFilters(jsonString, furniture.resourcesAllowed);

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine(",");

        //Out Construction
        JSONStartList(jsonString, Lex.LBL_OUTS);

        i = building.furnituresOut.Count;
        foreach (Furniture furniture in building.furnituresOut)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_CAP, furniture.Capacity) + ",");
            JSONFilters(jsonString, furniture.resourcesAllowed);

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine(",");

        //Process Construction
        JSONStartList(jsonString, Lex.LBL_PROCESSES);

        i = building.furnituresProcess.Count;
        foreach (Furniture furniture in building.furnituresProcess)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            JSONRecipes(jsonString, furniture.resourcesRecipes);

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine(",");

        //Opti Construction
        JSONStartList(jsonString, Lex.LBL_OPTIS);

        i = building.furnituresOpti.Count;
        foreach (Furniture furniture in building.furnituresOpti)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            JSONFilters(jsonString, furniture.resourcesAllowed);

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine(",");

        //Storage Construction
        JSONStartList(jsonString, Lex.LBL_STORAGES);

        i = building.furnituresStorage.Count;
        foreach (Furniture furniture in building.furnituresStorage)
        {
            i--;
            JSONStartSubCategory(jsonString);

            jsonString.AppendLine(JSONFormator(Lex.LBL_ID, furniture.Id) + ",");
            jsonString.AppendLine(JSONFormator(Lex.LBL_CAP, furniture.Capacity) + ",");
            JSONFilters(jsonString, furniture.resourcesAllowed);

            JSONEndSubCategory(jsonString, i);
        }
        JSONEndList(jsonString);
        jsonString.AppendLine(",");
    }

    private string JSONFormator(string label, string value)
    {
        string jsonElement = "";
        jsonElement = Tab() + "\"" + label + "\": \"" + value + "\"";
        return jsonElement;
    }

    private string JSONFormator(string label, float value)
    {
        string jsonElement = "";
        jsonElement = Tab() + "\"" + label + "\": " + value;
        return jsonElement;
    }

    private string JSONFormator(string label, int value)
    {
        string jsonElement = "";
        jsonElement = Tab() + "\"" + label + "\": " + value;
        return jsonElement;
    }

    private string JSONFormator(string label, bool value)
    {
        string jsonElement = "";
        if (value)
            jsonElement = Tab() + "\"" + label + "\": true";
        else
            jsonElement = Tab() + "\"" + label + "\": false";
        return jsonElement;
    }

    private string Tab()
    {
        string tab = "";
        for (int i = 0; i < GlobalValues.currentTab; i++)
        {
            tab += "  ";
        }
        return tab;
    }
}
