﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserPrefsManager : MonoBehaviour
{
    public static UserPrefsManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void GetUserPrefs()
    {
        GlobalValues.modPath = PlayerPrefs.GetString("modPath", Application.dataPath);
    }

    public void SaveUserPrefs()
    {        
        PlayerPrefs.SetString("modPath", GlobalValues.modPath);

        PlayerPrefs.Save();
    }    
}
