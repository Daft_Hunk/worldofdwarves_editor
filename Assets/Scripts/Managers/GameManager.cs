using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using SFB;
using System.IO;
using TMPro;

public class GameManager : MonoBehaviour
{
    public int currentEditor = 0;
    public GameObject[] editors;

    public Button upButton;
    public Button downButton;
    public Button saveButton;
    public Button revertButton;
    public Button addButton;

    public TMP_Text categoryLabel;
    public GameObject infoBox;

    public int tabIndex = 0;
    public Button[] tabs;
    public GameObject[] panels;

    public GameObject waitingPanel;

    public Sprite mineralsSprite;
    public Sprite gemsSprite;
    public Sprite resourcesSprite;
    public Sprite craftedSprite;

    public static GameManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void Start()
    {
        GlobalValues.currentPanel = 0;
        categoryLabel.text = Lex.ITEMS_CATS[0];

        upButton.interactable = false;
        downButton.interactable = false;
        addButton.interactable = false;


        tabs[0].interactable = false;
        for (int i = 1; i < tabs.Length; i++)
        {
            tabs[i].interactable = true;
        }

        tabIndex = 1;
        DecalTabs(true);

        panels[0].SetActive(true);
        for (int i = 1; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }

        for (int i = 1; i < editors.Length; i++)
        {
            editors[i].SetActive(false);
        }
        waitingPanel.SetActive(false);
    }

    public void Update()
    {
        if (GlobalValues.isPendingChanges)
        {
            saveButton.interactable = true;
            revertButton.interactable = true;
        }
        else
        {
            saveButton.interactable = false;
            revertButton.interactable = false;
        }
        if (infoBox.activeInHierarchy)
            infoBox.transform.position = new Vector3(Input.mousePosition.x + 1, Input.mousePosition.y + 1);
    }

    public void PanelChange(int i)
    {
        StartCoroutine(ChangePanel(i));
    }

    public void RevertChanges()
    {
        //JSON Items Reverting
        foreach (ContentInfo content in GlobalValues.contents)
        {
            List<ItemInfo> itemsToDestroy = content.items.FindAll(x => x.isNew == true);

            foreach (ItemInfo item in content.items)
                item.RevertChanges();
            
            foreach (ItemInfo itemToDestroy in itemsToDestroy)
                content.items.Remove(itemToDestroy);            
        }

        //Textures infos Reverting
        List<TextureInfo> texturesToDestroy = AssetsManager.Instance.textureInfos.FindAll(x => x.isNew == true);

        foreach (TextureInfo textureInfo in AssetsManager.Instance.textureInfos)        
            textureInfo.RevertChanges();
        
        foreach (TextureInfo textureToDestroy in texturesToDestroy)        
            AssetsManager.Instance.textureInfos.Remove(textureToDestroy);
        

        //Materials infos Reverting
        List<MaterialInfo> materialsToDestroy = AssetsManager.Instance.materialInfos.FindAll(x => x.isNew == true);

        foreach (MaterialInfo materialInfo in AssetsManager.Instance.materialInfos)        
            materialInfo.RevertChanges();
        
        foreach (MaterialInfo materialToDestroy in materialsToDestroy)        
            AssetsManager.Instance.materialInfos.Remove(materialToDestroy);
        
        if (MaterialsManager.Instance.currentMaterialInfo.isNew)
            MaterialsManager.Instance.ChangeMaterial(AssetsManager.Instance.materialInfos[0]);

        //Models infos Reverting
        List<ModelInfo> modelsToDestroy = AssetsManager.Instance.modelInfos.FindAll(x => x.isNew == true);

        foreach(ModelInfo modelInfo in AssetsManager.Instance.modelInfos)        
            modelInfo.RevertChanges();
        
        foreach(ModelInfo modelToDestroy in modelsToDestroy) 
            AssetsManager.Instance.modelInfos.Remove(modelToDestroy);
        
        if (ModelsManager.Instance.currentModelInfo.isNew)
            ModelsManager.Instance.ChangeModel(AssetsManager.Instance.modelInfos[0]);

        GlobalValues.isPendingChanges = false;
    }

    public void NewMod()
    {

    }

    public void OpenMod()
    {
        UserPrefsManager.Instance.GetUserPrefs();
        var paths = StandaloneFileBrowser.OpenFolderPanel(Lex.MESS_MOD_FOLDER, GlobalValues.modPath, false);
        if (paths.Count() > 0 && paths[0] != null)
        {
            addButton.interactable = true;
            StartCoroutine(JSONManager.Instance.StartJSONExtraction(paths[0]));
            UserPrefsManager.Instance.SaveUserPrefs();
            AssetsManager.Instance.StartAssetsLoading();
        }
    }

    public void SaveChanges()
    {
        //JSON Items Saving
        List<ItemInfo> itemsToDelete = new List<ItemInfo>();

        foreach (ContentInfo content in GlobalValues.contents)
        {
            foreach (ItemInfo item in content.items)
            {
                if (item.transform.parent == UIManager.Instance.toDeleteParent)
                    itemsToDelete.Add(item);
                item.SaveChanges();
            }
            foreach (ItemInfo itemToDelete in itemsToDelete)            
                content.items.Remove(itemToDelete);
            
            itemsToDelete.Clear();
        }

        //Textures infos Saving
        List<TextureInfo> texturesToDelete = new List<TextureInfo>();

        foreach (TextureInfo textureInfo in AssetsManager.Instance.textureInfos)
        {
            if (textureInfo.transform.parent == UIManager.Instance.toDeleteParent)
                texturesToDelete.Add(textureInfo);
            textureInfo.SaveChanges();
        }
        foreach(TextureInfo textureToDelete in texturesToDelete)        
            AssetsManager.Instance.textureInfos.Remove(textureToDelete);
        
        texturesToDelete.Clear();

        //Materials infos Saving
        List<MaterialInfo> materialsToDelete = new List<MaterialInfo>();

        foreach (MaterialInfo materialInfo in AssetsManager.Instance.materialInfos)
        {
            if (materialInfo.transform.parent == UIManager.Instance.toDeleteParent)
                materialsToDelete.Add(materialInfo);
            materialInfo.SaveChanges();
        }
        foreach (MaterialInfo materialToDelete in materialsToDelete)        
            AssetsManager.Instance.materialInfos.Remove(materialToDelete);
        
        materialsToDelete.Clear();

        //Models infos Saving 
        List<ModelInfo> modelsToDelete = new List<ModelInfo>();

        foreach(ModelInfo modelInfo in AssetsManager.Instance.modelInfos)
        {
            if (modelInfo.transform.parent == UIManager.Instance.toDeleteParent)
                modelsToDelete.Add(modelInfo);
            modelInfo.SaveChanges();
        }
        foreach(ModelInfo modelToDelete in modelsToDelete)        
            AssetsManager.Instance.modelInfos.Remove(modelToDelete);
        
        modelsToDelete.Clear();

        //Final Cleaning & JSON Creation
        GlobalValues.isPendingChanges = false;

        foreach (Transform child in UIManager.Instance.toDeleteParent)        
            Destroy(child.gameObject);
        
        JSONManager.Instance.CreateJSON();
    }

    public void AddNewItem()
    {
        switch (currentEditor)
        {
            case 0:
                {
                    GlobalValues.currentParent = UIManager.Instance.contentParents[GlobalValues.currentPanel];
                    AddNewJSONItem(UIManager.Instance.contentParents[GlobalValues.currentPanel].GetComponent<ContentInfo>().items.Count());
                    UIManager.Instance.CheckForInteractableMoves();
                    break;
                }
            case 1:
                {
                    int _id = AssetsManager.Instance.textureInfos.Count;
                    TextureData textureData = new TextureData(_id, "New Texture", null, null, null);

                    TextureInfo textureInfo = AssetsManager.Instance.CreateTextureElement(textureData);
                    textureInfo.isNew = true;
                    AssetsManager.Instance.textureInfos.Add(textureInfo);
                    break;
                }
            case 2:
                {
                    int _id = AssetsManager.Instance.materialInfos.Count;
                    MaterialData materialData = new MaterialData(_id, "New Material", AssetsManager.Instance.defaultMaterial, GlobalValues.textures[0]);

                    MaterialInfo materialInfo = AssetsManager.Instance.CreateMaterialElement(materialData);
                    materialInfo.isNew = true;
                    AssetsManager.Instance.materialInfos.Add(materialInfo);
                    break;
                }
            case 3:
                {
                    int _id = AssetsManager.Instance.modelInfos.Count;
                    List<MeshData> meshDatas = new List<MeshData>();
                    GameObject newModel = Instantiate(AssetsManager.Instance.defaultModel, AssetsManager.Instance.modelsPool);
                    int i = 0;
                    foreach(MeshRenderer mesh in newModel.GetComponentsInChildren<MeshRenderer>())
                    {
                        meshDatas.Add(new MeshData(i, mesh, GlobalValues.materials.First()));
                    }
                    ModelData modelData = new ModelData(_id, "New Model", 1, newModel, meshDatas);

                    ModelInfo modelInfo = AssetsManager.Instance.CreateModelElement(modelData);
                    modelInfo.isNew = true;
                    AssetsManager.Instance.modelInfos.Add(modelInfo);

                    ModelsManager.Instance.ChangeModel(modelInfo);
                    break;
                }
            case 4:
                {

                    break;
                }
        }
        
        GlobalValues.isPendingChanges = true;
    }

    private void AddNewJSONItem(int id)
    {
        switch (GlobalValues.currentPanel)
        {
            case 0:
                {
                    Resource _resource = new Resource(0, id, "", 0, 0, "", null, null);

                    ItemInfo item = UIManager.Instance.ResourceUIConstructor(_resource);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 1:
                {
                    Resource _resource = new Resource(1, id, "", 0, 0, "", null, null);

                    ItemInfo item = UIManager.Instance.ResourceUIConstructor(_resource);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 2:
                {
                    Resource _resource = new Resource(2, id, "", 0, 0, "", null, null);

                    ItemInfo item = UIManager.Instance.ResourceUIConstructor(_resource);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 3:
                {
                    Resource _resource = new Resource(3, id, "", 0, 0, "", null, null);

                    ItemInfo item = UIManager.Instance.ResourceUIConstructor(_resource);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 4:
                {
                    int uId = GlobalValues.furnitureUID++;
                    Furniture _furniture = new Furniture(uId, id, "", "", 0, 0, 0, null, null, null);

                    ItemInfo item = UIManager.Instance.FurnitureUIConstructor(_furniture);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 5:
                {
                    Building _building = new Building(id, "", "", null, null);

                    ItemInfo item = UIManager.Instance.BuildingUIConstructor(_building);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 6:
                {
                    Decoration _decoration = new Decoration(id, "", "", false, false, false, 0, 0, 0, null, null);

                    ItemInfo item = UIManager.Instance.DecorationUIConstructor(_decoration);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 7:
                {
                    Facility _facility = new Facility(id, "", "", null, null, 0, 0, 0);

                    ItemInfo item = UIManager.Instance.FacilityUIConstructor(_facility);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 8:
                {
                    ExteriorJob _exteriorJob = new ExteriorJob(id, "", "", 0, null);

                    ItemInfo item = UIManager.Instance.ExteriorJobUIConstructor(_exteriorJob);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 9:
                {
                    Weapon _weapon = new Weapon(id, "", "", 0, 0, 0, false, false, null);

                    ItemInfo item = UIManager.Instance.WeaponUIConstructor(_weapon);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
            case 10:
                {
                    Unit _unit = new Unit(id, "", "", 0, 0, 0, 0, null);

                    ItemInfo item = UIManager.Instance.UnitUIConstructor(_unit);
                    item.isNew = true;
                    StartCoroutine(DelayedJumpToItem(item.GetComponent<RectTransform>()));
                    break;
                }
        }
    }

    public void MarkForRebuild()
    {
        panels[GlobalValues.currentPanel].SetActive(false);
        panels[GlobalValues.currentPanel].SetActive(true);
    }

    public void OpenSpriteEditor(ItemInfo item)
    {
        var extensions = new[] { new ExtensionFilter("Sprite File", "png", "jpg", "jpeg") };

        string subPath = Lex.SP_PATHS[item.type];

        var path = StandaloneFileBrowser.OpenFilePanel(Lex.MESS_SELECT_SPRITE, GlobalValues.modPath + subPath, extensions, false);
        if (path.Count() > 0)
            StartCoroutine(SpriteLoader(path[0], item));
    }

    public void OpenTextureEditor(ResourceInfo resourceInfo)
    {
        var extensions = new[] { new ExtensionFilter("Texture File", "png", "jpg", "jpeg") };

        string subPath = "";

        switch (resourceInfo.resource.Type)
        {
            case 0:
                subPath = Lex.DEC_MINERALS;
                break;
            case 1:
                subPath = Lex.DEC_GEMS;
                break;
        }

        var path = StandaloneFileBrowser.OpenFilePanel(Lex.MESS_SELECT_TEXTURE, GlobalValues.modPath + subPath, extensions, false);
        if (path.Count() > 0)
            StartCoroutine(TextureLoader(path[0], resourceInfo));
    }

    public IEnumerator SpriteLoader(string path, ItemInfo item)
    {
        WWW www = new WWW(path);
        yield return www;
        Sprite sprite = null;
        sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));

        if (sprite != null)
            item.ChangeSprite(sprite);
    }

    public static Sprite SpriteLoader(string path)
    {
        Sprite sprite = null;
        WWW www = new WWW(path + Lex.EXT_PNG);

        if (!string.IsNullOrEmpty(www.error) || www.texture == null)
        {
            www = new WWW(path + Lex.EXT_JPG);
            if (!string.IsNullOrEmpty(www.error) || www.texture == null)
            {
                www = new WWW(path + Lex.EXT_JPEG);
                if (!string.IsNullOrEmpty(www.error) || www.texture == null)
                    Debug.Log(www.url + "\n" + www.error);
                else
                    sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            }
            else
                sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
        else
            sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));

        return sprite;
    }

    public IEnumerator TextureLoader(string path, ResourceInfo resource)
    {
        WWW www = new WWW(path);
        yield return www;
        Texture2D texture = null;
        texture = www.texture;

        if (texture != null)
            resource.ChangeTexture(texture);
    }

    public static Texture2D TextureLoader(string path)
    {
        Texture2D texture = null;
        WWW www = new WWW(path + Lex.EXT_PNG);

        if (!string.IsNullOrEmpty(www.error) || www.texture == null)
        {
            www = new WWW(path + Lex.EXT_JPG);
            if (!string.IsNullOrEmpty(www.error) || www.texture == null)
            {
                www = new WWW(path + Lex.EXT_JPEG);
                if (!string.IsNullOrEmpty(www.error) || www.texture == null)
                    Debug.Log(www.url + "\n" + www.error);
                else
                    texture = www.texture;
            }
            else
                texture = www.texture;
        }
        else
            texture = www.texture;

        return texture;
    }

    public static void SaveSprite(string subPath, Sprite sprite)
    {
        File.WriteAllBytes(GlobalValues.modPath + subPath + Lex.EXT_PNG, sprite.texture.EncodeToPNG());
    }

    public static void SavePNGTexture(string subPath, Texture2D texture)
    {
        File.WriteAllBytes(GlobalValues.modPath + subPath + Lex.EXT_PNG, texture.EncodeToPNG());
    }

    public static void SaveJPGTexture(string subPath, Texture2D texture)
    {
        File.WriteAllBytes(GlobalValues.modPath + subPath + Lex.EXT_JPG, texture.EncodeToJPG());
    }

    public void StartWaitingPanel()
    {
        waitingPanel.SetActive(true);
    }

    public IEnumerator StopWaitingPanel()
    {
        yield return new WaitForEndOfFrame();
        waitingPanel.SetActive(false);
    }

    public IEnumerator ChangePanel(int i)
    {
        waitingPanel.SetActive(true);
        yield return new WaitForEndOfFrame();
        tabs[GlobalValues.currentPanel].interactable = true;
        yield return new WaitForEndOfFrame();
        panels[GlobalValues.currentPanel].SetActive(false);
        yield return new WaitForEndOfFrame();
        tabs[i].interactable = false;
        yield return new WaitForEndOfFrame();
        panels[i].SetActive(true);
        yield return new WaitForEndOfFrame();
        categoryLabel.text = Lex.ITEMS_CATS[i];

        GlobalValues.currentPanel = i;
        waitingPanel.SetActive(false);
    }

    public void DecalTabs(bool isUp)
    {
        if (isUp)
            tabIndex--;
        else
            tabIndex++;

        for (int i = 0; i < tabs.Length; i++)
        {
            if (i >= tabIndex && i < tabIndex + 10)
                tabs[i].gameObject.SetActive(true);
            else
                tabs[i].gameObject.SetActive(false);
        }

        if (tabIndex == 0)
            upButton.interactable = false;
        else
            upButton.interactable = true;
        if (tabIndex + 10 >= tabs.Length)
            downButton.interactable = false;
        else
            downButton.interactable = true;
    }

    public IEnumerator DelayedJumpToItem(RectTransform itemRect)
    {
        yield return new WaitForEndOfFrame();
        float itemMargin = itemRect.rect.height / 2 + 20;
        Vector3 newLocalPos = new Vector3(0, -itemRect.localPosition.y + itemMargin);
        UIManager.Instance.contentParents[GlobalValues.currentPanel].GetComponent<RectTransform>().localPosition = newLocalPos;
    }

    public void ShowInfoBox(string infoText)
    {
        if (!infoBox.activeInHierarchy)
        {
            infoBox.GetComponentInChildren<TMP_Text>().text = infoText;
            infoBox.SetActive(true);
        }
    }

    public void HideInfoBox()
    {
        infoBox.SetActive(false);
    }

    public void ChangeEditor(int _editor)
    {
        if (currentEditor != _editor)
        {
            editors[currentEditor].SetActive(false);
            currentEditor = _editor;
            editors[currentEditor].SetActive(true);

            switch (_editor)
            {
                case 2:
                    if (GlobalValues.materials.Count > 0)
                        editors[currentEditor].GetComponent<MaterialsManager>().ChangeMaterial(AssetsManager.Instance.materialInfos.First());
                    break;
                case 3:
                    if (GlobalValues.models.Count > 0)
                        editors[currentEditor].GetComponent<ModelsManager>().ChangeModel(AssetsManager.Instance.modelInfos.First());
                    break;
            }
        }
    }

    public void ChangeOrbitFocus(GameObject _target, Transform _targetFocus, Vector3 _center)
    {
        Camera.main.GetComponent<UBER_MouseOrbit_DynamicDistance>().target = _target;
        Camera.main.GetComponent<UBER_MouseOrbit_DynamicDistance>().targetFocus = _targetFocus;
        _targetFocus.position = _center;
        Camera.main.GetComponent<UBER_MouseOrbit_DynamicDistance>().Reset();
    }
}
