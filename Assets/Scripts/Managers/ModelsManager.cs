using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SFB;
using UnityEngine.UI;
using System.Linq;

public class ModelsManager : MonoBehaviour
{
    public Transform modelViewer;
    public GameObject currentModel;
    public GameObject sceneScaler;
    public GameObject meshElement;

    public ModelInfo currentModelInfo;
    public TMP_Text titleInput;
    public TMP_InputField nameInput;
    public TMP_InputField scaleInput;

    public Transform meshParent;
    public List<MeshInfo> meshElements = new List<MeshInfo>();

    public static ModelsManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public void ChangeModel(ModelInfo modelInfo)
    {
        if (currentModel != null)
            currentModel.SetActive(false);
        if(meshElements.Count > 0)
        {
            foreach (MeshInfo meshElement in meshElements)
            {
                Destroy(meshElement.gameObject);
            }
            meshElements.Clear();
        }
        currentModelInfo = modelInfo;

        titleInput.text = currentModelInfo.name;
        nameInput.text = currentModelInfo.name;

        currentModel = currentModelInfo.currentModel;

        currentModel.transform.SetParent(AssetsManager.Instance.modelsPool);
        scaleInput.text = modelInfo.scale.ToString();
        ChangeScale(modelInfo.scale.ToString());

        currentModel.SetActive(true);
        if (!modelViewer.gameObject.activeInHierarchy)
            modelViewer.gameObject.SetActive(true);

        foreach (MeshInfo meshInfo in currentModelInfo.meshInfos.OrderBy(x => x.id))
        {
            MeshElementConstructor(meshInfo);
        }
        CheckForChanges("");
    }

    public void OnNameChanged(string _name)
    {
        titleInput.text = _name;
        currentModelInfo.nameInput.text = _name;
        CheckForChanges("");
    }

    private void OnDisable()
    {
        if (modelViewer != null)
            modelViewer.gameObject.SetActive(false);
    }

    public void ToggleCubeScaler(bool isOn)
    {
        sceneScaler.SetActive(isOn);
    }

    public void ChangeScale(string _scale)
    {
        float scale = float.Parse(_scale);
        currentModel.transform.localScale = new Vector3(scale, scale, scale);
        currentModelInfo.scale = scale;
        GameManager.Instance.ChangeOrbitFocus(currentModel, modelViewer.Find("Focus"), AssetsManager.Instance.CalculateLocalBounds(currentModel));
        CheckForChanges("");
    }

    public void MeshElementConstructor(MeshInfo meshInfo)
    {
        GameObject meshElmt = Instantiate(meshElement);
        meshElmt.transform.SetParent(meshParent, false);
        meshElmt.name = "MeshElement";

        MeshInfo currentMeshInfo = meshElmt.GetComponent<MeshInfo>();
        currentMeshInfo.id = meshInfo.id;
        currentMeshInfo.mesh = meshInfo.mesh;
        currentMeshInfo.currentMaterialData = meshInfo.currentMaterialData;

        meshElmt.GetComponentInChildren<TMP_Text>().text = meshInfo.mesh.name;

        List<string> materials = new List<string>();
        foreach(MaterialData material in GlobalValues.materials)
        {
            materials.Add(material.name);
        }

        TMP_Dropdown dropdown = meshElmt.GetComponentInChildren<TMP_Dropdown>();
        dropdown.ClearOptions();
        dropdown.AddOptions(materials);
        dropdown.onValueChanged.RemoveAllListeners();
        dropdown.onValueChanged.AddListener(currentMeshInfo.ChangeMeshMaterial);
        dropdown.value = meshInfo.currentMaterialData.id;

        meshElmt.GetComponentInChildren<Toggle>().onValueChanged.RemoveAllListeners();
        meshElmt.GetComponentInChildren<Toggle>().onValueChanged.AddListener(currentMeshInfo.ToggleMesh);

        meshElements.Add(currentMeshInfo);
    }

    public void OpenModelPicker()
    {
        var extensions = new[] { new ExtensionFilter("Model File", "obj") };

        var path = StandaloneFileBrowser.OpenFilePanel(Lex.MESS_SELECT_MODEL, GlobalValues.modPath + Lex.ASS_MODELS, extensions, false);
        if (path.Length > 0)
        {
            currentModelInfo.currentModel.transform.SetParent(UIManager.Instance.toDeleteParent);
            StartCoroutine(LoadModel(path[0]));            
        }
    }

    public IEnumerator LoadModel(string _path)
    {
        MaterialData materialData = GlobalValues.materials[0];
        ObjReader.ObjData objData;
        objData = ObjReader.use.ConvertFileAsync("file:///" + _path, false, materialData.material);

        while (!objData.isDone)
        {
            yield return null;
        }

        GameObject model = new GameObject();
        for (int i = 0; i < objData.gameObjects.Length; i++)
        {
            objData.gameObjects[i].transform.SetParent(model.transform);
            objData.gameObjects[i].name = "Mesh" + i;
        }

        List<MeshInfo> _meshesInfo = new List<MeshInfo>();
        MeshRenderer[] modelMeshes = model.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < modelMeshes.Length; i++)
        {
            modelMeshes[i].material = materialData.material;
            modelMeshes[i].transform.localScale = new Vector3(1f, 1f, 1f);

            _meshesInfo.Add(new MeshInfo(i, modelMeshes[i], materialData));
        }
        model.name = currentModelInfo.modelData.name;
        model.transform.SetParent(AssetsManager.Instance.modelsPool);
        currentModelInfo.meshInfos = _meshesInfo;
        currentModelInfo.currentModel = model;
        ChangeModel(currentModelInfo);
    }

    public void CheckForChanges(string text)
    {
        currentModelInfo.CheckForChanges(text);
    }
}
