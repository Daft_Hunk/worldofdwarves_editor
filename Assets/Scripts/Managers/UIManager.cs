using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class UIManager : MonoBehaviour
{
    public Transform[] contentParents;

    public Transform toDeleteParent;

    public GameObject item;
    public GameObject costs;
    public GameObject cost;
    public GameObject titleElement;
    public GameObject subtitleGeneral;
    public GameObject subtitleElement;
    public GameObject inputBoolElement;
    public GameObject inputDropdownElement;
    public GameObject inputFloatElement;
    public GameObject inputIntElement;
    public GameObject inputTextElement;
    public GameObject inputSpriteElement;
    public GameObject inputTextureElement;
    public GameObject commandsElement;
    public GameObject buildingFurnitures;
    public GameObject buildingFurniture;

    public static UIManager Instance { get; private set; }

    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        // Here we save our singleton instance
        Instance = this;
    }

    public ItemInfo ResourceUIConstructor(Resource resource)
    {
        ItemInfo itemInfo = ItemConstruction(resource.Id);
        itemInfo.type = 0;
        itemInfo.sprite = resource.Sprite;
        ResourceInfo resourceInfo = itemInfo.gameObject.AddComponent<ResourceInfo>();

        UIConstructor(resource.Name);
        UIConstructor(Lex.LBL_SPRITE, resource.Sprite);
        UIConstructor(Lex.LBL_ID, resource.Id, false);
        resourceInfo.nameInput = UIConstructor(Lex.LBL_NAME, resource.Name);
        resourceInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, resource.Description);
        resourceInfo.basePriceInput = UIConstructor(Lex.LBL_BASE_PRICE, resource.BasePrice);

        switch (resource.Type)
        {
            case 0:
                resourceInfo.rarityInput = UIConstructor(Lex.LBL_RARITY, resource.Rarity);
                UIConstructor(Lex.LBL_DECAL, resource.Decal);
                break;
            case 1:
                resourceInfo.rarityInput = UIConstructor(Lex.LBL_RARITY, resource.Rarity);
                UIConstructor(Lex.LBL_DECAL, resource.Decal);
                break;
            case 2:
                break;
            case 3:
                CostsInfo costsInfo = CostsConstructor();
                costsInfo.itemInfo = itemInfo;
                resourceInfo.costsInfo = costsInfo;

                int j = 0;
                foreach (ResourceAmount cost in resource.resourcesNeeded)
                {
                    GlobalValues.currentParent = costsInfo.transform;
                    CostInfo costInfo = CostConstructor(j, cost);
                    costInfo.parent = costsInfo.transform;
                    costsInfo.costs.Add(costInfo);
                    j++;
                }
                GlobalValues.currentParent = costsInfo.itemInfo.transform;
                break;
        }
        resourceInfo.resource = resource;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);

        return itemInfo;
    }

    public ItemInfo FurnitureUIConstructor(Furniture furniture)
    {
        ItemInfo itemInfo = ItemConstruction(furniture.Id);
        itemInfo.type = 1;
        itemInfo.sprite = furniture.Sprite;
        FurnitureInfo furnitureInfo = itemInfo.gameObject.AddComponent<FurnitureInfo>();

        UIConstructor(furniture.Name);
        UIConstructor(Lex.LBL_SPRITE, furniture.Sprite);
        UIConstructor(Lex.LBL_ID, furniture.Id, false);
        furnitureInfo.nameInput = UIConstructor(Lex.LBL_NAME, furniture.Name);
        furnitureInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, furniture.Description);
        furnitureInfo.staminaRateInput = UIConstructor(Lex.LBL_STAMINA_RATE, furniture.StaminaRate);
        furnitureInfo.healthRateInput = UIConstructor(Lex.LBL_HEALTH_RATE, furniture.HealthRate);
        furnitureInfo.hungerRateInput = UIConstructor(Lex.LBL_HUNGER_RATE, furniture.HungerRate);

        CostsInfo costsInfo = CostsConstructor();
        costsInfo.itemInfo = itemInfo;
        furnitureInfo.costsInfo = costsInfo;

        int j = 0;
        foreach (ResourceAmount cost in furniture.resourcesNeeded)
        {
            GlobalValues.currentParent = costsInfo.transform;
            CostInfo costInfo = CostConstructor(j, cost);
            costInfo.parent = costsInfo.transform;
            costsInfo.costs.Add(costInfo);
            j++;
        }
        GlobalValues.currentParent = costsInfo.itemInfo.transform;
        furnitureInfo.furniture = furniture;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);

        return itemInfo;
    }

    public ItemInfo FacilityUIConstructor(Facility facility)
    {
        ItemInfo itemInfo = ItemConstruction(facility.Id);
        itemInfo.type = 2;
        itemInfo.sprite = facility.Sprite;
        FacilityInfo facilityInfo = itemInfo.gameObject.AddComponent<FacilityInfo>();

        UIConstructor(facility.Name);
        UIConstructor(Lex.LBL_SPRITE, facility.Sprite);
        UIConstructor(Lex.LBL_ID, facility.Id, false);
        facilityInfo.nameInput = UIConstructor(Lex.LBL_NAME, facility.Name);
        facilityInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, facility.Description);
        facilityInfo.dwarfCapacityInput = UIConstructor(Lex.LBL_DWARF_CAP, facility.DwarfCapacity);
        facilityInfo.extractionTimeInput = UIConstructor(Lex.LBL_EXTRACT_TIME, facility.ExtractionTime);
        facilityInfo.extractionAmountInput = UIConstructor(Lex.LBL_EXTRACT_AMOUNT, facility.ExtractionAmount);

        CostsInfo costsInfo = CostsConstructor();
        costsInfo.itemInfo = itemInfo;
        facilityInfo.costsInfo = costsInfo;

        int j = 0;
        foreach (ResourceAmount cost in facility.resourcesNeeded)
        {
            GlobalValues.currentParent = costsInfo.transform;
            CostInfo costInfo = CostConstructor(j, cost);
            costInfo.parent = costsInfo.transform;
            costsInfo.costs.Add(costInfo);
            j++;
        }
        GlobalValues.currentParent = costsInfo.itemInfo.transform;
        facilityInfo.facility = facility;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);
        return itemInfo;
    }

    public ItemInfo DecorationUIConstructor(Decoration decoration)
    {
        ItemInfo itemInfo = ItemConstruction(decoration.Id);
        itemInfo.type = 3;
        itemInfo.sprite = decoration.Sprite;
        DecorationInfo decorationInfo = itemInfo.gameObject.AddComponent<DecorationInfo>();

        UIConstructor(decoration.Name);
        UIConstructor(Lex.LBL_SPRITE, decoration.Sprite);
        UIConstructor(Lex.LBL_ID, decoration.Id, false);
        decorationInfo.nameInput = UIConstructor(Lex.LBL_NAME, decoration.Name);
        decorationInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, decoration.Description);
        decorationInfo.isExteriorToggle = UIConstructor(Lex.LBL_IS_EXT, decoration.IsExterior);
        decorationInfo.isInteriorToggle = UIConstructor(Lex.LBL_IS_INT, decoration.IsInterior);
        decorationInfo.isWallToggle = UIConstructor(Lex.LBL_IS_WALL, decoration.IsWall);
        decorationInfo.staminaRateInput = UIConstructor(Lex.LBL_STAMINA_RATE, decoration.StaminaRate);
        decorationInfo.healthRateInput = UIConstructor(Lex.LBL_HEALTH_RATE, decoration.HealthRate);
        decorationInfo.hungerRateInput = UIConstructor(Lex.LBL_HUNGER_RATE, decoration.HungerRate);

        CostsInfo costsInfo = CostsConstructor();
        costsInfo.itemInfo = itemInfo;
        decorationInfo.costsInfo = costsInfo;

        int j = 0;
        foreach (ResourceAmount cost in decoration.resourcesNeeded)
        {
            GlobalValues.currentParent = costsInfo.transform;
            CostInfo costInfo = CostConstructor(j, cost);
            costInfo.parent = costsInfo.transform;
            costsInfo.costs.Add(costInfo);
            j++;
        }
        GlobalValues.currentParent = costsInfo.itemInfo.transform;
        decorationInfo.decoration = decoration;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);

        return itemInfo;
    }

    public ItemInfo BuildingUIConstructor(Building building)
    {
        ItemInfo itemInfo = ItemConstruction(building.Id);
        itemInfo.type = 4;
        itemInfo.sprite = building.Sprite;
        BuildingInfo buildingInfo = itemInfo.gameObject.AddComponent<BuildingInfo>();

        UIConstructor(building.Name);
        UIConstructor(Lex.LBL_SPRITE, building.Sprite);
        UIConstructor(Lex.LBL_ID, building.Id, false);
        buildingInfo.nameInput = UIConstructor(Lex.LBL_NAME, building.Name);
        buildingInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, building.Description);

        CostsInfo costsInfo = CostsConstructor();
        costsInfo.itemInfo = itemInfo;
        buildingInfo.costsInfo = costsInfo;

        int j = 0;
        foreach (ResourceAmount cost in building.resourcesNeeded)
        {
            GlobalValues.currentParent = costsInfo.transform;
            CostInfo costInfo = CostConstructor(j, cost);
            costInfo.parent = costsInfo.transform;
            costsInfo.costs.Add(costInfo);
            j++;
        }
        GlobalValues.currentParent = costsInfo.itemInfo.transform;
        buildingInfo.building = building;
        BuildingFurnituresInfo buildingFurnituresInfo = BuildingFurnituresConstructor();
        buildingInfo.buildingFurnituresInfo = buildingFurnituresInfo;
        buildingFurnituresInfo.itemInfo = itemInfo;

        GlobalValues.currentParent = buildingFurnituresInfo.transform;
        GameObject inCategory = UICategory(Lex.LBL_INS);
        inCategory.transform.SetSiblingIndex(buildingFurnituresInfo.insParent.GetSiblingIndex());
        Button addInButton = inCategory.GetComponentInChildren<Button>();
        addInButton.onClick.RemoveAllListeners();
        addInButton.onClick.AddListener(delegate { buildingFurnituresInfo.AddBuildingFurniture(0); });

        j = 0;
        foreach (Furniture furniture in building.furnituresIn)
        {
            GlobalValues.currentParent = buildingFurnituresInfo.insParent;
            BuildingFurnitureInfo inInfo = BuildingFurnitureConstructor(j, furniture, Lex.LBL_IN);
            buildingFurnituresInfo.furnituresInfo.Add(inInfo);
            j++;
        }
        GlobalValues.currentParent = buildingFurnituresInfo.transform;
        GameObject outCategory = UICategory(Lex.LBL_OUTS);
        outCategory.transform.SetSiblingIndex(buildingFurnituresInfo.outsParent.GetSiblingIndex());
        Button addOutButton = outCategory.GetComponentInChildren<Button>();
        addOutButton.onClick.RemoveAllListeners();
        addOutButton.onClick.AddListener(delegate { buildingFurnituresInfo.AddBuildingFurniture(1); });

        j = 0;
        foreach (Furniture furniture in building.furnituresOut)
        {
            GlobalValues.currentParent = buildingFurnituresInfo.outsParent;
            BuildingFurnitureInfo outInfo = BuildingFurnitureConstructor(j, furniture, Lex.LBL_OUT);
            buildingFurnituresInfo.furnituresInfo.Add(outInfo);
            j++;
        }
        GlobalValues.currentParent = buildingFurnituresInfo.transform;
        GameObject processCategory = UICategory(Lex.LBL_PROCESS);
        processCategory.transform.SetSiblingIndex(buildingFurnituresInfo.processesParent.GetSiblingIndex());
        Button addProcessButton = processCategory.GetComponentInChildren<Button>();
        addProcessButton.onClick.RemoveAllListeners();
        addProcessButton.onClick.AddListener(delegate { buildingFurnituresInfo.AddBuildingFurniture(2); });

        j = 0;
        foreach (Furniture furniture in building.furnituresProcess)
        {
            GlobalValues.currentParent = buildingFurnituresInfo.processesParent;
            BuildingFurnitureInfo processInfo = BuildingFurnitureConstructor(j, furniture, Lex.LBL_PROCESS);
            buildingFurnituresInfo.furnituresInfo.Add(processInfo);
            j++;
        }
        GlobalValues.currentParent = buildingFurnituresInfo.transform;
        GameObject optiCategory = UICategory(Lex.LBL_OPTIS);
        optiCategory.transform.SetSiblingIndex(buildingFurnituresInfo.optisParent.GetSiblingIndex());
        Button addOptiButton = optiCategory.GetComponentInChildren<Button>();
        addOptiButton.onClick.RemoveAllListeners();
        addOptiButton.onClick.AddListener(delegate { buildingFurnituresInfo.AddBuildingFurniture(3); });

        j = 0;
        foreach (Furniture furniture in building.furnituresOpti)
        {
            GlobalValues.currentParent = buildingFurnituresInfo.optisParent;
            BuildingFurnitureInfo optiInfo = BuildingFurnitureConstructor(j, furniture, Lex.LBL_OPTI);
            buildingFurnituresInfo.furnituresInfo.Add(optiInfo);
            j++;
        }
        GlobalValues.currentParent = buildingFurnituresInfo.transform;
        GameObject storageCategory = UICategory(Lex.LBL_STORAGES);
        storageCategory.transform.SetSiblingIndex(buildingFurnituresInfo.storagesParent.GetSiblingIndex());
        Button addStorageButton = storageCategory.GetComponentInChildren<Button>();
        addStorageButton.onClick.RemoveAllListeners();
        addStorageButton.onClick.AddListener(delegate { buildingFurnituresInfo.AddBuildingFurniture(4); });

        j = 0;
        foreach (Furniture furniture in building.furnituresStorage)
        {
            GlobalValues.currentParent = buildingFurnituresInfo.storagesParent;
            BuildingFurnitureInfo storageInfo = BuildingFurnitureConstructor(j, furniture, Lex.LBL_STORAGE);
            buildingFurnituresInfo.furnituresInfo.Add(storageInfo);
            j++;
        }
        GlobalValues.currentParent = buildingFurnituresInfo.itemInfo.transform;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);

        return itemInfo;
    }

    public ItemInfo ExteriorJobUIConstructor(ExteriorJob exteriorJob)
    {
        ItemInfo itemInfo = ItemConstruction(exteriorJob.Id);
        itemInfo.type = 5;
        itemInfo.sprite = exteriorJob.Sprite;
        ExteriorJobInfo exteriorJobInfo = itemInfo.gameObject.AddComponent<ExteriorJobInfo>();

        UIConstructor(exteriorJob.Name);
        UIConstructor(Lex.LBL_SPRITE, exteriorJob.Sprite);
        UIConstructor(Lex.LBL_ID, exteriorJob.Id, false);
        exteriorJobInfo.nameInput = UIConstructor(Lex.LBL_NAME, exteriorJob.Name);
        exteriorJobInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, exteriorJob.Description);
        exteriorJobInfo.extractionTimeInput = UIConstructor(Lex.LBL_EXTRACT_TIME, exteriorJob.ExtractionTime);

        FiltersInfo filtersInfo = FiltersConstructor();
        filtersInfo.itemInfo = itemInfo;
        exteriorJobInfo.filtersInfo = filtersInfo;

        int j = 0;
        foreach (ResourceFilter filter in exteriorJob.resources)
        {
            GlobalValues.currentParent = filtersInfo.transform;
            FilterInfo filterInfo = FilterConstructor(j, filter);
            filterInfo.parent = filtersInfo.transform;
            filtersInfo.filters.Add(filterInfo);
            j++;
        }
        GlobalValues.currentParent = filtersInfo.itemInfo.transform;
        exteriorJobInfo.exteriorJob = exteriorJob;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);
        return itemInfo;
    }

    public ItemInfo WeaponUIConstructor(Weapon weapon)
    {
        ItemInfo itemInfo = ItemConstruction(weapon.Id);
        itemInfo.type = 6;
        itemInfo.sprite = weapon.Sprite;
        WeaponInfo weaponInfo = itemInfo.gameObject.AddComponent<WeaponInfo>();

        UIConstructor(weapon.Name);
        UIConstructor(Lex.LBL_SPRITE, weapon.Sprite);
        UIConstructor(Lex.LBL_ID, weapon.Id, false);
        weaponInfo.nameInput = UIConstructor(Lex.LBL_NAME, weapon.Name);
        weaponInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, weapon.Description);
        weaponInfo.basePriceInput = UIConstructor(Lex.LBL_BASE_PRICE, weapon.BasePrice);
        weaponInfo.damageInput = UIConstructor(Lex.LBL_DAMAGE, weapon.Damage);
        weaponInfo.speedInput = UIConstructor(Lex.LBL_SPEED, weapon.Speed);
        weaponInfo.isRangedToggle = UIConstructor(Lex.LBL_IS_RANGED, weapon.IsRanged);
        weaponInfo.isOneHandedToggle = UIConstructor(Lex.LBL_IS_ONE_HANDED, weapon.IsOneHanded);

        CostsInfo costsInfo = CostsConstructor();
        costsInfo.itemInfo = itemInfo;
        weaponInfo.costsInfo = costsInfo;

        int j = 0;
        foreach (ResourceAmount cost in weapon.resourcesNeeded)
        {
            GlobalValues.currentParent = costsInfo.transform;
            CostInfo costInfo = CostConstructor(j, cost);
            costInfo.parent = costsInfo.transform;
            costsInfo.costs.Add(costInfo);
            j++;
        }
        GlobalValues.currentParent = costsInfo.itemInfo.transform;
        weaponInfo.weapon = weapon;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);
        return itemInfo;
    }

    public ItemInfo UnitUIConstructor(Unit unit)
    {
        ItemInfo itemInfo = ItemConstruction(unit.Id);
        itemInfo.type = 7;
        itemInfo.sprite = unit.Sprite;
        UnitInfo unitInfo = itemInfo.gameObject.AddComponent<UnitInfo>();

        UIConstructor(unit.Name);
        UIConstructor(Lex.LBL_SPRITE, unit.Sprite);
        UIConstructor(Lex.LBL_ID, unit.Id, false);
        unitInfo.nameInput = UIConstructor(Lex.LBL_NAME, unit.Name);
        unitInfo.descriptionInput = UIConstructor(Lex.LBL_DESC, unit.Description);
        unitInfo.staminaInput = UIConstructor(Lex.LBL_STAMINA, unit.Stamina);
        unitInfo.healthInput = UIConstructor(Lex.LBL_HEALTH, unit.Health);
        unitInfo.hungerInput = UIConstructor(Lex.LBL_HUNGER, unit.Hunger);
        unitInfo.speedInput = UIConstructor(Lex.LBL_SPEED, unit.Speed);
        unitInfo.unit = unit;
        UICommandsConstructor(itemInfo);

        GlobalValues.currentParent = itemInfo.parent;
        GlobalValues.currentParent.GetComponent<ContentInfo>().items.Add(itemInfo);

        return itemInfo;
    }

    public ItemInfo ItemConstruction(int _id)
    {
        GameObject _item = Instantiate(item) as GameObject;
        _item.transform.SetParent(GlobalValues.currentParent, false);
        _item.name = _id.ToString();

        ItemInfo itemInfo = _item.AddComponent<ItemInfo>();
        itemInfo.id = _id;
        itemInfo.parent = GlobalValues.currentParent;
        GlobalValues.currentParent = _item.transform;

        return itemInfo;
    }

    public BuildingFurnituresInfo BuildingFurnituresConstructor()
    {
        GameObject element = Instantiate(buildingFurnitures) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = Lex.LBL_BUILDING_FURN;
        BuildingFurnituresInfo buildingFurnituresInfo = element.AddComponent<BuildingFurnituresInfo>();
        buildingFurnituresInfo.insParent = element.transform.Find(Lex.LBL_INS);
        buildingFurnituresInfo.outsParent = element.transform.Find(Lex.LBL_OUTS);
        buildingFurnituresInfo.processesParent = element.transform.Find(Lex.LBL_PROCESSES);
        buildingFurnituresInfo.optisParent = element.transform.Find(Lex.LBL_OPTIS);
        buildingFurnituresInfo.storagesParent = element.transform.Find(Lex.LBL_STORAGES);

        return buildingFurnituresInfo;
    }

    public BuildingFurnitureInfo BuildingFurnitureConstructor(int _furnitureId, Furniture _furniture, string _label)
    {
        GameObject element = Instantiate(this.buildingFurniture) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = _furnitureId.ToString();

        BuildingFurnitureInfo buildingFurniture = element.AddComponent<BuildingFurnitureInfo>();
        buildingFurniture.infosParent = GlobalValues.currentParent.parent;
        buildingFurniture.id = _furnitureId;
        buildingFurniture.oldId = _furnitureId;
        buildingFurniture.furniture = _furniture;
        GlobalValues.currentParent = buildingFurniture.transform;

        // Subtitle Creation
        GameObject subtitleElement = UIConstructor(_furnitureId, _label);
        Button duplicateButton = subtitleElement.transform.Find("DuplicateButton").GetComponent<Button>();
        Button deleteButton = subtitleElement.transform.Find("DeleteButton").GetComponent<Button>();
        buildingFurniture.subtitle = subtitleElement.GetComponentInChildren<TMP_Text>();

        // Furniture Dropdown creation 
        GameObject dropdownElement = Instantiate(inputDropdownElement) as GameObject;
        dropdownElement.transform.SetParent(GlobalValues.currentParent, false);
        dropdownElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_FURN + " :";

        TMP_Dropdown dropdown = dropdownElement.GetComponentInChildren<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedFurnitures = new List<TMP_Dropdown.OptionData>();
        int value = 0;

        foreach (Furniture furniture in GlobalValues.furnitures.OrderBy(x => x.id))
        {
            orderedFurnitures.Add(new TMP_Dropdown.OptionData(furniture.Name, furniture.Sprite));

            if (furniture.Id == _furniture.Id)
            {
                value = furniture.Id;
            }
        }
        dropdown.AddOptions(orderedFurnitures);
        dropdown.value = value;
        dropdown.onValueChanged.AddListener(buildingFurniture.ChangeFurniture);
        buildingFurniture.furnitureInput = dropdown;

        switch (_furniture.Type)
        {
            case 0:
                {
                    buildingFurniture.capacityInput = UIConstructor(Lex.LBL_CAP, _furniture.Capacity, true);
                    buildingFurniture.capacityInput.onValueChanged.AddListener(buildingFurniture.ChangeCapacity);

                    FiltersInfo filtersInfo = FiltersConstructor();
                    filtersInfo.itemInfo = buildingFurniture.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo;
                    buildingFurniture.filters = filtersInfo;

                    int k = 0;
                    foreach (ResourceFilter filter in _furniture.resourcesAllowed)
                    {
                        GlobalValues.currentParent = filtersInfo.transform;
                        FilterInfo filterInfo = FilterConstructor(k, filter);
                        filterInfo.parent = filtersInfo.transform;
                        filtersInfo.filters.Add(filterInfo);
                        k++;
                    }
                    break;
                }
            case 1:
                {
                    buildingFurniture.capacityInput = UIConstructor(Lex.LBL_CAP, _furniture.Capacity, true);
                    buildingFurniture.capacityInput.onValueChanged.AddListener(buildingFurniture.ChangeCapacity);

                    FiltersInfo filtersInfo = FiltersConstructor();
                    filtersInfo.itemInfo = buildingFurniture.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo;
                    buildingFurniture.filters = filtersInfo;

                    int k = 0;
                    foreach (ResourceFilter filter in _furniture.resourcesAllowed)
                    {
                        GlobalValues.currentParent = filtersInfo.transform;
                        FilterInfo filterInfo = FilterConstructor(k, filter);
                        filterInfo.parent = filtersInfo.transform;
                        filtersInfo.filters.Add(filterInfo);
                        k++;
                    }
                    break;
                }
            case 2:
                {
                    RecipesInfo recipesInfo = RecipesConstructor();
                    recipesInfo.buildingFurnitureInfo = buildingFurniture;
                    buildingFurniture.recipes = recipesInfo;

                    int k = 0;
                    foreach (ResourceRecipe recipe in _furniture.resourcesRecipes)
                    {
                        GlobalValues.currentParent = recipesInfo.transform;
                        RecipeInfo recipeInfo = RecipeConstructor(k, recipe);
                        recipeInfo.infosParent = recipesInfo.transform;
                        recipesInfo.recipes.Add(recipeInfo);
                        k++;
                    }
                    break;
                }
            case 3:
                {
                    FiltersInfo filtersInfo = FiltersConstructor();
                    filtersInfo.itemInfo = buildingFurniture.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo;
                    buildingFurniture.filters = filtersInfo;

                    int k = 0;
                    foreach (ResourceFilter filter in _furniture.resourcesAllowed)
                    {
                        GlobalValues.currentParent = filtersInfo.transform;
                        FilterInfo filterInfo = FilterConstructor(k, filter);
                        filterInfo.parent = filtersInfo.transform;
                        filtersInfo.filters.Add(filterInfo);
                        k++;
                    }
                    break;
                }
            case 4:
                {
                    buildingFurniture.capacityInput = UIConstructor(Lex.LBL_CAP, _furniture.Capacity, true);
                    buildingFurniture.capacityInput.onValueChanged.AddListener(buildingFurniture.ChangeCapacity);

                    FiltersInfo filtersInfo = FiltersConstructor();
                    filtersInfo.itemInfo = buildingFurniture.infosParent.GetComponent<BuildingFurnituresInfo>().itemInfo;
                    buildingFurniture.filters = filtersInfo;

                    int k = 0;
                    foreach (ResourceFilter filter in _furniture.resourcesAllowed)
                    {
                        GlobalValues.currentParent = filtersInfo.transform;
                        FilterInfo filterInfo = FilterConstructor(k, filter);
                        filterInfo.parent = filtersInfo.transform;
                        filtersInfo.filters.Add(filterInfo);
                        k++;
                    }
                    break;
                }
        }
        //Buttons Bindings
        duplicateButton.onClick.RemoveAllListeners();
        duplicateButton.onClick.AddListener(buildingFurniture.Duplicate);
        deleteButton.onClick.RemoveAllListeners();
        deleteButton.onClick.AddListener(buildingFurniture.Delete);

        return buildingFurniture;
    }

    public CostsInfo CostsConstructor()
    {
        Button addButton = UICategory(Lex.LBL_COSTS).GetComponentInChildren<Button>();
        GameObject element = Instantiate(costs) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = Lex.LBL_COSTS;
        CostsInfo costsInfo = element.AddComponent<CostsInfo>();
        costsInfo.addButton = addButton;
        CostsCommandBindings(costsInfo);

        return costsInfo;
    }

    public CostInfo CostConstructor(int _costId, ResourceAmount _resourceAmount)
    {
        GameObject element = Instantiate(cost) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = _costId.ToString();

        CostInfo costInfo = element.AddComponent<CostInfo>();
        costInfo.id = _costId;
        costInfo.oldId = _costId;
        costInfo.resourceAmount = _resourceAmount;
        GlobalValues.currentParent = costInfo.transform;

        // Subtitle Creation
        GameObject subtitleElement = UIConstructor(_costId, Lex.LBL_COST);
        Button duplicateButton = subtitleElement.transform.Find("DuplicateButton").GetComponent<Button>();
        Button deleteButton = subtitleElement.transform.Find("DeleteButton").GetComponent<Button>();
        costInfo.subtitle = subtitleElement.GetComponentInChildren<TMP_Text>();

        // Type Dropdown creation         
        GameObject typeElement = Instantiate(inputDropdownElement) as GameObject;
        typeElement.transform.SetParent(GlobalValues.currentParent, false);
        typeElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_TYPE + " :";

        TMP_Dropdown typeDropdown = typeElement.GetComponentInChildren<TMP_Dropdown>();
        typeDropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> resourceNames = new List<TMP_Dropdown.OptionData>
        {
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[0], GameManager.Instance.mineralsSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[1], GameManager.Instance.gemsSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[2], GameManager.Instance.resourcesSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[3], GameManager.Instance.craftedSprite)
        };
        typeDropdown.AddOptions(resourceNames);
        typeDropdown.value = _resourceAmount.Type;
        typeDropdown.onValueChanged.AddListener(costInfo.ChangeType);
        costInfo.typeInput = typeDropdown;

        // Resource Dropdown creation 
        GameObject resourceElement = Instantiate(inputDropdownElement) as GameObject;
        resourceElement.transform.SetParent(GlobalValues.currentParent, false);
        resourceElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_RESOURCE + " :";

        TMP_Dropdown dropdown = resourceElement.GetComponentInChildren<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedResources = new List<TMP_Dropdown.OptionData>();
        int value = 0;

        foreach (Resource resource in GlobalValues.resources.OrderBy(x => x.id))
        {
            if (resource.type == _resourceAmount.Type)
            {
                orderedResources.Add(new TMP_Dropdown.OptionData(resource.Name, resource.Sprite));

                if (resource.id == _resourceAmount.Id)
                {
                    value = resource.id;
                }
            }
        }
        dropdown.AddOptions(orderedResources);
        dropdown.value = value;
        dropdown.onValueChanged.AddListener(costInfo.ChangeId);
        costInfo.idInput = dropdown;

        // Amount creation
        costInfo.amountInput = UIConstructor(Lex.LBL_AMOUNT, _resourceAmount.Amount, true);
        costInfo.amountInput.onValueChanged.AddListener(costInfo.ChangeAmount);

        //Buttons Bindings
        costInfo.duplicateButton = duplicateButton;
        costInfo.deleteButton = deleteButton;
        CostCommandBindings(costInfo);

        return costInfo;
    }

    public FiltersInfo FiltersConstructor()
    {
        Button addButton = UICategory(Lex.LBL_FILTERS).GetComponentInChildren<Button>();
        GameObject element = Instantiate(costs) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = Lex.LBL_FILTERS;
        FiltersInfo filtersInfo = element.AddComponent<FiltersInfo>();

        addButton.onClick.RemoveAllListeners();
        addButton.onClick.AddListener(filtersInfo.AddRecipe);

        return filtersInfo;
    }

    public FilterInfo FilterConstructor(int _filterId, ResourceFilter _resourceFilter)
    {
        GameObject element = Instantiate(cost) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = _filterId.ToString();

        FilterInfo filterInfo = element.AddComponent<FilterInfo>();
        filterInfo.id = _filterId;
        filterInfo.oldId = _filterId;
        filterInfo.resourceFilter = _resourceFilter;
        GlobalValues.currentParent = filterInfo.transform;

        // Subtitle Creation
        GameObject subtitleElement = UIConstructor(_filterId, Lex.LBL_FILTER);
        Button duplicateButton = subtitleElement.transform.Find("DuplicateButton").GetComponent<Button>();
        Button deleteButton = subtitleElement.transform.Find("DeleteButton").GetComponent<Button>();
        filterInfo.subtitle = subtitleElement.GetComponentInChildren<TMP_Text>();

        // Type Dropdown creation         
        GameObject typeElement = Instantiate(inputDropdownElement) as GameObject;
        typeElement.transform.SetParent(GlobalValues.currentParent, false);
        typeElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_TYPE + " :";

        TMP_Dropdown typeDropdown = typeElement.GetComponentInChildren<TMP_Dropdown>();
        typeDropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> resourceNames = new List<TMP_Dropdown.OptionData>
        {
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[0], GameManager.Instance.mineralsSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[1], GameManager.Instance.gemsSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[2], GameManager.Instance.resourcesSprite),
            new TMP_Dropdown.OptionData(Lex.ITEMS_CATS[3], GameManager.Instance.craftedSprite)
        };
        typeDropdown.AddOptions(resourceNames);
        typeDropdown.value = _resourceFilter.Type;
        typeDropdown.onValueChanged.AddListener(filterInfo.ChangeType);
        filterInfo.typeInput = typeDropdown;

        // Resource Dropdown creation 
        GameObject resourceElement = Instantiate(inputDropdownElement) as GameObject;
        resourceElement.transform.SetParent(GlobalValues.currentParent, false);
        resourceElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_RESOURCE + " :";

        TMP_Dropdown dropdown = resourceElement.GetComponentInChildren<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedResources = new List<TMP_Dropdown.OptionData>();
        int value = 0;

        foreach (Resource resource in GlobalValues.resources.OrderBy(x => x.id))
        {
            if (resource.type == _resourceFilter.Type)
            {
                orderedResources.Add(new TMP_Dropdown.OptionData(resource.Name, resource.Sprite));

                if (resource.id == _resourceFilter.Id)
                {
                    value = resource.id;
                }
            }
        }
        dropdown.AddOptions(orderedResources);
        dropdown.value = value;
        dropdown.onValueChanged.AddListener(filterInfo.ChangeId);
        filterInfo.idInput = dropdown;

        // Percent creation
        filterInfo.percentInput = UIConstructor(Lex.LBL_PERCENT, _resourceFilter.Percent, true);
        filterInfo.percentInput.onValueChanged.AddListener(filterInfo.ChangePercent);

        //Buttons Bindings
        duplicateButton.onClick.RemoveAllListeners();
        duplicateButton.onClick.AddListener(filterInfo.Duplicate);
        deleteButton.onClick.RemoveAllListeners();
        deleteButton.onClick.AddListener(filterInfo.Delete);

        return filterInfo;
    }

    public RecipesInfo RecipesConstructor()
    {
        Button addButton = UICategory(Lex.LBL_RECIPES).GetComponentInChildren<Button>();
        GameObject element = Instantiate(costs) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = Lex.LBL_RECIPES;
        RecipesInfo recipesInfo = element.AddComponent<RecipesInfo>();

        addButton.onClick.RemoveAllListeners();
        addButton.onClick.AddListener(recipesInfo.AddRecipe);

        return recipesInfo;
    }

    public RecipeInfo RecipeConstructor(int _recipeId, ResourceRecipe _resourceRecipe)
    {
        GameObject element = Instantiate(cost) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = _recipeId.ToString();

        RecipeInfo recipeInfo = element.AddComponent<RecipeInfo>();
        recipeInfo.id = _recipeId;
        recipeInfo.oldId = _recipeId;
        recipeInfo.resourceRecipe = _resourceRecipe;
        GlobalValues.currentParent = recipeInfo.transform;

        // Subtitle Creation        
        GameObject subtitleElement = UIConstructor(_recipeId, Lex.LBL_RECIPE);
        Button duplicateButton = subtitleElement.transform.Find("DuplicateButton").GetComponent<Button>();
        Button deleteButton = subtitleElement.transform.Find("DeleteButton").GetComponent<Button>();
        recipeInfo.subtitle = subtitleElement.GetComponentInChildren<TMP_Text>();

        // Resource Dropdown creation 
        GameObject resourceElement = Instantiate(inputDropdownElement) as GameObject;
        resourceElement.transform.SetParent(GlobalValues.currentParent, false);
        resourceElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_RESOURCE + " :";

        TMP_Dropdown dropdown = resourceElement.GetComponentInChildren<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> orderedResources = new List<TMP_Dropdown.OptionData>();
        int value = 0;

        foreach (Resource resource in GlobalValues.resources.OrderBy(x => x.id))
        {
            if (resource.type == 3)
            {
                orderedResources.Add(new TMP_Dropdown.OptionData(resource.Name, resource.Sprite));

                if (resource.id == _resourceRecipe.Id)
                {
                    value = resource.id;
                }
            }
        }
        dropdown.AddOptions(orderedResources);
        dropdown.value = value;
        dropdown.onValueChanged.AddListener(recipeInfo.ChangeId);
        recipeInfo.idInput = dropdown;

        // Speed creation
        recipeInfo.speedInput = UIConstructor(Lex.LBL_SPEED, _resourceRecipe.Speed);
        recipeInfo.speedInput.onValueChanged.AddListener(recipeInfo.ChangeSpeed);

        // IsAutoCrafted creation
        GameObject toggleElement = Instantiate(inputBoolElement) as GameObject;
        toggleElement.transform.SetParent(GlobalValues.currentParent, false);
        toggleElement.GetComponentInChildren<TMP_Text>().text = Lex.LBL_IS_AUTO_CRAFTED + " :";
        toggleElement.name = Lex.LBL_IS_AUTO_CRAFTED;
        toggleElement.GetComponentInChildren<Toggle>().isOn = _resourceRecipe.IsAutoCrafted;

        recipeInfo.isAutocraftedInput = toggleElement.GetComponentInChildren<Toggle>();
        recipeInfo.isAutocraftedInput.onValueChanged.AddListener(recipeInfo.ChangeIsAutoCrafted);

        //Buttons Bindings
        duplicateButton.onClick.RemoveAllListeners();
        duplicateButton.onClick.AddListener(recipeInfo.Duplicate);
        deleteButton.onClick.RemoveAllListeners();
        deleteButton.onClick.AddListener(recipeInfo.Delete);

        return recipeInfo;
    }

    public GameObject UICategory(string _categoryTitle)
    {
        GameObject subtitle = Instantiate(subtitleGeneral) as GameObject;
        subtitle.transform.SetParent(GlobalValues.currentParent, false);
        subtitle.GetComponentInChildren<TMP_Text>().text = _categoryTitle;

        return subtitle;
    }

    public void UIConstructor(string _title)
    {
        GameObject element = Instantiate(titleElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _title;
    }

    public GameObject UIConstructor(int _index, string _subtitle)
    {
        GameObject element = Instantiate(subtitleElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _subtitle + " :";
        element.transform.Find("Text").Find("IndexPanel").GetComponentInChildren<TMP_Text>().text = _index.ToString();

        return element;
    }

    public TMP_InputField UIConstructor(string _label, int _value, bool _interactable)
    {
        GameObject element = Instantiate(inputIntElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;

        TMP_InputField input = element.GetComponentInChildren<TMP_InputField>();
        input.text = _value.ToString();

        if (!_interactable)
            input.interactable = false;

        if (GlobalValues.currentParent.GetComponent<ItemInfo>())
            element.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener(GlobalValues.currentParent.GetComponent<ItemInfo>().CheckForChange);

        return input;
    }

    public TMP_InputField UIConstructor(string _label, float _value)
    {
        GameObject element = Instantiate(inputFloatElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;
        element.GetComponentInChildren<TMP_InputField>().text = _value.ToString();

        if (GlobalValues.currentParent.GetComponent<ItemInfo>())
            element.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener(GlobalValues.currentParent.GetComponent<ItemInfo>().CheckForChange);

        return element.GetComponentInChildren<TMP_InputField>();
    }

    public Toggle UIConstructor(string _label, bool _value)
    {
        GameObject element = Instantiate(inputBoolElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;
        element.GetComponentInChildren<Toggle>().isOn = _value;
        element.GetComponentInChildren<Toggle>().onValueChanged.AddListener(GlobalValues.currentParent.GetComponent<ItemInfo>().CheckForChange);

        return element.GetComponentInChildren<Toggle>();
    }

    public void UIConstructor(string _label, Sprite _value)
    {
        GameObject element = Instantiate(inputSpriteElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;
        element.transform.Find("Button").Find("Image").GetComponent<Image>().sprite = _value;

        ItemInfo parentItem = GlobalValues.currentParent.GetComponent<ItemInfo>();
        Button spriteButton = element.transform.Find("Button").GetComponent<Button>();

        parentItem.spriteButton = spriteButton;
        spriteButton.onClick.RemoveAllListeners();
        spriteButton.onClick.AddListener(parentItem.SelectSprite);
    }

    public void UIConstructor(string _label, Texture2D _value)
    {
        GameObject element = Instantiate(inputTextureElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;
        element.transform.Find("Button").Find("RawImage").GetComponent<RawImage>().texture = _value;

        ResourceInfo parentItem = GlobalValues.currentParent.GetComponent<ResourceInfo>();
        Button textureButton = element.transform.Find("Button").GetComponent<Button>();

        parentItem.decalButton = textureButton;
        textureButton.onClick.RemoveAllListeners();
        textureButton.onClick.AddListener(parentItem.SelectTexture);
    }

    public TMP_InputField UIConstructor(string _label, string _value)
    {
        GameObject element = Instantiate(inputTextElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.GetComponentInChildren<TMP_Text>().text = _label + " :";
        element.name = _label;
        element.GetComponentInChildren<TMP_InputField>().text = _value;

        if (GlobalValues.currentParent.GetComponent<ItemInfo>())
            element.GetComponentInChildren<TMP_InputField>().onEndEdit.AddListener(GlobalValues.currentParent.GetComponent<ItemInfo>().CheckForChange);

        return element.GetComponentInChildren<TMP_InputField>();
    }

    public void UICommandsConstructor(ItemInfo info)
    {
        GameObject element = Instantiate(commandsElement) as GameObject;
        element.transform.SetParent(GlobalValues.currentParent, false);
        element.name = "commandsElement";

        CommandsBindings(info, element.transform);
    }

    public void CommandsBindings(ItemInfo info, Transform element)
    {
        Button upButton = element.Find("UpButton").GetComponent<Button>();
        Button downButton = element.Find("DownButton").GetComponent<Button>();
        Button duplicateButton = element.Find("DuplicateButton").GetComponent<Button>();
        Button deleteButton = element.Find("DeleteButton").GetComponent<Button>();
        Button revertButton = element.Find("RevertButton").GetComponent<Button>();
        Button saveButton = element.Find("SaveButton").GetComponent<Button>();

        upButton.onClick.RemoveAllListeners();
        upButton.onClick.AddListener(info.MoveUp);
        info.upButton = upButton;

        downButton.onClick.RemoveAllListeners();
        downButton.onClick.AddListener(info.MoveDown);
        info.downButton = downButton;

        duplicateButton.onClick.RemoveAllListeners();
        duplicateButton.onClick.AddListener(info.Duplicate);
        info.duplicateButton = duplicateButton;

        deleteButton.onClick.RemoveAllListeners();
        deleteButton.onClick.AddListener(info.Delete);
        info.deleteButton = deleteButton;

        revertButton.onClick.RemoveAllListeners();
        revertButton.onClick.AddListener(info.RevertChanges);
        info.revertButton = revertButton;

        saveButton.onClick.RemoveAllListeners();
        saveButton.onClick.AddListener(info.RevertChanges);
        info.saveButton = saveButton;
    }

    public void CostsCommandBindings(CostsInfo costsInfo)
    {
        costsInfo.addButton.onClick.RemoveAllListeners();
        costsInfo.addButton.onClick.AddListener(costsInfo.AddCost);
    }

    public void CostCommandBindings(CostInfo costInfo)
    {
        costInfo.duplicateButton.onClick.RemoveAllListeners();
        costInfo.duplicateButton.onClick.AddListener(costInfo.Duplicate);

        costInfo.deleteButton.onClick.RemoveAllListeners();
        costInfo.deleteButton.onClick.AddListener(costInfo.Delete);
    }

    public void CheckForInteractableMoves()
    {
        foreach (ItemInfo item in GlobalValues.currentParent.GetComponent<ContentInfo>().items)
        {
            item.CheckForInteractableMove();
        }
    }
}
