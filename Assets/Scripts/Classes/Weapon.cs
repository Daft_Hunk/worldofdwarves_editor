﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon
{
    public int id;
    public string name;
    public string description;
    public int basePrice;
    public float damage;
    public float speed;
    public bool isRanged;
    public bool isOneHanded;
    public List<ResourceAmount> resourcesNeeded;
    public Sprite sprite;

    public Weapon(int _id, string _name, string _description, int _basePrice, float _damage, float _speed, bool _isRanged, bool _isOneHanded, Sprite _sprite)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.basePrice = _basePrice;
        this.damage = _damage;
        this.speed = _speed;
        this.isRanged = _isRanged;
        this.isOneHanded = _isOneHanded;
        this.sprite = _sprite;
        this.resourcesNeeded = new List<ResourceAmount>();
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public int BasePrice
    {
        get { return this.basePrice; }
        set { this.basePrice = value; }
    }
    public float Damage
    {
        get { return this.damage; }
        set { this.damage = value; }
    }
    public float Speed
    {
        get { return this.speed; }
        set { this.speed = value; }
    }
    public bool IsRanged
    {
        get { return this.isRanged; }
        set { this.isRanged = value; }
    }
    public bool IsOneHanded
    {
        get { return this.isOneHanded; }
        set { this.isOneHanded = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}
