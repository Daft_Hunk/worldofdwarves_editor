using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Furniture
{
    public int uID;
    public int id;
    public string name;
    public string description;
    public float staminaRate;
    public float healthRate;
    public float hungerRate;
    public Sprite sprite;
    public GameObject prefab;
    public Sprite minimapIcon;
    public List<ResourceAmount> resourcesNeeded = new List<ResourceAmount>();
    public int type;
    //Type Specific
    public int capacity;
    public List<ResourceFilter> resourcesAllowed = new List<ResourceFilter>();
    public List<ResourceRecipe> resourcesRecipes = new List<ResourceRecipe>();

    public Furniture(int _uID, int _id, string _name, string _description, float _staminaRate, float _healthRate, float _hungerRate, Sprite _sprite, GameObject _prefab, Sprite _minimapIcon)
    {
        this.uID = _uID;
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.staminaRate = _staminaRate;
        this.healthRate = _healthRate;
        this.hungerRate = _hungerRate;
        this.sprite = _sprite;
        this.prefab = _prefab;
        this.minimapIcon = _minimapIcon;
        if (sprite != null)
            this.minimapIcon = this.sprite;
    }
    /// <summary>
    ///     Allow to overload the furniture with a type.
    ///     > 0 for In which need capacity and resources
    ///     > 1 for Out which need capacity and resources
    ///     > 2 for Process which need resourcesRecipes
    ///     > 3 for Opti which need resources
    ///     > 4 for Storage which need capacity and resources
    /// </summary>
    /// <param name="_type"></param>
    /// <param name="_capacity"></param>
    public void InitiateType(int _type, int _capacity)
    {
        this.type = _type;
        switch (_type)
        {
            case 0:
                this.capacity = _capacity;
                break;
            case 1:
                this.capacity = _capacity;
                break;
            case 4:
                this.capacity = _capacity;
                break;
        }
    }

    public int UId
    {
        get { return this.uID; }
    }
    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public float StaminaRate
    {
        get { return this.staminaRate; }
        set { this.staminaRate = value; }
    }
    public float HealthRate
    {
        get { return this.healthRate; }
        set { this.healthRate = value; }
    }
    public float HungerRate
    {
        get { return this.hungerRate; }
        set { this.hungerRate = value; }
    }
    public int Type
    {
        get { return this.type; }
        set { this.type = value; }
    }
    public int Capacity
    {
        get { return this.capacity; }
        set { this.capacity = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
    public GameObject Prefab
    {
        get { return this.prefab; }
        set { this.prefab = value; }
    }
    public Sprite MinimapIcon
    {
        get { return this.minimapIcon; }
        set { this.minimapIcon = value; }
    }
}