using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Building
{
    public int id;
    public string name;
    public string description;
    public Sprite sprite;
    public Material floorMaterial;
    public GameObject wall;
    public GameObject wallDoor;
    public int currentInstances;
    public List<ResourceAmount> resourcesNeeded;

    public List<Furniture> furnituresIn;
    public List<Furniture> furnituresOut;
    public List<Furniture> furnituresProcess;
    public List<Furniture> furnituresOpti;
    public List<Furniture> furnituresStorage;

    public Building(int _id, string _name, string _description, Sprite _sprite, Material _floorMaterial)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.sprite = _sprite;
        this.floorMaterial = _floorMaterial;

        /*
        this._wall = Resources.Load("Buildings/Prefabs/" + id + "/Wall") as GameObject;

        foreach (Transform child in _wall.transform.Find("Model"))
        {
            child.GetComponent<MeshRenderer>().material = Resources.Load("Buildings/Walls/" + id + "/Wall", typeof(Material)) as Material;
        }

        this._wallDoor = Resources.Load("Buildings/Prefabs/" + id + "/WallDoor") as GameObject;

        foreach (Transform child in _wallDoor.transform.Find("Model"))
        {
            child.GetComponent<MeshRenderer>().material = Resources.Load("Buildings/Walls/" + id + "/WallDoor", typeof(Material)) as Material;
        }*/

        this.resourcesNeeded = new List<ResourceAmount>();
        this.furnituresIn = new List<Furniture>();
        this.furnituresOut = new List<Furniture>();
        this.furnituresProcess = new List<Furniture>();
        this.furnituresOpti = new List<Furniture>();
        this.furnituresStorage = new List<Furniture>();
        this.currentInstances = 0;
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public Material FloorMaterial
    {
        get { return this.floorMaterial; }
        set { this.floorMaterial = value; }
    }
    public GameObject Wall
    {
        get { return this.wall; }
        set { this.wall = value; }
    }
    public GameObject WallDoor
    {
        get { return this.wallDoor; }
        set { this.wallDoor = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
    public int CurrentInstances
    {
        get { return this.currentInstances; }
        set { this.currentInstances = value; }
    }
}
