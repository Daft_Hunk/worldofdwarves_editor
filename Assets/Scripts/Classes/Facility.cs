using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Facility
{
    public int id;
    public string name;
    public string description;
    public Sprite sprite;
    public GameObject prefab;
    public List<ResourceAmount> resourcesNeeded = new List<ResourceAmount>();
    public int dwarfCapacity;
    public float extractionTime;
    public int extractionAmount;

    public Facility(int _id, string _name, string _description, Sprite _sprite, GameObject _prefab, int _dwarfCapacity, float _extractionTime, int _extractionAmount)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.sprite = _sprite;
        this.prefab = _prefab;
        this.dwarfCapacity = _dwarfCapacity;
        this.extractionTime = _extractionTime;
        this.extractionAmount = _extractionAmount;
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
    public GameObject Prefab
    {
        get { return this.prefab; }
        set { this.prefab = value; }
    }
    public int DwarfCapacity
    {
        get { return this.dwarfCapacity; }
        set { this.dwarfCapacity = value; }
    }
    public int ExtractionAmount
    {
        get { return this.extractionAmount; }
        set { this.extractionAmount = value; }
    }
    public float ExtractionTime
    {
        get { return this.extractionTime; }
        set { this.extractionTime = value; }
    }
}
