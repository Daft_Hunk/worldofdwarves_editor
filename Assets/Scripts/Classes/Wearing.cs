﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wearing
{
    public int id;
    public string name;
    public int basePrice;
    public string description;
    public int defense;
    public float speed;
    public int slot;
    public List<ResourceAmount> resourcesNeeded;
    public Sprite sprite;

    public Wearing(int _id, string _name, string _description, int _basePrice, int _defense, float _speed, int _slot, Sprite _sprite)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.basePrice = _basePrice;
        this.defense = _defense;
        this.speed = _speed;
        this.slot = _slot;
        this.sprite = _sprite;
    }

    public int Id
    {
        get { return this.id; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public int BasePrice
    {
        get { return this.basePrice; }
        set { this.basePrice = value; }
    }
    public int Defense
    {
        get { return this.defense; }
        set { this.defense = value; }
    }
    public float Speed
    {
        get { return this.speed; }
        set { this.speed = value; }
    }
    public int Slot
    {
        get { return this.slot; }
        set { this.slot = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}
