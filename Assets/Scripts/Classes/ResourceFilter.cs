[System.Serializable]
public class ResourceFilter
{
    private int type;
    private int id;
    private int percent;

    public ResourceFilter(int _type, int _id, int _percent)
    {
        this.type = _type;
        this.id = _id;
        this.percent = _percent;
    }

    public int Type
    {
        get { return this.type; }
        set { this.type = value; }
    }
    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public int Percent
    {
        get { return this.percent; }
        set { this.percent = value; }
    }
}
