﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ExpeditionEvent
{
    public int id;
    public string title;
    public int[] choices;
    public Sprite sprite;

    public ExpeditionEvent(int _id, string _title, int[] _choices, Sprite _sprite)
    {
        this.id = _id;
        this.title = _title;
        this.choices = _choices;
        this.sprite = _sprite;
    }

    public int Id
    {
        get { return this.id; }
    }
    public string Title
    {
        get { return this.title; }
        set { this.title = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}

public class Choice
{
    private int id;
    private string desc;
    public int[] answers;

    public Choice(int _id, string _desc, int[] _answers)
    {
        this.id = _id;
        this.desc = _desc;
        this.answers = _answers;
    }
}

public class Answer
{
    private int id;
    private string desc;
    private int choice;

    public Answer(int _id, string _desc, int _choice)
    {
        this.id = _id;
        this.desc = _desc;
        this.choice = _choice;
    }
}
