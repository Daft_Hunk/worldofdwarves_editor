[System.Serializable]
public class ResourceAmount
{
    public int type;
    public int id;
    public int amount;

    public ResourceAmount(int _type, int _id, int _amount)
    {
        this.type = _type;
        this.id = _id;
        this.amount = _amount;
    }
    public int Type
    {
        get { return this.type; }
        set { this.type = value; }
    }
    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public int Amount
    {
        get { return this.amount; }
        set { this.amount = value; }
    }
}
