using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Resource
{
    public int type;
    public int id;
    public string name;
    public int rarity;
    public int basePrice;
    public string description;
    public List<ResourceAmount> resourcesNeeded;
    public Sprite sprite;
    public Texture2D decal;

    public Resource(int _type, int _id, string _name, int _rarity, int _basePrice, string _description, Sprite _sprite, Texture2D _decal)
    {
        this.type = _type;
        this.id = _id;
        this.name = _name;
        this.basePrice = _basePrice;
        this.description = _description;
        switch (_type)
        {
            case 0:
                this.rarity = _rarity;
                break;
            case 1:
                this.rarity = _rarity;
                break;
            case 2:
                break;
            case 3:
                resourcesNeeded = new List<ResourceAmount>();
                break;
        }
        this.sprite = _sprite;
        this.decal = _decal;
    }

    public int Type
    {
        get { return this.type; }
    }
    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public int Rarity
    {
        get { return this.rarity; }
        set { this.rarity = value; }
    }
    public int BasePrice
    {
        get { return this.basePrice; }
        set { this.basePrice = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
    public Texture2D Decal
    {
        get { return this.decal; }
        set { this.decal = value; }
    }
}
