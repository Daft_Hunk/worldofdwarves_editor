using System.Collections.Generic;

[System.Serializable]
public class ResourceRecipe
{
    private int id;
    private bool isAutoCrafted;
    private float speed;
    public Resource resource;
    public List<ResourceAmount> resourceCost;

    public ResourceRecipe(int _Id, bool _isAutoCrafted, float _speed)
    {
        this.id = _Id;
        this.isAutoCrafted = _isAutoCrafted;
        this.speed = _speed;

        //this.resource = resource;

        resourceCost = new List<ResourceAmount>();
        //resourceCost = this.resource.resourcesNeeded;
    }
    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public bool IsAutoCrafted
    {
        get { return this.isAutoCrafted; }
        set { this.isAutoCrafted = value; }
    }
    public float Speed
    {
        get { return this.speed; }
        set { this.speed = value; }
    }
}
