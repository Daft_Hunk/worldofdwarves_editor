using UnityEngine;

[System.Serializable]
public class Unit
{
    public int id;
    public string name;
    public string description;
    public float health;
    public float stamina;
    public float hunger;
    public float speed;
    public Sprite sprite;

    public Unit(int _id, string _name, string _description, float _health, float _stamina, float _hunger, float _speed, Sprite _sprite)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.health = _health;
        this.stamina = _stamina;
        this.hunger = _hunger;
        this.speed = _speed;
        this.sprite = _sprite;
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public float Health
    {
        get { return this.health; }
        set { this.health = value; }
    }
    public float Stamina
    {
        get { return this.stamina; }
        set { this.stamina = value; }
    }
    public float Hunger
    {
        get { return this.hunger; }
        set { this.hunger = value; }
    }
    public float Speed
    {
        get { return this.speed; }
        set { this.speed = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}
