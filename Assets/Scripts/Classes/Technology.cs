﻿using UnityEngine;

[System.Serializable]
public class Technology
{
    public int id;
    public string name;
    public string description;
    public int price;
    public int[] prerequies;
    public Sprite sprite;

    public Technology(int _id, string _name, string _description, int _price, int[] _prerequies, Sprite _sprite)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.price = _price;
        this.prerequies = _prerequies;
        this.sprite = _sprite;
    }

    public int Id
    {
        get { return this.id; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public int Price
    {
        get { return this.price; }
        set { this.price = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}
