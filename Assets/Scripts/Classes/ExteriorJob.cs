using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ExteriorJob
{
    public int id;
    public string name;
    public string description;
    public int extractionTime;
    public Sprite sprite;

    public List<ResourceFilter> resources = new List<ResourceFilter>();

    public ExteriorJob(int _id, string _name, string _description, int _extractionTime, Sprite _sprite)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.extractionTime = _extractionTime;
        this.sprite = _sprite;
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public int ExtractionTime
    {
        get { return this.extractionTime; }
        set { this.extractionTime = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
}
