﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Decoration
{
    public int id;
    public string name;
    public string description;
    public bool isExterior;
    public bool isInterior;
    public bool isWall;
    public float staminaRate;
    public float healthRate;
    public float hungerRate;
    public Sprite sprite;
    public GameObject prefab;
    public List<ResourceAmount> resourcesNeeded;

    public Decoration(int _id, string _name, string _description, bool _isExterior, bool _isInterior, bool _isWall, float _staminaRate, float _healthRate, float _hungerRate, Sprite _sprite, GameObject _prefab)
    {
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.isExterior = _isExterior;
        this.isInterior = _isInterior;
        this.isWall = _isWall;
        this.staminaRate = _staminaRate;
        this.healthRate = _healthRate;
        this.hungerRate = _hungerRate;
        this.sprite = _sprite;
        this.prefab = _prefab;
        resourcesNeeded = new List<ResourceAmount>();
    }

    public int Id
    {
        get { return this.id; }
        set { this.id = value; }
    }
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Description
    {
        get { return this.description; }
        set { this.description = value; }
    }
    public bool IsExterior
    {
        get { return this.isExterior; }
        set { this.isExterior = value; }
    }
    public bool IsInterior
    {
        get { return this.isInterior; }
        set { this.isInterior = value; }
    }
    public bool IsWall
    {
        get { return this.isWall; }
        set { this.isWall = value; }
    }
    public float StaminaRate
    {
        get { return this.staminaRate; }
        set { this.staminaRate = value; }
    }
    public float HealthRate
    {
        get { return this.healthRate; }
        set { this.healthRate = value; }
    }
    public float HungerRate
    {
        get { return this.hungerRate; }
        set { this.hungerRate = value; }
    }
    public Sprite Sprite
    {
        get { return this.sprite; }
        set { this.sprite = value; }
    }
    public GameObject Prefab
    {
        get { return this.prefab; }
        set { this.prefab = value; }
    }
}
