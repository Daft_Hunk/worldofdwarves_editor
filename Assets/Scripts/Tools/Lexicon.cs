using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Lexicon in case someone ask why "Lex"
public static class Lex
{ 
    //Values labels
    public const string LBL_BUILDING_FURN = "buildingFurnitures";
    public const string LBL_FURN = "furniture";
    public const string LBL_INS = "furnituresIn";
    public const string LBL_IN = "furnitureIn";
    public const string LBL_OUTS = "furnituresOut";
    public const string LBL_OUT = "furnitureOut";
    public const string LBL_PROCESSES = "furnituresProcess";
    public const string LBL_PROCESS = "furnitureProcess";
    public const string LBL_OPTIS = "furnituresOptimisation";
    public const string LBL_OPTI = "furnitureOptimisation";
    public const string LBL_STORAGES = "furnituresStorage";
    public const string LBL_STORAGE = "furnitureStorage";

    public const string LBL_ID = "id";
    public const string LBL_TYPE = "type";
    public const string LBL_NAME = "name";
    public const string LBL_DESC = "description";
    public const string LBL_BASE_PRICE = "basePrice";
    public const string LBL_RARITY = "rarity";
    public const string LBL_SPRITE = "sprite";
    public const string LBL_DECAL = "decal";
    public const string LBL_SCALE = "scale";

    public const string LBL_STAMINA_RATE = "staminaRate";
    public const string LBL_HEALTH_RATE = "healthRate";
    public const string LBL_HUNGER_RATE = "hungerRate";
    public const string LBL_EXTRACT_TIME = "extractionTime";
    public const string LBL_EXTRACT_AMOUNT = "extractionAmount";

    public const string LBL_IS_EXT = "isExterior";
    public const string LBL_IS_INT = "isInterior";
    public const string LBL_IS_WALL = "isWall";
    public const string LBL_IS_RANGED = "isRanged";
    public const string LBL_IS_ONE_HANDED = "isOneHanded";
    public const string LBL_IS_AUTO_CRAFTED = "isAutoCrafted";

    public const string LBL_AMOUNT = "amount";
    public const string LBL_CAP = "capacity";
    public const string LBL_DWARF_CAP = "dwarfCapacity";
    public const string LBL_PERCENT = "percent";

    public const string LBL_COSTS = "costs";
    public const string LBL_COST = "cost";
    public const string LBL_RECIPES = "recipes";
    public const string LBL_RECIPE = "recipe";
    public const string LBL_FILTERS = "filters";
    public const string LBL_FILTER = "filter";
    public const string LBL_RESOURCE = "resource";
    public const string LBL_RESOURCES = "resources";
    public const string LBL_RESOURCES_ALLOWED = "resourcesAllowed";
    public const string LBL_RESOURCES_RECIPES = "resourcesRecipes";

    public const string LBL_DAMAGE = "damage";
    public const string LBL_HEALTH = "health";
    public const string LBL_STAMINA = "stamina";
    public const string LBL_HUNGER = "hunger";
    public const string LBL_SPEED = "speed";

    //Items categories
    public static readonly string[] ITEMS_CATS = new string[]
    {
        "minerals",
        "gems",
        "resources",
        "crafted",
        "furnitures",
        "facilities",
        "decorations",
        "buildings",
        "exteriorJobs",
        "weapons",
        "units"
    };

    //JSON subpaths
    public static readonly string[] JSON_PATHS = new string[]
    {
        "/JSON/Minerals/",
        "/JSON/Gems/",
        "/JSON/Resources/",
        "/JSON/Crafted/",
        "/JSON/Furnitures/",
        "/JSON/Facilities/",
        "/JSON/Decorations/",
        "/JSON/Buildings/",
        "/JSON/ExteriorJobs/",
        "/JSON/Weapons/",
        "/JSON/Units/"
    };

    //Sprites subpaths
    public static readonly string[] SP_PATHS = new string[]
    {
        "/Sprites/Minerals/",
        "/Sprites/Gems/",
        "/Sprites/Resources/",
        "/Sprites/Crafted/",
        "/Sprites/Furnitures/",
        "/Sprites/Facilities/",
        "/Sprites/Decorations/",
        "/Sprites/Buildings/",
        "/Sprites/ExteriorJobs/",
        "/Sprites/Weapons/",
        "/Sprites/Units/"
    };

    //Decals subpaths
    public const string DEC_MINERALS = "/Decals/Minerals/";
    public const string DEC_GEMS = "/Decals/Gems/";

    //Assets subpath
    public const string JSON_ASSETS_FOLDER = "/Assets/";
    public const string JSON_ASSETS = "Assets";

    //Assets resources subpath
    public const string ASS_MODELS = "/Assets/Models/";
    public const string ASS_TEXTURES = "/Assets/Textures/";

    //Assets categories
    public static readonly string[] ASS_CATS = new string[]
    {
        "textures",
        "materials",
        "models",
        "prefabs"
    };

    //Texture Extensions
    public static readonly string[] TEX_EXT = new string[]
    {
        "_Albedo",
        "_Metallic",
        "_Normal"
    };

    //Materials parameters
    public static readonly string[] MAT_PARAMS = new string[]
    {
        "textureData",
        "albedoColor",
        "occlusion",
        "metallicStrengh",
        "smoothness",
        "smoothnessMin",
        "smoothnessMax",
        "normalStrengh"
    };

    //Materials parameters
    public static readonly string[] MODEL_PARAMS = new string[]
    {
        "meshes",
        "id",
        "material",
        "scale"
    };

    //Languages
    public const string LG_ENG = "_ENG";
    public const string LG_FR = "_FR";

    //Extensions
    public const string EXT_PNG = ".png";
    public const string EXT_JPG = ".jpg";
    public const string EXT_JPEG = ".jpeg";
    public const string EXT_JSON = ".json";
    public const string EXT_OBJ = ".obj";

    //Error Messages
    public const string ER_MISSING = " is missing or corrupted";

    //Info Messages
    public const string MESS_MOD_FOLDER = "Select the mod folder";
    public const string MESS_SELECT_SPRITE = "Select the new Sprite";
    public const string MESS_SELECT_TEXTURE = "Select the new Texture";
    public const string MESS_SELECT_MODEL = "Select the new Model";
}
