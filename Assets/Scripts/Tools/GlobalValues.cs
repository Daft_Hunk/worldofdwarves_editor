using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class GlobalValues
{
    public static int furnitureUID;
    public static string modPath;
    public static int currentTab;
    public static int currentPanel;
    public static Transform currentParent;
    public static bool isPendingChanges;

    public static List<Unit> units = new List<Unit>();
    public static List<Resource> resources = new List<Resource>();
    public static List<Furniture> furnitures = new List<Furniture>();
    public static List<Decoration> decorations = new List<Decoration>();
    public static List<Building> buildings = new List<Building>();
    public static List<Facility> facilities = new List<Facility>();
    public static List<ExteriorJob> exteriorJobs = new List<ExteriorJob>();
    public static List<Weapon> weapons = new List<Weapon>();

    public static List<ContentInfo> contents = new List<ContentInfo>();

    public static List<ModelData> models = new List<ModelData>();
    public static List<TextureData> textures = new List<TextureData>();
    public static List<MaterialData> materials = new List<MaterialData>();
    public static List<PrefabData> prefabs = new List<PrefabData>();

    public static string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }

    public static void FolderCheck(string subPath)
    {
        if (!Directory.Exists(modPath + subPath))
            Directory.CreateDirectory(modPath + subPath);
    }

    public static void FolderDelete(string subPath)
    {
        if (Directory.Exists(modPath + subPath))
            Directory.Delete(modPath + subPath, true);
    }

    public static void FolderRename(string subPath, string oldName, string newName)
    {
        if (Directory.Exists(modPath + subPath + oldName))
            Directory.Move(modPath + subPath + oldName, modPath + subPath + newName);
        else
            Debug.Log(modPath + subPath + oldName);
    }

    public static void TextureFolderRename(string subPath, string oldName, string newName)
    {
        if (Directory.Exists(modPath + subPath + oldName + "/"))
        {
            if (!Directory.Exists(modPath + subPath + newName + "/"))
                Directory.CreateDirectory(modPath + subPath + newName + "/");
            for (int i = 0; i < 3; i++)
            {
                if(File.Exists(modPath + subPath + oldName + "/" + oldName + Lex.TEX_EXT[i] + Lex.EXT_JPG))
                    File.Copy(modPath + subPath + oldName + "/" + oldName + Lex.TEX_EXT[i] + Lex.EXT_JPG, modPath + subPath + newName + "/" + newName + Lex.TEX_EXT[i] + Lex.EXT_JPG);
            }
            Directory.Delete(modPath + subPath + oldName + "/", true);
        }
        else
            Debug.Log(modPath + subPath + oldName + "/");
    }

    public static void FileDelete(string subPath)
    {
        if (File.Exists(modPath + subPath))
            File.Delete(modPath + subPath);
    }

    public static string FileRead(string subPath)
    {
        string text = "";
        using (FileStream fS = new FileStream(modPath + subPath, FileMode.Open))
        {
            StreamReader sW = new StreamReader(fS);
            text = sW.ReadToEnd();
        }
        return text;
    }

    public static void FileRename(string subPath, string oldName, string newName)
    {
        if (File.Exists(modPath + subPath + oldName))
        {
            File.Copy(modPath + subPath + oldName, modPath + subPath + newName);
            File.Delete(modPath + subPath + oldName);
        }
        else
            Debug.Log(modPath + subPath + oldName + "/");
    }

    public static void FileWrite(string subPath, string text)
    {
        if (File.Exists(modPath + subPath))
        {
            using (FileStream fS = new FileStream(modPath + subPath, FileMode.Truncate))
            {
                StreamWriter sW = new StreamWriter(fS);
                sW.Write(text);
                sW.Close();
            }
        }
        else
        {
            using (FileStream fS = new FileStream(modPath + subPath, FileMode.Create))
            {
                StreamWriter sW = new StreamWriter(fS);
                sW.Write(text);
                sW.Close();
            }
        }            
    }
}
