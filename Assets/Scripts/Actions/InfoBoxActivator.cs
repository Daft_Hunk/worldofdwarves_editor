using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InfoBoxActivator : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string infoText;
    public float timer;
    public bool isOnElement;

    public void FixedUpdate()
    {
        if (isOnElement)
        {
            timer += Time.fixedDeltaTime;
            if (timer > 1)
                GameManager.Instance.ShowInfoBox(infoText);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOnElement = true;        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOnElement = false;
        timer = 0;
        GameManager.Instance.HideInfoBox();
    }
}
