using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderValues : MonoBehaviour
{
    public Slider slider;
    public TMP_InputField text;

    public void ValueChanged(float _value)
    {
        text.text = _value.ToString("0.000");
    }

    public void InputChanged(string _value)
    {        
        float value = float.Parse(_value);
        if (value > 1)
            value = 1;
        text.text = value.ToString("0.000");
        slider.value = value;
    }
}