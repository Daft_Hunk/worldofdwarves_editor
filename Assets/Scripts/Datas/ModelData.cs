using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModelData
{
    public int id;
    public string name;
    public float scale;
    public GameObject model;
    public List<MeshData> meshesData = new List<MeshData>();

    public ModelData(int _id, string _name, float _scale, GameObject _model, List<MeshData> _meshesData)
    {
        this.id = _id;
        this.name = _name;
        this.scale = _scale;
        this.model = _model;
        this.meshesData = _meshesData;
    }
}
