using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PrefabData
{
    public string name;
    public GameObject prefab;
    public ModelData model;

    public PrefabData(string _name, GameObject _prefab, ModelData _model)
    {
        this.name = _name;
        this.prefab = _prefab;
        this.model = _model;
    }
}
