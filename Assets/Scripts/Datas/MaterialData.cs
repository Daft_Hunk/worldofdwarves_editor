using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialData
{
    public int id;
    public string name;
    public Material material;
    public TextureData textureData;

    public MaterialData(int _id, string _name, Material _material, TextureData _textureData)
    {
        this.id = _id;
        this.name = _name;
        this.material = _material;
        this.textureData = _textureData;
    }
}
