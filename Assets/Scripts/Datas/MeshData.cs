using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshData : MonoBehaviour
{
    public int id;
    public MeshRenderer mesh;
    public MaterialData materialData;

    public MeshData(int _id, MeshRenderer _mesh, MaterialData _material)
    {
        this.id = _id;
        this.mesh = _mesh;
        this.materialData = _material;
    }
}
