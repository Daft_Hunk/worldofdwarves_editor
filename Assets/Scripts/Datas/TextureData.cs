using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextureData
{
    public int id;
    public string name;
    public Texture2D albedo;
    public Texture2D metallic;
    public Texture2D normal;

    public TextureData(int _id, string _name, Texture2D _albedo, Texture2D _metallness, Texture2D _normal)
    {
        this.id = _id;
        this.name = _name;
        this.albedo = _albedo;
        this.metallic = _metallness;
        this.normal = _normal;
    }
}
